import { nextTick } from 'process';

export const makeRedis = () => {
  const redis = {
    connect: jest.fn(),
    multi: jest.fn(),
    xAdd: jest.fn(),
    xRead: jest.fn(),
    expire: jest.fn(),
    exec: jest.fn(),
    once: jest.fn(),
    removeListener: jest.fn(),
    executeIsolated: jest.fn(),
    isOpen: false,
    isReady: false
  };
  redis.connect.mockImplementation(() => Promise.resolve(undefined));
  redis.multi.mockImplementation(() => redis);
  redis.xAdd.mockImplementation(() => redis);
  redis.expire.mockImplementation(() => redis);
  redis.exec.mockImplementation(() => Promise.resolve([]));
  redis.executeIsolated.mockImplementation((callback) => callback(redis));
  redis.once.mockImplementation((eventType, fn) => {
    if (eventType === 'ready') {
      nextTick(fn);
    }
  });
  return redis;
};
