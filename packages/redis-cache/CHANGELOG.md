# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5...@jmorecroft67/redis-cache@0.2.6) (2023-01-05)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.9...@jmorecroft67/redis-cache@0.2.5) (2023-01-04)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.9](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.8...@jmorecroft67/redis-cache@0.2.5-staging.9) (2022-12-13)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.8](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.7...@jmorecroft67/redis-cache@0.2.5-staging.8) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.6...@jmorecroft67/redis-cache@0.2.5-staging.7) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.5...@jmorecroft67/redis-cache@0.2.5-staging.6) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.4...@jmorecroft67/redis-cache@0.2.5-staging.5) (2022-12-02)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.3...@jmorecroft67/redis-cache@0.2.5-staging.4) (2022-12-01)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.2...@jmorecroft67/redis-cache@0.2.5-staging.3) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.1...@jmorecroft67/redis-cache@0.2.5-staging.2) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.5-staging.0...@jmorecroft67/redis-cache@0.2.5-staging.1) (2022-11-24)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.5-staging.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.4...@jmorecroft67/redis-cache@0.2.5-staging.0) (2022-11-24)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.3...@jmorecroft67/redis-cache@0.2.4) (2022-11-03)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.2...@jmorecroft67/redis-cache@0.2.3) (2022-10-25)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.1...@jmorecroft67/redis-cache@0.2.2) (2022-10-24)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.2.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.2.0...@jmorecroft67/redis-cache@0.2.1) (2022-08-22)

**Note:** Version bump only for package @jmorecroft67/redis-cache





# 0.2.0 (2022-08-13)


### Features

* **@jmorecroft67/api-maker:** Updated nice-grpc lib and tweaked AbortController usage as required. ([178e526](https://gitlab.com/jmorecroft/arch-packages/commit/178e526a0c2752913388c4918aecda25317d3aec))





## [0.1.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.1.3...@jmorecroft67/redis-cache@0.1.4) (2022-07-21)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.1.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.1.2...@jmorecroft67/redis-cache@0.1.3) (2022-07-21)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.1.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.1.1...@jmorecroft67/redis-cache@0.1.2) (2022-07-12)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.1.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.1.0...@jmorecroft67/redis-cache@0.1.1) (2022-07-12)


### Bug Fixes

* Better retrieval of package info. ([11b2d90](https://gitlab.com/jmorecroft/arch-packages/commit/11b2d9036f0af677c104720419a100bdc34bc8fa))





# [0.1.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.0.7...@jmorecroft67/redis-cache@0.1.0) (2022-07-12)


### Bug Fixes

* Added missing files. ([e2bdfc1](https://gitlab.com/jmorecroft/arch-packages/commit/e2bdfc122c2793ec6b4dd6c95ce64a78e5119eb0))


### Features

* Initial ALPHA support for OpenTelemetry in some packages. ([628006d](https://gitlab.com/jmorecroft/arch-packages/commit/628006daf4ce15e0337f53b9247ec3d248fb63dd))





## [0.0.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.0.6...@jmorecroft67/redis-cache@0.0.7) (2022-07-11)


### Bug Fixes

* **@jmorecroft67/redis-cache:** Change isolated call to blocking XREAD to enable better auto instrumentation, without duplicates. ([4c8b7b6](https://gitlab.com/jmorecroft/arch-packages/commit/4c8b7b6c1c7c69857787e9150712c93a83ddbc23))





## [0.0.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.0.5...@jmorecroft67/redis-cache@0.0.6) (2022-07-11)


### Bug Fixes

* **@jmorecroft67/redis-cache:** Fix test by removing redundant checks. ([8456135](https://gitlab.com/jmorecroft/arch-packages/commit/845613530598abb955b03f425da1bb65b23dbc17))





## [0.0.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.0.4...@jmorecroft67/redis-cache@0.0.5) (2022-07-11)

**Note:** Version bump only for package @jmorecroft67/redis-cache





## [0.0.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.0.3...@jmorecroft67/redis-cache@0.0.4) (2022-07-11)


### Bug Fixes

* **@jmorecroft67/redis-cache:** Updated redis client and fixed connect handling. ([39a3182](https://gitlab.com/jmorecroft/arch-packages/commit/39a31823920b8c1621ba260361a344b805edb555))





## [0.0.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/redis-cache@0.0.2...@jmorecroft67/redis-cache@0.0.3) (2022-07-05)


### Bug Fixes

* **@jmorecroft67/redis-cache:** Better error detection in push. ([8f82ce3](https://gitlab.com/jmorecroft/arch-packages/commit/8f82ce3f5dc7e38cab81475ebd0cbbf4e2cd480b))
* **@jmorecroft67/redis-cache:** Fix test. ([3922eb1](https://gitlab.com/jmorecroft/arch-packages/commit/3922eb13aa5776a67abc07b63e4f9cbfb6448549))





## 0.0.2 (2022-07-02)

**Note:** Version bump only for package @jmorecroft67/redis-cache
