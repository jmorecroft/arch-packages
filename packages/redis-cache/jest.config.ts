// jest.config.ts
import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  preset: 'ts-jest',
  roots: ['<rootDir>/src'],
  testEnvironment: 'node',
  verbose: true,
  moduleNameMapper: {
    'test-utils': '<rootDir>/test/test-utils'
  },
  moduleDirectories: ['node_modules'],
  setupFilesAfterEnv: ['jest-extended/all']
};

export default config;
