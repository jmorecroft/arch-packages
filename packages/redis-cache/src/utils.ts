import { createClient } from 'redis';

export type RedisClient = ReturnType<typeof createClient>;

export const connected = async (redis: RedisClient) => {
  if (!redis.isOpen) {
    await redis.connect();
  }
  if (!redis.isReady) {
    await new Promise<void>((resolve, reject) => {
      const onReady = () => {
        redis.removeListener('error', onError);
        resolve();
      };
      const onError = (e: Error) => {
        redis.removeListener('ready', onReady);
        reject(e);
      };
      redis.once('ready', onReady);
      redis.once('error', onError);
    });
  }
};
