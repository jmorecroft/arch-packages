import Logger from 'bunyan';
import {
  StreamMessagesReply,
  StreamsMessagesReply
} from '@redis/client/dist/lib/commands/generic-transformers';
import * as A from 'fp-ts/Array';
import * as R from 'fp-ts/Reader';
import { struct as eqStruct } from 'fp-ts/lib/Eq';
import { Eq as eqString } from 'fp-ts/lib/string';
import { randomUUID } from 'crypto';
import { connected, RedisClient } from './utils';

export const WAIT_POLL_TIME_MS = 1000 * 60 * 10; // 10 minutes
export const MAX_COUNT_PER_STREAM = 100;

export interface Deps {
  redis: RedisClient;
  logger: Logger;
}

export interface XReadStream {
  key: string;
  id: string;
}

export interface Id {
  time: number;
  seq: number;
}

export interface Request {
  uuid: string;
  stream: Id & XReadStream;
  resolve: (reply: StreamMessagesReply) => void;
  reject: (error: Error) => void;
}

type RequestMap = Map<string, Request[]>;

const cmp = (x: Id, y: Id) => {
  const cmp = x.time - y.time;
  if (cmp !== 0) {
    return cmp;
  }
  return x.seq - y.seq;
};

const toId = (id: string) => {
  const idx = id.indexOf('-');
  if (idx === -1) {
    return {
      time: Number(id),
      seq: 0
    };
  }
  return {
    time: Number(id.substring(0, idx)),
    seq: Number(id.substring(idx + 1))
  };
};

const aggregate = (requests: Map<string, Request[]>) =>
  Array.from(requests.values())
    .flatMap(
      (val) =>
        val.reduce<(XReadStream & Id) | undefined>((prev, next) => {
          if (!prev || cmp(next.stream, prev) < 0) {
            return next.stream;
          }
          return prev;
        }, undefined) ?? []
    )
    .sort((x, y) => x.key.localeCompare(y.key));

export type XReadType = (stream: XReadStream) => R.Reader<
  Deps,
  {
    promise: Promise<StreamMessagesReply>;
    cancel(): void;
  }
>;

const eqAggregateRequest = A.getEq(
  eqStruct({
    key: eqString,
    id: eqString
  })
);

export const makeXRead: () => XReadType = () => {
  let controlStream = {
    key: randomUUID(),
    id: '0-1'
  };
  let current: XReadStream[] | undefined;

  let pending: RequestMap = new Map();

  return (stream) => (deps) => {
    // create request object, including promise to return to caller, and add to pending

    let resolve!: (reply: StreamMessagesReply) => void;
    let reject!: (error: Error) => void;

    const promise = new Promise<StreamMessagesReply>((res, rej) => {
      resolve = res;
      reject = rej;
    });

    const id = toId(stream.id);

    const request: Request = {
      uuid: randomUUID(),
      stream: { ...stream, ...id },
      resolve,
      reject
    };

    if (!pending.get(stream.key)?.push(request)) {
      pending.set(stream.key, [request]);
    }

    const candidate = aggregate(pending);

    if (current) {
      if (!eqAggregateRequest.equals(candidate, current)) {
        // a blocking query is active already so send a control message
        // to unblock and re-submit.
        unblock(controlStream)(deps);
      }
    } else {
      current = candidate;

      const { redis, logger } = deps;

      // query redis until all requests are fulfilled
      const read = async (request: XReadStream[]) => {
        try {
          await connected(redis);

          logger.debug({ request }, 'About to read from stream(s).');

          const reply = await redis.executeIsolated((isolatedClient) =>
            isolatedClient.xRead([...request, controlStream], {
              BLOCK: WAIT_POLL_TIME_MS,
              COUNT: MAX_COUNT_PER_STREAM
            })
          );

          logger.debug({ request, reply }, 'Read from stream(s).');

          const controlMsgs = reply?.find((r) => r.name === controlStream.key);
          if (controlMsgs) {
            const { messages } = controlMsgs;
            controlStream = {
              ...controlStream,
              id: `${toId(messages[messages.length - 1].id).time + 1}-1`
            };
          }

          const { notify, remain } = assign(request, reply, pending);

          pending = remain;

          notify.forEach(([req, res]) => req.resolve(res));

          current = pending.size > 0 ? aggregate(pending) : undefined;

          if (current) {
            read(current);
            return;
          }

          logger.debug('No more reads requested.');
        } catch (e: unknown) {
          pending.forEach((v) =>
            v.forEach((request) => request.reject(e as Error))
          );

          pending.clear();

          current = undefined;
        }
      };

      read(current);
    }

    const cancel = () => {
      const ret = remove(request, pending);

      if (ret.removed) {
        pending = ret.map;

        deps.logger.debug(request.stream, 'Cancelling read.');

        request.resolve([]);

        if (
          current &&
          !eqAggregateRequest.equals(aggregate(pending), current)
        ) {
          unblock(controlStream)(deps);
        }
      }
    };

    return { promise, cancel };
  };
};

export const unblock: (controlStream: XReadStream) => R.Reader<Deps, void> =
  (controlStream) =>
  ({ redis, logger }) => {
    const key = controlStream.key;
    const id = `${(toId(controlStream.id).time + 1).toString()}-*`;

    logger.debug({ key, id }, 'Unblocking read via control stream.');

    redis
      .multi()
      .xAdd(key, id, { data: 'anything' })
      .expire(key, 10)
      .exec()
      .catch((err) => {
        logger.error({ err, key, id }, 'Error trying to unblock connection.');
      });
  };

const remove: (
  val: Request,
  map: RequestMap
) => { map: RequestMap; removed: boolean } = (val, map) => {
  const requests = map.get(val.stream.key);
  if (!requests) {
    return { map, removed: false };
  }

  const index = requests.findIndex((r) => r.uuid === val.uuid);
  if (index === -1) {
    return { map, removed: false };
  }

  const mapCopy = new Map(map);
  if (requests.length === 1) {
    mapCopy.delete(val.stream.key);
  } else {
    const requestsCopy = [...requests];
    requestsCopy.splice(index, 1);
    mapCopy.set(val.stream.key, requestsCopy);
  }

  return { map: mapCopy, removed: true };
};

export const assign: (
  request: XReadStream[],
  reply: StreamsMessagesReply,
  listeners: RequestMap
) => {
  notify: [Request, StreamMessagesReply][];
  remain: RequestMap;
} = (request, reply, listeners) => {
  const notify: [Request, StreamMessagesReply][] = [];
  const remain: Map<string, Request[]> = new Map();

  reply?.forEach(({ name, messages }) => {
    const id = request.find((v) => v.key === name)?.id;

    const start = id ? toId(id) : undefined;

    const key = name.toString();

    const candidates = listeners.get(key);

    if (start && candidates) {
      const remains: Request[] = [];

      candidates.forEach((candidate) => {
        let index;
        if (
          cmp(candidate.stream, start) >= 0 &&
          (index = messages.findIndex(
            (v) => cmp(candidate.stream, toId(v.id.toString())) < 0
          )) !== -1
        ) {
          if (index === 0) {
            notify.push([candidate, messages]);
            return;
          }
          notify.push([candidate, messages.slice(index)]);
          return;
        }
        remains.push(candidate);
      });

      if (remains.length > 0) {
        remain.set(key, remains);
      }
    }
  });

  for (const [key, val] of listeners.entries()) {
    if (reply === null || !reply.some((r) => r.name === key)) {
      remain.set(key, val);
    }
  }

  return {
    notify,
    remain
  };
};
