import { trace } from '@opentelemetry/api';
import findPackageJson = require('find-package-json');

const packageJson = Array.from(findPackageJson()).at(0);

const libName = packageJson?.name ?? '@jmorecroft67/redis-cache';
const libVersion = packageJson?.version ?? 'unknown';

export const tracer = trace.getTracer(libName, libVersion);
