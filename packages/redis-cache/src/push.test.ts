/* eslint-disable @typescript-eslint/no-explicit-any */
import * as E from 'fp-ts/Either';
import { ErrorReply } from 'redis';
import { makeRedis } from 'test-utils';
import { push, EXPIRY_TIME_MS } from './push';

jest.mock('./make-xread');

describe(__filename, () => {
  afterEach(() => jest.resetAllMocks());

  it('should push onto stream, no errors', async () => {
    const redis = makeRedis();

    const result = await push(
      'key',
      new Date(Date.UTC(1970, 0, 1)),
      'd1'
    )({ redis: redis as any })();

    expect(E.isRight(result));
    expect(redis.multi).toHaveBeenCalledTimes(1);
    expect(redis.exec).toHaveBeenCalledTimes(1);
    expect(redis.xAdd).toHaveBeenCalledWith(
      'key',
      '0-*',
      { data: 'd1' },
      { TRIM: { strategy: 'MINID', threshold: -EXPIRY_TIME_MS } }
    );
    expect(redis.expire).toHaveBeenCalledWith('key', EXPIRY_TIME_MS / 1000);
  });

  it('should push onto stream, adjust id on error', async () => {
    const redis = makeRedis();
    redis.exec.mockResolvedValue([new ErrorReply('boom')]);
    redis.xRead.mockResolvedValue([
      { messages: [{ id: '123-4' }, { id: '124-5' }] }
    ]);

    const result = await push(
      'key',
      new Date(Date.UTC(1970, 0, 1)),
      'd1'
    )({ redis: redis as any })();

    expect(E.isRight(result));
    expect(redis.multi).toHaveBeenCalledTimes(2);
    expect(redis.exec).toHaveBeenCalledTimes(2);
    expect(redis.expire).toHaveBeenCalledTimes(2);
    expect(redis.xRead).toHaveBeenCalledWith({
      key: 'key',
      id: '0'
    });
    expect(redis.xAdd).toHaveBeenNthCalledWith(
      1,
      'key',
      '0-*',
      { data: 'd1' },
      { TRIM: { strategy: 'MINID', threshold: -EXPIRY_TIME_MS } }
    );
    expect(redis.xAdd).toHaveBeenNthCalledWith(
      2,
      'key',
      '124-*',
      { data: 'd1' },
      { TRIM: { strategy: 'MINID', threshold: -EXPIRY_TIME_MS } }
    );
    expect(redis.expire).toHaveBeenCalledTimes(2);
  });
});
