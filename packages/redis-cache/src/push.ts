import { ErrorReply } from 'redis';
import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import { connected, RedisClient } from './utils';
import api from '@opentelemetry/api';

export type Deps = {
  redis: RedisClient;
};

export const EXPIRY_TIME_MS = 100000; // 100 seconds

export const push: (
  key: string,
  updatedAt: Date,
  data: string
) => RTE.ReaderTaskEither<Deps, Error, void> =
  (key, updatedAt, data) =>
  ({ redis }) => {
    const id = `${updatedAt.getTime()}-*`;

    return TE.tryCatch(
      async () => {
        const payload = { data };
        api.propagation.inject(api.context.active(), payload);

        const addAndExpire = (id: string) =>
          redis
            .multi()
            .xAdd(key, id, payload, {
              TRIM: {
                strategy: 'MINID',
                threshold: updatedAt.getTime() - EXPIRY_TIME_MS
              }
            })
            .expire(key, EXPIRY_TIME_MS / 1000)
            .exec();

        await connected(redis);
        let added = await addAndExpire(id);

        if ((added as unknown[]).some((item) => item instanceof ErrorReply)) {
          const latest = await redis.xRead({
            key,
            id: `${updatedAt.getTime()}`
          });

          const messages = latest?.[0].messages;
          if (messages && messages.length > 0) {
            const last = messages[messages.length - 1];
            const id2 = `${last.id.substring(0, last.id.indexOf('-'))}-*`;

            added = await addAndExpire(id2);

            const errors = (added as unknown[]).filter(
              (item): item is ErrorReply => item instanceof ErrorReply
            );

            if (errors.length > 0) {
              throw errors[0];
            }
          }
        }
      },
      (e) => e as Error
    );
  };
