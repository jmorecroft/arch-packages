/* eslint-disable @typescript-eslint/no-explicit-any */
import { nextTick } from 'process';
import { makeRedis } from 'test-utils';
import { connected } from './utils';

describe(__filename, () => {
  let redis!: ReturnType<typeof makeRedis>;

  beforeEach(() => {
    redis = makeRedis();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should handle connect ok', async () => {
    await connected(redis as any);

    expect(redis.connect).toHaveBeenCalledTimes(1);
    expect(redis.once).toHaveBeenCalledTimes(2);
    expect(redis.once).toHaveBeenNthCalledWith(
      1,
      'ready',
      expect.any(Function)
    );
    expect(redis.once).toHaveBeenNthCalledWith(
      2,
      'error',
      expect.any(Function)
    );
    expect(redis.removeListener).toHaveBeenCalledTimes(1);
    expect(redis.removeListener).toHaveBeenCalledWith(
      'error',
      expect.any(Function)
    );
  });

  it('should handle connect fail', async () => {
    redis.once.mockImplementation((eventType, fn) => {
      if (eventType === 'error') {
        nextTick(fn, new Error('boom!'));
      }
    });

    await expect(connected(redis as any)).rejects.toEqual(
      expect.objectContaining({
        message: 'boom!'
      })
    );

    expect(redis.connect).toHaveBeenCalledTimes(1);
    expect(redis.once).toHaveBeenCalledTimes(2);
    expect(redis.once).toHaveBeenNthCalledWith(
      1,
      'ready',
      expect.any(Function)
    );
    expect(redis.once).toHaveBeenNthCalledWith(
      2,
      'error',
      expect.any(Function)
    );
    expect(redis.removeListener).toHaveBeenCalledTimes(1);
    expect(redis.removeListener).toHaveBeenCalledWith(
      'ready',
      expect.any(Function)
    );
  });
});
