import * as RTE from 'fp-ts/ReaderTaskEither';
import * as TE from 'fp-ts/TaskEither';
import { connected, RedisClient } from './utils';

export type Deps = {
  redis: RedisClient;
};

export const clear: (key: string) => RTE.ReaderTaskEither<Deps, Error, void> =
  (key) =>
  ({ redis }) => {
    return TE.tryCatch(
      async () => {
        await connected(redis);
        await redis.del(key);
      },
      (e) => e as Error
    );
  };
