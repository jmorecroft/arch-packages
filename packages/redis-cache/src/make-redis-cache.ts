import * as E from 'fp-ts/Either';
import * as R from 'fp-ts/Reader';
import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { Repeater } from '@repeaterjs/repeater';
import Logger from 'bunyan';
import { XReadType } from './make-xread';
import { connected, RedisClient } from './utils';
import * as _ from 'lodash';
import api, { Link, SpanKind, SpanStatusCode } from '@opentelemetry/api';
import { tracer } from './tracing';

export type CompatAbortSignal = AbortSignal & {
  addEventListener(type: 'abort', listener: () => void): void;
  removeEventListener(type: 'abort', listener: () => void): void;
};

export interface Cache {
  idAfterOrEqual(date: Date): string;
  get(id: string): TE.TaskEither<Error, { id: string; data: string }[]>;
  stream(
    id: string,
    signal: AbortSignal
  ): AsyncIterable<E.Either<Error, string[]>>;
}

export type Deps = {
  redis: RedisClient;
  xread: XReadType;
  logger: Logger;
};

export type Options = {
  key: {
    prefix: string;
    components?: Record<string, string | number | boolean>;
  };
};

export const makeRedisCache: (options: Options) => R.Reader<Deps, Cache> =
  ({ key: { components, prefix } }) =>
  ({ redis, logger, xread }) => {
    const key = components
      ? Object.keys(components)
          .sort() // alphabetical order
          .reduce((prev, next) => `${prev}:${components[next]}`, prefix)
      : prefix;

    const idAfterOrEqual: Cache['idAfterOrEqual'] = (d) =>
      (d.getTime() - 1).toString();

    const get: Cache['get'] = (id) => {
      return pipe(
        TE.tryCatch(
          async () => {
            await connected(redis);
            return redis.xRead({ key, id });
          },
          (e) => e as Error
        ),
        TE.map((a) => {
          if (a === null) {
            return [];
          }

          return a[0].messages.map(({ id, message: { data } }) => ({
            id,
            data
          }));
        })
      );
    };

    const stream: Cache['stream'] = (start, signal) =>
      readData({ key, start, signal: signal as CompatAbortSignal })({
        xread,
        redis,
        logger
      });

    return {
      idAfterOrEqual,
      get,
      stream
    };
  };

const readData: (args: {
  key: string;
  start: string;
  signal: CompatAbortSignal;
}) => R.Reader<Deps, AsyncIterable<E.Either<Error, string[]>>> =
  ({ key, start, signal }) =>
  ({ xread, ...deps }) => {
    return new Repeater<E.Either<Error, string[]>>(async (push, stop) => {
      let id = start;

      const result = await TE.tryCatch(
        async () => {
          let cancel: () => void | undefined;

          const onAbort = _.once(() => cancel?.());

          signal.addEventListener('abort', onAbort);

          const stop2 = stop.then(_.identity);

          try {
            while (!signal.aborted) {
              const ret = xread({ id, key })(deps);

              cancel = ret.cancel;

              const reply = await Promise.race([stop2, ret.promise]);

              if (reply === undefined) {
                onAbort();
                break;
              }

              if (reply && reply.length > 0) {
                id = reply[reply.length - 1].id.toString();

                const data = reply.map(({ message: { data } }) =>
                  data.toString()
                );

                const ctx = api.context.active();

                const links = reply.flatMap(({ message }): Link | [] => {
                  const remoteCtx = api.propagation.extract(ctx, message);
                  if (remoteCtx === ctx) {
                    return [];
                  }

                  const spanCtx = api.trace.getSpanContext(remoteCtx);
                  if (spanCtx === undefined) {
                    return [];
                  }

                  return {
                    context: spanCtx
                  };
                });

                await tracer.startActiveSpan(
                  'consumeMessages',
                  { links, kind: SpanKind.CONSUMER },
                  async (span) => {
                    try {
                      await push(E.right(data));
                      span.setStatus({ code: SpanStatusCode.OK });
                    } catch (e) {
                      const error = e as Error;
                      span.recordException(error);
                      span.setStatus({
                        code: SpanStatusCode.ERROR,
                        message: error.message
                      });
                      throw e;
                    } finally {
                      span.end();
                    }
                  }
                );
              }
            }
          } finally {
            signal.removeEventListener('abort', onAbort);
          }
        },
        (e) => e as Error
      )();

      if (E.isLeft(result)) {
        await push(result);
      }
      stop();
    });
  };
