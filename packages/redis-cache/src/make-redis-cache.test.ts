/* eslint-disable @typescript-eslint/no-explicit-any */
import * as E from 'fp-ts/Either';
import { makeRedisCache } from './make-redis-cache';
import { createLogger } from 'bunyan';
import { promisify } from 'util';

describe(__filename, () => {
  const logger = createLogger({
    name: 'test',
    level: 'debug'
  });

  const makeRedis = () => {
    const redis = {
      connect: jest.fn(),
      multi: jest.fn(),
      xAdd: jest.fn(),
      xRead: jest.fn(),
      expire: jest.fn(),
      exec: jest.fn()
    };
    redis.connect.mockImplementation(() => Promise.resolve(undefined));
    redis.multi.mockImplementation(() => redis);
    redis.xAdd.mockImplementation(() => redis);
    redis.expire.mockImplementation(() => redis);
    redis.exec.mockImplementation(() => Promise.resolve([]));
    return redis;
  };

  afterEach(() => jest.resetAllMocks());

  it('should stream, then cancel on break', async () => {
    const cancel = jest.fn();
    const xread2 = jest.fn();
    const xread = jest.fn(() => xread2);

    const redis = makeRedis();

    const cache = makeRedisCache({
      key: {
        prefix: 'test',
        components: {
          a: 'x',
          b: 'y',
          c: 'z'
        }
      }
    })({ logger, redis: redis as any, xread });

    xread2.mockImplementationOnce(() => ({
      promise: Promise.resolve([{ id: '123-1', message: { data: 'one' } }]),
      cancel
    }));
    xread2.mockImplementationOnce(() => ({
      promise: Promise.resolve([{ id: '123-2', message: { data: 'two' } }]),
      cancel
    }));
    xread2.mockImplementationOnce(() => ({
      promise: Promise.resolve([]),
      cancel
    }));

    const results: E.Either<Error, string[]>[] = [];

    for await (const data of cache.stream(
      '123',
      new AbortController().signal
    )) {
      results.push(data);
      if (results.length === 2) {
        break;
      }
    }

    expect(results).toEqual([E.right(['one']), E.right(['two'])]);
    expect(cancel).toHaveBeenCalledTimes(1);
    expect(xread).toHaveBeenCalledTimes(3);
    expect(xread).toHaveBeenNthCalledWith(1, { id: '123', key: 'test:x:y:z' });
    expect(xread).toHaveBeenNthCalledWith(2, {
      id: '123-1',
      key: 'test:x:y:z'
    });
    expect(xread).toHaveBeenNthCalledWith(3, {
      id: '123-2',
      key: 'test:x:y:z'
    });
  });

  it('should stream, then cancel on abort', async () => {
    const cancel = jest.fn();
    const xread2 = jest.fn();
    const xread = jest.fn(() => xread2);

    const redis = makeRedis();

    const cache = makeRedisCache({
      key: {
        prefix: 'test',
        components: {
          a: 'x',
          b: 'y',
          c: 'z'
        }
      }
    })({ logger, redis: redis as any, xread });

    xread2.mockImplementationOnce(() => ({
      promise: Promise.resolve([{ id: '123-1', message: { data: 'one' } }]),
      cancel
    }));
    xread2.mockImplementationOnce(() => ({
      promise: Promise.resolve([{ id: '123-2', message: { data: 'two' } }]),
      cancel
    }));
    xread2.mockImplementationOnce(() => ({
      promise: promisify(setTimeout)(2000).then(() => []),
      cancel
    }));

    const results: E.Either<Error, string[]>[] = [];

    const controller = new AbortController();

    setTimeout(() => controller.abort(), 1000);

    for await (const data of cache.stream('123', controller.signal)) {
      results.push(data);
    }

    expect(results).toEqual([E.right(['one']), E.right(['two'])]);
    expect(cancel).toHaveBeenCalledTimes(1);
    expect(xread).toHaveBeenCalledTimes(3);
    expect(xread).toHaveBeenNthCalledWith(1, { id: '123', key: 'test:x:y:z' });
    expect(xread).toHaveBeenNthCalledWith(2, {
      id: '123-1',
      key: 'test:x:y:z'
    });
    expect(xread).toHaveBeenNthCalledWith(3, {
      id: '123-2',
      key: 'test:x:y:z'
    });
  });
  it('should stream, then exit on error', async () => {
    const cancel = jest.fn();
    const xread2 = jest.fn();
    const xread = jest.fn(() => xread2);

    const redis = makeRedis();

    const cache = makeRedisCache({
      key: {
        prefix: 'test',
        components: {
          a: 'x',
          b: 'y',
          c: 'z'
        }
      }
    })({ logger, redis: redis as any, xread });

    xread2.mockImplementationOnce(() => ({
      promise: Promise.resolve([{ id: '123-1', message: { data: 'one' } }]),
      cancel
    }));
    xread2.mockImplementationOnce(() => ({
      promise: Promise.reject(new Error('boom!')),
      cancel
    }));

    const results: E.Either<Error, string[]>[] = [];

    for await (const data of cache.stream(
      '123',
      new AbortController().signal
    )) {
      results.push(data);
    }

    expect(results).toEqual([E.right(['one']), E.left(new Error('boom!'))]);
    expect(cancel).toHaveBeenCalledTimes(0);
    expect(xread).toHaveBeenCalledTimes(2);
    expect(xread).toHaveBeenNthCalledWith(1, { id: '123', key: 'test:x:y:z' });
    expect(xread).toHaveBeenNthCalledWith(2, {
      id: '123-1',
      key: 'test:x:y:z'
    });
  });
});
