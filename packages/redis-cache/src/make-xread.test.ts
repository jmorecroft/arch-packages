/* eslint-disable @typescript-eslint/no-explicit-any */
import Logger, { createLogger } from 'bunyan';
import {
  makeXRead,
  WAIT_POLL_TIME_MS,
  MAX_COUNT_PER_STREAM
} from './make-xread';
import { makeRedis } from 'test-utils';

jest.mock('crypto', () => ({
  randomUUID: () => 'control'
}));

describe(__filename, () => {
  const logger = createLogger({
    name: 'test',
    level: 'debug'
  });

  let redis!: ReturnType<typeof makeRedis>;
  let deps!: { redis: any; logger: Logger };

  beforeEach(() => {
    redis = makeRedis();
    deps = { redis, logger };
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should handle single read', async () => {
    const messages = [
      {
        id: '123-1',
        message: { data: 'any1' }
      },
      {
        id: '123-2',
        message: { data: 'any2' }
      }
    ];
    redis.xRead.mockResolvedValue([
      {
        name: 'key',
        messages
      }
    ]);

    const xread = makeXRead();

    const { promise } = xread({ key: 'key', id: '123' })(deps);

    const reply = await promise;

    expect(redis.xRead).toHaveBeenCalledWith(
      [
        expect.objectContaining({
          id: '123',
          key: 'key'
        }),
        expect.objectContaining({
          id: '0-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(reply).toEqual(messages);
  });

  it('should handle multiple reads with increasing ids', async () => {
    redis.xRead.mockResolvedValueOnce([
      {
        name: 'key',
        messages: [
          {
            id: '123-1',
            message: { data: 'any1' }
          },
          {
            id: '123-2',
            message: { data: 'any2' }
          }
        ]
      }
    ]);
    redis.xRead.mockResolvedValueOnce([
      {
        name: 'key',
        messages: [
          {
            id: '124-1',
            message: { data: 'any' }
          }
        ]
      },
      {
        name: 'key2',
        messages: [
          {
            id: '100-1',
            message: { data: 'any' }
          }
        ]
      }
    ]);

    const xread = makeXRead();

    const { promise: promise1 } = xread({ key: 'key', id: '123-0' })(deps);
    const { promise: promise2 } = xread({ key: 'key', id: '123-1' })(deps);
    const { promise: promise3 } = xread({ key: 'key', id: '124' })(deps);
    const { promise: promise4 } = xread({ key: 'key2', id: '100' })(deps);

    const replies = await Promise.all([promise1, promise2, promise3, promise4]);

    expect(redis.xRead).toHaveBeenNthCalledWith(
      1,
      [
        expect.objectContaining({
          id: '123-0',
          key: 'key'
        }),
        expect.objectContaining({
          id: '0-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(redis.xRead).toHaveBeenNthCalledWith(
      2,
      [
        expect.objectContaining({
          id: '124',
          key: 'key'
        }),
        expect.objectContaining({
          id: '100',
          key: 'key2'
        }),
        expect.objectContaining({
          id: '0-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(redis.multi).toHaveBeenCalledTimes(1);
    expect(redis.exec).toHaveBeenCalledTimes(1);
    expect(redis.xAdd).toHaveBeenCalledTimes(1);
    expect(redis.xAdd).toHaveBeenCalledWith('control', '1-*', {
      data: 'anything'
    });
    expect(redis.expire).toHaveBeenCalledTimes(1);
    expect(redis.expire).toHaveBeenCalledWith('control', 10);
    expect(replies).toEqual([
      [
        {
          id: '123-1',
          message: {
            data: 'any1'
          }
        },
        {
          id: '123-2',
          message: {
            data: 'any2'
          }
        }
      ],
      [
        {
          id: '123-2',
          message: {
            data: 'any2'
          }
        }
      ],
      [
        {
          id: '124-1',
          message: {
            data: 'any'
          }
        }
      ],
      [
        {
          id: '100-1',
          message: {
            data: 'any'
          }
        }
      ]
    ]);
  });

  it('should handle multiple reads with decreasing ids', async () => {
    redis.xRead.mockResolvedValueOnce([
      {
        name: 'key',
        messages: [
          {
            id: '124-1',
            message: { data: 'any' }
          }
        ]
      }
    ]);
    redis.xRead.mockResolvedValueOnce([
      {
        name: 'key',
        messages: [
          {
            id: '123-1',
            message: { data: 'any' }
          },
          {
            id: '123-2',
            message: { data: 'any' }
          },
          {
            id: '124-1',
            message: { data: 'any' }
          }
        ]
      },
      {
        name: 'key2',
        messages: [
          {
            id: '100-1',
            message: { data: 'any' }
          }
        ]
      },
      {
        name: 'control',
        messages: [
          {
            id: '1-2'
          }
        ]
      }
    ]);

    const xread = makeXRead();

    const { promise: promise1 } = xread({ key: 'key', id: '124' })(deps);
    const { promise: promise2 } = xread({ key: 'key', id: '123-1' })(deps);
    const { promise: promise3 } = xread({ key: 'key', id: '123-0' })(deps);
    const { promise: promise4 } = xread({ key: 'key2', id: '100' })(deps);

    const replies = await Promise.all([promise1, promise2, promise3, promise4]);

    expect(redis.xRead).toHaveBeenNthCalledWith(
      1,
      [
        expect.objectContaining({
          id: '124',
          key: 'key'
        }),
        expect.objectContaining({
          id: '0-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(redis.xRead).toHaveBeenNthCalledWith(
      2,
      [
        expect.objectContaining({
          id: '123-0',
          key: 'key'
        }),
        expect.objectContaining({
          id: '100',
          key: 'key2'
        }),
        expect.objectContaining({
          id: '0-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(redis.multi).toHaveBeenCalledTimes(3);
    expect(redis.exec).toHaveBeenCalledTimes(3);

    const controlArgs = [
      'control',
      '1-*',
      {
        data: 'anything'
      }
    ];
    expect(redis.xAdd).toHaveBeenCalledTimes(3);
    expect(redis.xAdd).toHaveBeenNthCalledWith(1, ...controlArgs);
    expect(redis.xAdd).toHaveBeenNthCalledWith(2, ...controlArgs);
    expect(redis.xAdd).toHaveBeenNthCalledWith(3, ...controlArgs);

    const expireArgs = ['control', 10];
    expect(redis.expire).toHaveBeenCalledTimes(3);
    expect(redis.expire).toHaveBeenNthCalledWith(1, ...expireArgs);
    expect(redis.expire).toHaveBeenNthCalledWith(2, ...expireArgs);
    expect(redis.expire).toHaveBeenNthCalledWith(3, ...expireArgs);

    expect(replies).toEqual([
      [
        {
          id: '124-1',
          message: {
            data: 'any'
          }
        }
      ],
      [
        {
          id: '123-2',
          message: {
            data: 'any'
          }
        },
        {
          id: '124-1',
          message: {
            data: 'any'
          }
        }
      ],
      [
        {
          id: '123-1',
          message: {
            data: 'any'
          }
        },
        {
          id: '123-2',
          message: {
            data: 'any'
          }
        },
        {
          id: '124-1',
          message: {
            data: 'any'
          }
        }
      ],

      [
        {
          id: '100-1',
          message: {
            data: 'any'
          }
        }
      ]
    ]);
  });

  it('should handle error', async () => {
    redis.xRead.mockRejectedValue(new Error('boom'));

    const xread = makeXRead();

    const { promise } = xread({ key: 'key', id: '123' })(deps);

    await expect(promise).rejects.toEqual(
      expect.objectContaining({ message: 'boom' })
    );
  });

  it('should handle cancel', async () => {
    redis.xRead.mockResolvedValueOnce([
      {
        name: 'control',
        messages: [
          {
            id: '1-0',
            message: { data: 'anything' }
          }
        ]
      }
    ]);
    redis.xRead.mockResolvedValueOnce([
      {
        name: 'key2',
        messages: [
          {
            id: '124-1',
            message: { data: 'any' }
          }
        ]
      }
    ]);
    const xread = makeXRead();

    const { promise: promise1, cancel } = xread({ key: 'key', id: '124' })(
      deps
    );
    const { promise: promise2 } = xread({ key: 'key2', id: '124' })(deps);

    cancel();

    const replies = await Promise.all([promise1, promise2]);

    expect(redis.xRead).toHaveBeenNthCalledWith(
      1,
      [
        expect.objectContaining({
          id: '124',
          key: 'key'
        }),
        expect.objectContaining({
          id: '0-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(redis.xRead).toHaveBeenNthCalledWith(
      2,
      [
        expect.objectContaining({
          id: '124',
          key: 'key2'
        }),
        expect.objectContaining({
          id: '2-1',
          key: 'control'
        })
      ],
      {
        BLOCK: WAIT_POLL_TIME_MS,
        COUNT: MAX_COUNT_PER_STREAM
      }
    );
    expect(redis.multi).toHaveBeenCalledTimes(2);
    expect(redis.exec).toHaveBeenCalledTimes(2);

    const controlArgs = [
      'control',
      '1-*',
      {
        data: 'anything'
      }
    ];
    expect(redis.xAdd).toHaveBeenCalledTimes(2);
    expect(redis.xAdd).toHaveBeenNthCalledWith(1, ...controlArgs);
    expect(redis.xAdd).toHaveBeenNthCalledWith(2, ...controlArgs);
    const expireArgs = ['control', 10];
    expect(redis.expire).toHaveBeenCalledTimes(2);
    expect(redis.expire).toHaveBeenNthCalledWith(1, ...expireArgs);
    expect(redis.expire).toHaveBeenNthCalledWith(2, ...expireArgs);
    expect(replies).toEqual([[], [{ id: '124-1', message: { data: 'any' } }]]);
  });
});
