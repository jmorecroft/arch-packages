import { makeHandler } from './make-handler';
import { utils } from '@jmorecroft67/io-ts-types';
import * as redisCache from '@jmorecroft67/redis-cache';
import * as RTE from 'fp-ts/ReaderTaskEither';

jest.mock('@jmorecroft67/redis-cache');

const mockRedisCache = redisCache as jest.Mocked<typeof redisCache>;

describe(__filename, () => {
  jest.spyOn(utils, 'getEnvOrThrow').mockReturnValue({
    REDIS_HOST: 'redis',
    REDIS_PORT: 6789,
    LOG_LEVEL: 'debug'
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it.each([['Insert'], ['Update']])('should handle %s', async (eventType) => {
    const handler = makeHandler();

    mockRedisCache.push.mockReturnValue(RTE.of(undefined));

    await handler({
      data: {
        message: {
          attributes: {
            eventType,
            orderingKey: 'key',
            txnTimeStamp: '2022-01-01T00:00:00Z',
            updatedAt: '2022-01-02T00:00:00Z'
          },
          data: Buffer.from('123').toString('base64')
        }
      },
      id: '',
      source: '',
      specversion: '',
      type: ''
    });

    expect(mockRedisCache.push).toHaveBeenCalledWith(
      'key',
      new Date(Date.UTC(2022, 0, 2)),
      '123'
    );
  });

  it('should handle Delete', async () => {
    const handler = makeHandler();

    mockRedisCache.push.mockReturnValue(RTE.of(undefined));

    await handler({
      data: {
        message: {
          attributes: {
            eventType: 'Delete',
            orderingKey: 'key',
            txnTimeStamp: '2022-01-01T00:00:00Z',
            updatedAt: '2022-01-02T00:00:00Z'
          },
          data: Buffer.from('123').toString('base64')
        }
      },
      id: '',
      source: '',
      specversion: '',
      type: ''
    });

    expect(mockRedisCache.push).toHaveBeenCalledWith(
      'key',
      new Date(Date.UTC(2022, 0, 1)),
      '123'
    );
  });

  it('should handle Relation', async () => {
    const handler = makeHandler();

    mockRedisCache.push.mockReturnValue(RTE.of(undefined));

    await handler({
      data: {
        message: {
          attributes: {
            eventType: 'Relation',
            orderingKey: 'key',
            txnTimeStamp: '2022-01-01T00:00:00Z',
            updatedAt: '2022-01-02T00:00:00Z'
          },
          data: Buffer.from('123').toString('base64')
        }
      },
      id: '',
      source: '',
      specversion: '',
      type: ''
    });

    expect(mockRedisCache.push).toHaveBeenCalledTimes(0);
  });

  it('should throw on error', async () => {
    const handler = makeHandler();

    mockRedisCache.push.mockReturnValue(RTE.of(undefined));

    // missing data
    await expect(
      handler({
        id: '',
        source: '',
        specversion: '',
        type: ''
      })
    ).rejects.toEqual(
      expect.objectContaining({
        message: 'cannot decode undefined, should be Record<string, unknown>'
      })
    );
  });
});
