import './tracing';
import { cloudEvent } from '@google-cloud/functions-framework';
import { makeHandler } from './make-handler';

const handler = makeHandler();

cloudEvent<unknown>('gcpPubsubToRedis', handler);
