import * as E from 'fp-ts/Either';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as D from 'io-ts/Decoder';
import { flow, pipe } from 'fp-ts/lib/function';
import { createClient } from 'redis';
import { push, RedisClient } from '@jmorecroft67/redis-cache';
import {
  utils as U,
  decoder as De,
  types as T
} from '@jmorecroft67/io-ts-types';
import Logger, { createLogger } from 'bunyan';
import { CloudEvent } from '@google-cloud/functions-framework';
import api, { SpanContext, SpanKind, SpanStatusCode } from '@opentelemetry/api';
import { tracer } from './tracing';

const envDecoder = D.struct({
  REDIS_HOST: U.withDefault(D.string)('redis'),
  REDIS_PORT: U.withDefault(De.int)(6379 as T.Int),
  LOG_LEVEL: U.withDefault(
    D.union(
      D.literal('info'),
      D.literal('error'),
      D.literal('debug'),
      D.literal('trace')
    )
  )('info')
});

const spanContextDecoder: D.Decoder<unknown, SpanContext> = pipe(
  D.string,
  D.parse((a) =>
    E.tryCatch(
      () => JSON.parse(a),
      (e) => D.error(a, `JSON, but got ${(e as Error).message}`)
    )
  ),
  D.compose(
    D.struct({
      traceId: D.string,
      spanId: D.string,
      traceFlags: D.number
    })
  )
);

const commonAttrs = pipe(
  D.struct({
    txnTimeStamp: De.date,
    orderingKey: D.string
  }),
  D.intersect(
    D.partial({
      googclient_OpenTelemetrySpanContext: spanContextDecoder
    })
  )
);

const cloudEventDataDecoder = D.struct({
  message: D.struct({
    attributes: D.sum('eventType')({
      Relation: pipe(
        D.struct({
          eventType: D.literal('Relation')
        }),
        D.intersect(commonAttrs)
      ),
      Insert: pipe(
        D.struct({
          eventType: D.literal('Insert'),
          updatedAt: De.date
        }),
        D.intersect(commonAttrs)
      ),
      Update: pipe(
        D.struct({
          eventType: D.literal('Update'),
          updatedAt: De.date
        }),
        D.intersect(commonAttrs)
      ),
      Delete: pipe(
        D.struct({
          eventType: D.literal('Delete')
        }),
        D.intersect(commonAttrs)
      )
    }),
    data: D.string
  })
});

export type CloudEventData = D.TypeOf<typeof cloudEventDataDecoder>;

export const makeHandler = () => {
  const { REDIS_HOST, REDIS_PORT, LOG_LEVEL } = U.getEnvOrThrow(envDecoder);

  const logger = createLogger({
    name: 'gcp-pubsub-to-redis',
    level: LOG_LEVEL
  });

  const redis = createClient({
    url: `redis://${REDIS_HOST}:${REDIS_PORT}`
  });

  return (event: CloudEvent<unknown>) => {
    logger.debug({ event }, 'Received event.');

    const { data } = event;

    return pipe(
      { redis, logger },
      pipe(
        RTE.fromEither(cloudEventDataDecoder.decode(data)),
        RTE.mapLeft((e) => new Error(D.draw(e))),
        RTE.chain(process)
      )
    )().then(
      flow(
        E.fold(
          (err) => {
            logger.error({ err, event }, 'Failed to push event to Redis.');
            throw err;
          },
          () => {
            logger.debug('Pushed event to Redis.');
          }
        )
      )
    );
  };
};

type Deps = { redis: RedisClient; logger: Logger };

const process: (
  data: CloudEventData
) => RTE.ReaderTaskEither<Deps, Error, void> =
  ({ message: { attributes, data } }) =>
  (deps) =>
  () => {
    const spanCtx = attributes.googclient_OpenTelemetrySpanContext;

    const context = spanCtx
      ? api.trace.setSpanContext(api.context.active(), spanCtx)
      : api.context.active();

    return tracer.startActiveSpan(
      'forwardMessage',
      { kind: SpanKind.PRODUCER },
      context,
      async (span) => {
        if (attributes.eventType === 'Relation') {
          return E.of(undefined);
        }

        const { orderingKey } = attributes;
        const message = Buffer.from(data, 'base64').toString();

        const updatedAt =
          attributes.eventType === 'Delete'
            ? attributes.txnTimeStamp
            : attributes.updatedAt;

        const result = await push(orderingKey, updatedAt, message)(deps)();

        pipe(
          result,
          E.fold(
            (error) => {
              span.recordException(error);
              span.setStatus({
                code: SpanStatusCode.ERROR,
                message: error.message
              });
            },
            () => {
              span.setStatus({ code: SpanStatusCode.OK });
            }
          )
        );

        span.end();

        return result;
      }
    );
  };
