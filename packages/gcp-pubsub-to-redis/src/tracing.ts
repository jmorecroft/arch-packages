import { trace } from '@opentelemetry/api';
import { Resource } from '@opentelemetry/resources';
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';
import { NodeTracerProvider } from '@opentelemetry/sdk-trace-node';
import { registerInstrumentations } from '@opentelemetry/instrumentation';
import { BatchSpanProcessor } from '@opentelemetry/sdk-trace-base';
import { RedisInstrumentation } from '@opentelemetry/instrumentation-redis-4';
import { env } from 'process';
import { JaegerExporter } from '@opentelemetry/exporter-jaeger';
import { TraceExporter } from '@google-cloud/opentelemetry-cloud-trace-exporter';
import findPackageJson = require('find-package-json');

const packageJson = Array.from(findPackageJson()).at(0);

const serviceName = packageJson?.name ?? 'gcp-pubsub-to-redis';
const serviceVersion = packageJson?.version ?? 'unknown';

export const tracer = trace.getTracer(serviceName, serviceVersion);

if (env.OTEL_EXPORTER_GCLOUD || env.OTEL_EXPORTER_JAEGER_AGENT_HOST) {
  // Optionally register automatic instrumentation libraries
  registerInstrumentations({
    instrumentations: [
      new RedisInstrumentation({
        dbStatementSerializer: function (cmdName, cmdArgs) {
          return [cmdName, ...cmdArgs].join(' ');
        }
      })
    ]
  });

  const resource = Resource.default().merge(
    new Resource({
      [SemanticResourceAttributes.SERVICE_NAME]: serviceName,
      [SemanticResourceAttributes.SERVICE_VERSION]: serviceVersion
    })
  );

  const provider = new NodeTracerProvider({
    resource
  });

  const exporter = env.OTEL_EXPORTER_GCLOUD
    ? new TraceExporter()
    : new JaegerExporter();

  const processor = new BatchSpanProcessor(exporter);
  provider.addSpanProcessor(processor);
  provider.register();
}
