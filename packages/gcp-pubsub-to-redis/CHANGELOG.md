# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.9](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8...gcp-pubsub-to-redis@0.4.9) (2023-01-05)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.9...gcp-pubsub-to-redis@0.4.8) (2023-01-04)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.9](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.8...gcp-pubsub-to-redis@0.4.8-staging.9) (2022-12-13)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.8](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.7...gcp-pubsub-to-redis@0.4.8-staging.8) (2022-12-03)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.7](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.6...gcp-pubsub-to-redis@0.4.8-staging.7) (2022-12-03)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.6](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.5...gcp-pubsub-to-redis@0.4.8-staging.6) (2022-12-03)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.5](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.4...gcp-pubsub-to-redis@0.4.8-staging.5) (2022-12-02)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.4](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.3...gcp-pubsub-to-redis@0.4.8-staging.4) (2022-12-01)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.3](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.2...gcp-pubsub-to-redis@0.4.8-staging.3) (2022-11-29)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.2](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.1...gcp-pubsub-to-redis@0.4.8-staging.2) (2022-11-29)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.1](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.8-staging.0...gcp-pubsub-to-redis@0.4.8-staging.1) (2022-11-24)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.8-staging.0](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.7...gcp-pubsub-to-redis@0.4.8-staging.0) (2022-11-24)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.7](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.6...gcp-pubsub-to-redis@0.4.7) (2022-11-03)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.6](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.5...gcp-pubsub-to-redis@0.4.6) (2022-10-25)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.5](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.4...gcp-pubsub-to-redis@0.4.5) (2022-10-24)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.4](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.3...gcp-pubsub-to-redis@0.4.4) (2022-08-22)

**Note:** Version bump only for package gcp-pubsub-to-redis





## 0.4.3 (2022-08-13)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.2](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.1...gcp-pubsub-to-redis@0.4.2) (2022-07-21)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.4.1](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.4.0...gcp-pubsub-to-redis@0.4.1) (2022-07-21)

**Note:** Version bump only for package gcp-pubsub-to-redis





# [0.4.0](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.3.2...gcp-pubsub-to-redis@0.4.0) (2022-07-18)


### Features

* **@jmorecroft67/auth:** New lib for auth-related components. ([d8a56ca](https://gitlab.com/jmorecroft/arch-packages/commit/d8a56ca89966111d9df75001c99ed6db3fd9a641))





## [0.3.2](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.3.1...gcp-pubsub-to-redis@0.3.2) (2022-07-12)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.3.1](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.3.0...gcp-pubsub-to-redis@0.3.1) (2022-07-12)


### Bug Fixes

* Better retrieval of package info. ([11b2d90](https://gitlab.com/jmorecroft/arch-packages/commit/11b2d9036f0af677c104720419a100bdc34bc8fa))





# [0.3.0](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.2.2...gcp-pubsub-to-redis@0.3.0) (2022-07-12)


### Bug Fixes

* Added missing files. ([e2bdfc1](https://gitlab.com/jmorecroft/arch-packages/commit/e2bdfc122c2793ec6b4dd6c95ce64a78e5119eb0))


### Features

* Initial ALPHA support for OpenTelemetry in some packages. ([628006d](https://gitlab.com/jmorecroft/arch-packages/commit/628006daf4ce15e0337f53b9247ec3d248fb63dd))





## [0.2.2](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.2.1...gcp-pubsub-to-redis@0.2.2) (2022-07-11)

**Note:** Version bump only for package gcp-pubsub-to-redis





## [0.2.1](https://gitlab.com/jmorecroft/arch-packages/compare/gcp-pubsub-to-redis@0.2.0...gcp-pubsub-to-redis@0.2.1) (2022-07-11)

**Note:** Version bump only for package gcp-pubsub-to-redis





# 0.2.0 (2022-07-11)


### Features

* **@jmorecroft67/io-ts-types:** Added utils functions - withDefault, getEnvOrThrow. ([4caeb09](https://gitlab.com/jmorecroft/arch-packages/commit/4caeb097719d91dbaf79c367311986e0fa260891))
* **gcp-pubsub-to-redis:** Updated package.json with scripts for publishing Docker image. ([0ec423c](https://gitlab.com/jmorecroft/arch-packages/commit/0ec423c3c143641926450af3d69237f6fc28377b))





# 0.1.0 (2022-07-11)


### Features

* **@jmorecroft67/io-ts-types:** Added utils functions - withDefault, getEnvOrThrow. ([4caeb09](https://gitlab.com/jmorecroft/arch-packages/commit/4caeb097719d91dbaf79c367311986e0fa260891))
