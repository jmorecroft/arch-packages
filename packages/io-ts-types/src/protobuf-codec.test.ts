import 'jest-extended';
import * as SPC from './protobuf-codec';
import * as pb from 'protobufjs';
import * as E from 'fp-ts/Either';
import * as D from 'io-ts/Decoder';
import { pipe } from 'fp-ts/lib/function';

type TestData<T> = {
  codecMaker: SPC.ProtobufCodecMaker<T>;
  objects: T[];
};
describe(__filename, () => {
  const testData: [string, TestData<unknown>][] = [];

  const add =
    <T>(description: string, codecMaker: SPC.ProtobufCodecMaker<T>) =>
    (objects: T[]) =>
      testData.push([description, { codecMaker, objects }]);

  add(
    'simple partial',
    SPC.partial({
      myNum: SPC.number,
      myString: SPC.string,
      myBool: SPC.boolean,
      myDate: SPC.date,
      myLiteral: SPC.literal('hello', 'world')
    })
  )([
    {
      myNum: 123,
      myBool: true,
      myDate: new Date()
    },
    {
      myNum: 123,
      myString: 'hello world',
      myLiteral: 'world'
    }
  ]);

  add(
    'simple struct',
    SPC.struct({
      myNum: SPC.number,
      myString: SPC.string,
      myBool: SPC.boolean,
      myDate: SPC.date,
      myLiteral: SPC.literal(22, 33)
    })
  )([
    {
      myNum: 123,
      myBool: true,
      myDate: new Date(),
      myString: 'hello world',
      myLiteral: 22
    },
    {
      myNum: 123,
      myBool: true,
      myDate: new Date(),
      myString: 'hello world',
      myLiteral: 33
    }
  ]);

  add(
    'struct with nullables',
    SPC.struct({
      mynum: SPC.nullable(SPC.number),
      mystring: SPC.nullable(SPC.string),
      mybool: SPC.nullable(SPC.boolean),
      mydate: SPC.nullable(SPC.date)
    })
  )([
    {
      mynum: null,
      mybool: null,
      mydate: null,
      mystring: null
    },
    {
      mynum: 1,
      mybool: false,
      mydate: new Date(),
      mystring: 'hello'
    },
    {
      mynum: 1,
      mybool: null,
      mydate: null,
      mystring: 'hello'
    }
  ]);

  add(
    'intersection of struct and partial',
    pipe(
      SPC.struct({
        one: SPC.number,
        two: SPC.number
      }),
      SPC.intersect(
        SPC.partial({
          three: SPC.number,
          four: SPC.number
        })
      )
    )
  )([
    {
      one: 1,
      two: 2,
      four: 4
    }
  ]);

  const group1 = SPC.struct({
    one: SPC.number,
    two: SPC.number
  });
  const group2 = SPC.struct({
    three: SPC.number,
    four: SPC.number
  });
  const group3 = SPC.struct({
    five: pipe(group2, SPC.intersect(group1))
  });

  add(
    'intersect with same named property',
    pipe(
      SPC.struct({
        hello: SPC.partial({
          hi: SPC.string
        })
      }),
      SPC.intersect(
        SPC.partial({
          hello: SPC.struct({
            hi: SPC.string
          })
        })
      )
    )
  )([
    {
      hello: {
        hi: 'there'
      }
    }
  ]);

  add(
    'multiple intersects',
    pipe(group1, SPC.intersect(group2), SPC.intersect(group3))
  )([
    {
      one: 1,
      two: 2,
      three: 3,
      four: 4,
      five: {
        one: 11,
        two: 22,
        three: 33,
        four: 44
      }
    }
  ]);

  add(
    'intersect with struct later re-used',
    SPC.struct({
      one: pipe(group1, SPC.intersect(group2)),
      two: group1
    })
  )([
    {
      one: {
        one: 1,
        two: 2,
        three: 3,
        four: 4
      },
      two: {
        one: 11,
        two: 22
      }
    }
  ]);

  add(
    'intersects in various directions, plus nesting',
    SPC.struct({
      one: pipe(group1, SPC.intersect(group2)),
      two: pipe(group2, SPC.intersect(group1)),
      three: pipe(
        group1,
        SPC.intersect(
          SPC.struct({
            five: pipe(group1, SPC.intersect(group2))
          })
        )
      )
    })
  )([
    {
      one: {
        one: 1,
        two: 2,
        three: 3,
        four: 4
      },
      two: {
        one: 11,
        two: 22,
        three: 33,
        four: 44
      },
      three: {
        one: 111,
        two: 222,
        five: {
          one: 1111,
          two: 2222,
          three: 3333,
          four: 4444
        }
      }
    }
  ]);

  add(
    'struct of structs',
    SPC.struct({
      mystruct1: SPC.struct({
        mystring: SPC.nullable(SPC.string),
        mybool: SPC.nullable(SPC.boolean),
        mydate: SPC.nullable(SPC.date)
      }),
      mystruct2: SPC.struct({
        mystring: SPC.string,
        mybool: SPC.nullable(SPC.boolean),
        mydate: SPC.nullable(SPC.date)
      })
    })
  )([
    {
      mystruct1: {
        mybool: null,
        mydate: null,
        mystring: null
      },
      mystruct2: {
        mystring: 'hello',
        mybool: null,
        mydate: null
      }
    },
    {
      mystruct1: {
        mybool: false,
        mydate: new Date(),
        mystring: 'hello'
      },
      mystruct2: {
        mybool: true,
        mydate: new Date(),
        mystring: 'hello'
      }
    },
    {
      mystruct1: {
        mybool: null,
        mydate: null,
        mystring: 'hello'
      },
      mystruct2: {
        mybool: null,
        mydate: null,
        mystring: 'hello'
      }
    }
  ]);

  add('simple array', SPC.array(SPC.number))([[1, 2, 3, 5]]);

  add(
    'array of structs',
    SPC.array(SPC.struct({ hello: SPC.string }))
  )([[{ hello: 'world' }, { hello: 'goodbye' }]]);

  add(
    'array of nullable structs',
    SPC.array(SPC.nullable(SPC.struct({ hello: SPC.string })))
  )([[null, { hello: 'goodbye' }]]);

  add(
    'simple tuple',
    SPC.tuple(SPC.number, SPC.string, SPC.boolean)
  )([[1, 'hello', true]]);

  add(
    'tuple of nullables and structs',
    SPC.tuple(
      SPC.struct({
        hello: SPC.string
      }),
      SPC.nullable(SPC.string),
      SPC.boolean
    )
  )([[{ hello: 'x' }, null, true]]);

  add(
    'simple record',
    SPC.record(SPC.number)
  )([
    {
      one: 1,
      two: 2,
      three: 3
    }
  ]);

  add(
    'record of nullable structs',
    SPC.record(SPC.nullable(SPC.struct({ value: SPC.number })))
  )([
    {
      one: {
        value: 1
      },
      two: {
        value: 2
      },
      three: null
    }
  ]);

  type Node = {
    value: number;
    next: Node | null;
  };

  const linkedList: SPC.ProtobufCodecMaker<Node> = SPC.struct({
    value: SPC.number,
    next: SPC.nullable(SPC.lazy('', () => linkedList))
  });

  add(
    'recursive struct',
    linkedList
  )([
    {
      value: 1,
      next: null
    },
    {
      value: 1,
      next: {
        value: 2,
        next: {
          value: 3,
          next: null
        }
      }
    }
  ]);

  type RecursiveArray = RecursiveArray[];

  const recursiveArray: SPC.ProtobufCodecMaker<RecursiveArray> = SPC.array(
    SPC.lazy('', () => recursiveArray)
  );

  add('recursive array', recursiveArray)([[], [[], [[]]]]);

  type RecursiveTuple = [string, RecursiveTuple | null];

  const recursiveTuple: SPC.ProtobufCodecMaker<RecursiveTuple> = SPC.tuple(
    SPC.string,
    SPC.nullable(SPC.lazy('', () => recursiveTuple))
  );

  add(
    'recursive tuple',
    recursiveTuple
  )([
    ['hi there', null],
    ['hello', ['goodbye', null]],
    ['lets', ['go', ['deep!', null]]]
  ]);

  interface Record2<T> {
    [k: string]: T;
  }

  type RecursiveRecord = Record2<RecursiveRecord | null>;

  const recursiveRecord: SPC.ProtobufCodecMaker<RecursiveRecord> = SPC.record(
    SPC.nullable(SPC.lazy('', () => recursiveRecord))
  );

  add(
    'recursive record',
    recursiveRecord
  )([
    { 'hi there': null },
    { hello: { goodbye: null } },
    { lets: { go: { 'deep!': null } } }
  ]);

  add(
    'sum',
    SPC.sum('type')({
      circle: SPC.struct({
        type: SPC.literal('circle'),
        radius: SPC.number
      }),
      rectangle: SPC.struct({
        type: SPC.literal('rectangle'),
        length: SPC.number,
        width: SPC.number
      }),
      square: SPC.struct({
        type: SPC.literal('square'),
        side: SPC.number
      })
    })
  )([
    {
      type: 'circle',
      radius: 1111
    },
    {
      type: 'square',
      side: 2222
    }
  ]);

  it.each(testData)(
    'should encode then decode: %s',
    (_, { codecMaker, objects }) => {
      const codec = codecMaker(SPC.makeContext());

      objects.forEach((obj) => {
        const buf = codec.encodeDelimited(obj).finish();
        const obj2 = codec.decodeDelimited(buf);
        if (E.isLeft(obj2)) {
          console.log(D.draw(obj2.left));
        }
        expect(E.isRight(obj2)).toBeTruthy();
        expect((obj2 as E.Right<unknown>).right).toEqual(obj);
      });
    }
  );

  it.each(testData)(
    'should make compatible pb type: %s',
    (_, { codecMaker, objects }) => {
      const codec = codecMaker(SPC.makeContext());

      const root = new pb.Root();
      const type = root.lookupType(codec.addPbType(root));

      objects.forEach((obj) => {
        const buf = codec.encodeDelimited(obj).finish();
        const obj2 = type.decodeDelimited(buf);
        const buf2 = type.encodeDelimited(obj2).finish();
        const obj3 = codec.decodeDelimited(buf2);
        if (E.isLeft(obj3)) {
          console.log(D.draw(obj3.left));
        }
        expect(E.isRight(obj3)).toBeTruthy();
        expect((obj3 as E.Right<unknown>).right).toEqual(obj);
      });
    }
  );

  it('should fail on missing properties', () => {
    const codecMaker = SPC.struct({
      mynumber: SPC.number,
      mystring: SPC.string,
      mybool: SPC.boolean
    });

    const codecMaker2 = SPC.partial({
      mynumber: SPC.number,
      mystring: SPC.string,
      mybool: SPC.boolean
    });

    const codec = codecMaker(SPC.makeContext());
    const codec2 = codecMaker2(SPC.makeContext());
    const root = new pb.Root();
    const type = root.lookupType(codec2.addPbType(root));

    const buf = type.encodeDelimited({ mystring: 'hello world' }).finish();

    const obj = codec.decodeDelimited(buf);

    expect(E.isLeft(obj)).toBeTruthy();
    expect(D.draw((obj as E.Left<D.DecodeError>).left)).toEqual(
      `required property "mynumber"
└─ cannot decode undefined, should be defined
required property "mybool"
└─ cannot decode undefined, should be defined`
    );
  });

  it('should fail on bad sum', () => {
    const codecMaker = SPC.sum('type')({
      circle: SPC.struct({
        type: SPC.literal('circle'),
        radius: SPC.number
      }),
      square: SPC.struct({
        type: SPC.literal('square'),
        side: SPC.number
      })
    });

    const codecMaker2 = SPC.sum('type')({
      circle: pipe(
        SPC.struct({
          type: SPC.literal('circle')
        }),
        SPC.intersect(
          SPC.partial({
            radius: SPC.number
          })
        )
      ),
      square: SPC.struct({
        type: SPC.literal('square'),
        side: SPC.number
      })
    });

    const codec = codecMaker(SPC.makeContext());
    const codec2 = codecMaker2(SPC.makeContext());
    const root = new pb.Root();
    const type = root.lookupType(codec2.addPbType(root));

    const buf = type
      .encodeDelimited({ circle: { type: 'circle', side: 10 } })
      .finish();

    const obj = codec.decodeDelimited(buf);

    expect(E.isLeft(obj)).toBeTruthy();
    expect(D.draw((obj as E.Left<D.DecodeError>).left)).toEqual(
      `required property "radius"
└─ cannot decode undefined, should be defined`
    );
  });

  it('should fail on decimal parse', () => {
    const codecMaker = SPC.struct({
      mydecimal: SPC.decimal
    });

    const codec = codecMaker(SPC.makeContext());
    const root = new pb.Root();
    const type = root.lookupType(codec.addPbType(root));

    const buf = type.encodeDelimited({ mydecimal: 'not a decimal' }).finish();

    const obj = codec.decodeDelimited(buf);

    expect(E.isLeft(obj)).toBeTruthy();
    expect(D.draw((obj as E.Left<D.DecodeError>).left)).toEqual(
      expect.stringMatching(/cannot decode "not a decimal", should be decimal/)
    );
  });

  it('should truncate on int parse', () => {
    const codecMaker = SPC.struct({
      myint: SPC.int
    });

    const codec = codecMaker(SPC.makeContext());
    const root = new pb.Root();
    const type = root.lookupType(codec.addPbType(root));

    const buf = type.encodeDelimited({ myint: 123.4 }).finish();
    const obj = codec.decodeDelimited(buf);

    expect(E.isRight(obj)).toBeTruthy();
    expect((obj as E.Right<unknown>).right).toEqual({ myint: 123 });
  });

  it('should fail on date parse', () => {
    const codecMaker = SPC.struct({
      mydate: SPC.date
    });

    const codec = codecMaker(SPC.makeContext());
    const root = new pb.Root();
    const type = root.lookupType(codec.addPbType(root));

    const buf = type
      .encodeDelimited({
        mydate: -123
      })
      .finish();

    const obj = codec.decodeDelimited(buf);

    expect(E.isLeft(obj)).toBeTruthy();
    expect(D.draw((obj as E.Left<D.DecodeError>).left)).toEqual(
      expect.stringMatching(/cannot decode -?[0-9]+, should be date epoch/)
    );
  });
});
