/* eslint-disable @typescript-eslint/no-explicit-any */
import { Decimal } from './types';
import { Eq, struct } from 'fp-ts/lib/Eq';
import * as A from 'fp-ts/Array';
import * as S from 'fp-ts/string';
import * as N from 'fp-ts/number';
import * as B from 'fp-ts/boolean';
import * as D from 'fp-ts/Date';

export const eqNullable = <T>(eq: Eq<T>): Eq<T | null> => ({
  equals: (x, y) =>
    (x !== null && y !== null && eq.equals(x, y)) || (x === null && y === null)
});

export const eqUndefined = <T>(eq: Eq<T>): Eq<T | undefined> => ({
  equals: (x, y) => x === undefined || y === undefined || eq.equals(x, y)
});

export const eqNumber = N.Eq;
export const eqString = S.Eq;
export const eqBoolean = B.Eq;
export const eqDate = D.Eq;
export const eqDecimal: Eq<Decimal> = { equals: (x, y) => x.eq(y as any) };
export const eqArray = A.getEq;
export const eqStruct = struct;
export const eqPartial: <A>(props: { [K in keyof A]: Eq<A[K]> }) => Eq<
  Partial<{ [K in keyof A]: A[K] }>
> = (eqs) =>
  eqStruct(
    Object.entries(eqs).reduce((prev, [key, eq]) => {
      return {
        ...prev,
        [key]: eqUndefined(eq as Eq<unknown>)
      };
    }, {})
  ) as any;

export const eqNullableNumber = eqNullable(eqNumber);
export const eqNullableString = eqNullable(eqString);
export const eqNullableBoolean = eqNullable(eqBoolean);
export const eqNullableDate = eqNullable(eqDate);
export const eqNullableDecimal = eqNullable(eqDecimal);
export const eqNullableArray: <A>(eq: Eq<A>) => Eq<A[] | null> = (eq) =>
  eqNullable(eqArray(eq));
export const eqNullableStruct: <A>(eqs: { [K in keyof A]: Eq<A[K]> }) => Eq<
  { readonly [K_1 in keyof A]: A[K_1] } | null
> = (eqs) => eqNullable(eqStruct(eqs));
export const eqNullablePartial: <A>(props: { [K in keyof A]: Eq<A[K]> }) => Eq<
  Partial<{ [K in keyof A]: A[K] } | null>
> = (eqs) => eqNullable(eqPartial(eqs));
