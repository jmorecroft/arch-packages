import * as D from 'io-ts/Decoder';
import * as G from 'io-ts/Guard';
import * as G2 from './guard';
import { Schemable2C } from './schemable';
import { Decimal, Int } from './types';
import DecimalJs from 'decimal.js';

export const decimal: D.Decoder<unknown, Decimal> = {
  decode: (i) => {
    if (G2.decimal.is(i)) {
      return D.success(i);
    }
    if (G.string.is(i) || G.number.is(i)) {
      try {
        const d = new DecimalJs(i);
        if (!d.isNaN()) {
          return D.success(d);
        }
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }
    return D.failure(i, 'Decimal');
  }
};

export const date: D.Decoder<unknown, Date> = {
  decode: (i) => {
    if (G2.date.is(i)) {
      return D.success(i);
    }
    if (G.number.is(i) || G.string.is(i)) {
      const dt = new Date(i);
      if (!isNaN(dt.getTime())) {
        return D.success(dt);
      }
    }
    return D.failure(i, 'Date');
  }
};

export const int: D.Decoder<unknown, Int> = {
  decode: (i) => {
    if (G2.int.is(i)) {
      return D.success(i);
    }
    if (G.string.is(i)) {
      const i2 = Number(i);
      if (G2.int.is(i2)) {
        return D.success(i2);
      }
    }
    return D.failure(i, 'Int');
  }
};

export const Schemable: Schemable2C<D.URI> = {
  ...D.Schemable,
  date,
  decimal,
  int
};
