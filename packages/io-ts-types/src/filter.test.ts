import * as SL from './schemable-light';
import * as S from './schemas';
import * as F from './filter';
import * as E from 'fp-ts/Either';
import Decimal from 'decimal.js';
import { pipe } from 'fp-ts/lib/function';
import { Int } from './types';

describe(__filename, () => {
  const wrap: <T>(
    filter: F.Filter<T>
  ) => (
    algType: 'failFast' | 'checkAll'
  ) => (spec: S.Spec<T>, obj: T) => E.Either<string[], string[]> =
    (filter) => (algType) => (spec, obj) =>
      pipe(
        filter(algType)(spec, obj)(),
        E.bimap(
          (i) => i.map((j) => j()),
          (i) => i.map((j) => j())
        )
      );

  it('filter should fail all checks', () => {
    const personSchema = SL.make((S) =>
      S.struct({
        id: S.int,
        name: S.string,
        favouriteNumber: S.literal(7, 42),
        savings: S.decimal,
        pets: S.array(S.string),
        gender: S.nullable(S.literal('male', 'female')),
        died: S.nullable(S.date),
        createdAt: S.date
      })
    );

    type Person = SL.TypeOf<typeof personSchema>;
    type Spec = S.Spec<Person>;

    const spec: Spec = {
      id: {
        not: {
          equals: 123 as Int
        }
      },
      name: {
        startsWith: 'B'
      },
      favouriteNumber: {
        notIn: [42]
      },
      savings: {
        gt: new Decimal(100)
      },
      pets: {
        hasEvery: ['rex', 'bluey'],
        hasSome: ['bingo', 'louie']
      },
      gender: {
        in: ['female']
      },
      died: {
        equals: null
      },
      createdAt: {
        gt: new Date(Date.UTC(2000, 1, 1))
      },
      AND: [
        {
          id: {
            lt: 50 as Int
          }
        },
        {
          id: {
            gt: 0 as Int
          }
        }
      ],
      OR: [
        {
          name: {
            in: ['Betty', 'Beatrice']
          }
        },
        {
          name: {
            in: ['Bianca', 'Barb']
          }
        }
      ],
      NOT: [
        {
          gender: {
            in: ['female']
          }
        },
        {
          savings: {
            gt: new Decimal(200)
          }
        }
      ]
    };

    const filter = wrap(SL.interpreter(F.Schemable)(personSchema));

    const alan: Person = {
      id: 123 as Int,
      name: 'Alan',
      favouriteNumber: 42,
      savings: new Decimal(99.9),
      pets: ['rex', 'sparkle'],
      gender: 'male',
      died: new Date(Date.UTC(2020, 1, 1)),
      createdAt: new Date(Date.UTC(1999, 1, 1))
    };

    const result = filter('checkAll')(spec, alan);
    const result2 = filter('failFast')(spec, alan);

    expect(result).toEqual(
      E.left([
        'id: 123 = 123',
        'name: Alan does not start with B',
        'favouriteNumber: 42 in 42',
        'savings: 99.9 !> 100',
        'pets: rex,sparkle does not have at least one item in bingo,louie',
        'pets: rex,sparkle does not have every item in rex,bluey',
        'gender: male not in female',
        'died: Sat Feb 01 2020 00:00:00 GMT+0000 (Coordinated Universal Time) != null',
        'createdAt: Mon Feb 01 1999 00:00:00 GMT+0000 (Coordinated Universal Time) !> Tue Feb 01 2000 00:00:00 GMT+0000 (Coordinated Universal Time)',
        'id: 123 !< 50',
        'name: Alan not in Betty,Beatrice',
        'name: Alan not in Bianca,Barb'
      ])
    );
    expect(result2).toEqual(E.left(['id: 123 = 123']));
  });

  it('filter should pass all checks', () => {
    const personSchema = SL.make((S) =>
      S.struct({
        id: S.int,
        name: S.string,
        favouriteNumber: S.literal(7, 42),
        savings: S.decimal,
        pets: S.array(S.string),
        gender: S.nullable(S.literal('male', 'female')),
        died: S.nullable(S.date),
        createdAt: S.date
      })
    );

    type Person = SL.TypeOf<typeof personSchema>;
    type Spec = S.Spec<Person>;

    const spec: Spec = {
      id: {
        not: {
          equals: 123 as Int
        }
      },
      name: {
        startsWith: 'B'
      },
      favouriteNumber: {
        not: {
          in: [7]
        }
      },
      savings: {
        gt: new Decimal(100)
      },
      pets: {
        hasSome: ['bingo', 'louie'],
        hasEvery: ['bingo', 'albie']
      },
      gender: {
        in: ['female']
      },
      died: {
        equals: null
      },
      createdAt: {
        gt: new Date(Date.UTC(2000, 1, 1))
      },
      AND: [
        {
          id: {
            lt: 50 as Int
          }
        },
        {
          id: {
            gt: 0 as Int
          }
        }
      ],
      OR: [
        {
          name: {
            in: ['Betty', 'Beatrice']
          }
        },
        {
          name: {
            in: ['Bianca', 'Barb']
          }
        }
      ],
      NOT: [
        {
          gender: {
            in: ['female']
          }
        },
        {
          savings: {
            gt: new Decimal(200)
          }
        }
      ]
    };

    const filter = wrap(SL.interpreter(F.Schemable)(personSchema));

    const barb: Person = {
      id: 1 as Int,
      name: 'Barb',
      favouriteNumber: 42,
      savings: new Decimal(100.1),
      pets: ['bingo', 'albie'],
      gender: 'female',
      died: null,
      createdAt: new Date(Date.UTC(2001, 1, 1))
    };

    const result = filter('checkAll')(spec, barb);
    const result2 = filter('failFast')(spec, barb);

    expect(result).toEqual(result2);
    expect(result).toEqual(
      E.right([
        'id: 1 != 123',
        'name: Barb starts with B',
        'favouriteNumber: 42 not in 7',
        'savings: 100.1 > 100',
        'pets: bingo,albie has at least one item in bingo,louie',
        'pets: bingo,albie has every item in bingo,albie',
        'gender: female in female',
        'died: null = null',
        'createdAt: Thu Feb 01 2001 00:00:00 GMT+0000 (Coordinated Universal Time) > Tue Feb 01 2000 00:00:00 GMT+0000 (Coordinated Universal Time)',
        'id: 1 < 50',
        'id: 1 > 0',
        'name: Barb in Bianca,Barb',
        'savings: 100.1 !> 200'
      ])
    );
  });

  it('filter should handle nested conditions', () => {
    const personSchema = SL.make((S) =>
      S.struct({
        id: S.int,
        name: S.string,
        savings: S.decimal,
        pets: S.array(S.string),
        gender: S.nullable(S.literal('male', 'female')),
        died: S.nullable(S.date),
        createdAt: S.date
      })
    );

    type Person = SL.TypeOf<typeof personSchema>;
    type Spec = S.Spec<Person>;

    // Either name is "B*b", or has a pet named "Bingo", or is dead.
    const spec: Spec = {
      OR: [
        {
          AND: [
            {
              name: {
                startsWith: 'B'
              }
            },
            {
              name: {
                endsWith: 'b'
              }
            }
          ]
        },
        {
          OR: [
            {
              pets: {
                has: 'Bingo'
              }
            },
            {
              died: {
                not: {
                  equals: null
                }
              }
            }
          ]
        }
      ]
    };

    const filter = wrap(SL.interpreter(F.Schemable)(personSchema));

    const barb: Person = {
      id: 1 as Int,
      name: 'Barb',
      savings: new Decimal(100.1),
      pets: ['Albie'],
      gender: 'female',
      died: null,
      createdAt: new Date(Date.UTC(2001, 1, 1))
    };

    const tom: Person = {
      id: 1 as Int,
      name: 'Tom',
      savings: new Decimal(100.1),
      pets: ['Bingo'],
      gender: 'female',
      died: null,
      createdAt: new Date(Date.UTC(2001, 1, 1))
    };

    const dick: Person = {
      id: 1 as Int,
      name: 'Dick',
      savings: new Decimal(100.1),
      pets: [],
      gender: 'female',
      died: new Date(Date.UTC(2022, 1, 1)),
      createdAt: new Date(Date.UTC(2001, 1, 1))
    };

    const harry: Person = {
      id: 1 as Int,
      name: 'Harry',
      savings: new Decimal(100.1),
      pets: [],
      gender: 'female',
      died: null,
      createdAt: new Date(Date.UTC(2001, 1, 1))
    };

    const result1 = filter('checkAll')(spec, barb);
    const result2 = filter('checkAll')(spec, tom);
    const result3 = filter('checkAll')(spec, dick);
    const result4 = filter('checkAll')(spec, harry);

    expect(result1).toEqual(
      E.right(['name: Barb starts with B', 'name: Barb ends with b'])
    );
    expect(result2).toEqual(E.right(['pets: Bingo has Bingo']));
    expect(result3).toEqual(
      E.right([
        'died: Tue Feb 01 2022 00:00:00 GMT+0000 (Coordinated Universal Time) != null'
      ])
    );
    expect(result4).toEqual(
      E.left([
        'name: Harry does not start with B',
        'name: Harry does not end with b',
        'pets:  does not have Bingo',
        'died: null = null'
      ])
    );
  });
});
