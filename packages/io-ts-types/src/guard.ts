import { Decimal } from 'decimal.js';
import * as G from 'io-ts/Guard';
import { Schemable1 } from './schemable';
import { Int } from './types';

export const decimal: G.Guard<unknown, Decimal> = {
  is: (i): i is Decimal => Decimal.isDecimal(i)
};

export const date: G.Guard<unknown, Date> = {
  is: (i): i is Date => i instanceof Date
};

export const int: G.Guard<unknown, Int> = {
  is: (i): i is Int => Number.isInteger(i)
};

export const Schemable: Schemable1<G.URI> = {
  ...G.Schemable,
  int,
  date,
  decimal
};
