/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  eqNullable,
  eqNullableArray,
  eqNullableDate,
  eqNullableDecimal
} from './eq';
import { NullableLiteralSpec, Spec } from './schemas';
import * as SCL from './schemable-light';
import { Literal, memoize } from 'io-ts/lib/Schemable';
import * as E from 'fp-ts/Either';
import * as IE from 'fp-ts/IOEither';
import { Lazy, pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/Array';
import { sequenceT } from 'fp-ts/lib/Apply';
import { Int, Decimal } from './types';

const checkAll = (value: FilterResult, ...values: FilterResult[]) =>
  pipe(
    sequenceT(IE.getApplicativeIOValidation(A.getSemigroup<Lazy<string>>()))(
      value,
      ...values
    ),
    IE.map((items) => items.flat())
  );

const failFast = (value: FilterResult, ...values: FilterResult[]) =>
  pipe(
    sequenceT(IE.ApplicativePar)(value, ...values),
    IE.map((items) => items.flat())
  );

const alg = (algType: AlgType) =>
  algType === 'checkAll' ? checkAll : failFast;

export type AlgType = 'checkAll' | 'failFast';

export interface Filter<T> {
  (algType: AlgType): (spec: Spec<T>, value: T) => FilterResult;
}

export type FilterResult = IE.IOEither<Lazy<string>[], Lazy<string>[]>;

export type TypeOf<T> = T extends Filter<infer A> ? A : never;

export const URI = 'io-ts/Filter';

export type URI = typeof URI;

declare module 'fp-ts/lib/HKT' {
  interface URItoKind<A> {
    readonly [URI]: Filter<A>;
  }
}

const check = (
  check: boolean | undefined,
  message: () => string,
  error: () => string
) =>
  check === undefined
    ? E.right([])
    : check
    ? E.right([message])
    : E.left([error]);

export const number: Filter<number> & Filter<number | null> =
  (algType) => (spec, value) => {
    return alg(algType)(
      () =>
        check(
          spec.equals === undefined ? undefined : spec.equals === value,
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        check(
          spec.gt === undefined ? undefined : value !== null && value > spec.gt,
          () => `${value} > ${spec.gt}`,
          () => `${value} !> ${spec.gt}`
        ),
      () =>
        check(
          spec.gte === undefined
            ? undefined
            : value !== null && value >= spec.gte,
          () => `${value} >= ${spec.gte}`,
          () => `${value} !>= ${spec.gte}`
        ),
      () =>
        check(
          spec.lt === undefined ? undefined : value !== null && value < spec.lt,
          () => `${value} < ${spec.lt}`,
          () => `${value} !< ${spec.lt}`
        ),
      () =>
        check(
          spec.lte === undefined
            ? undefined
            : value !== null && value <= spec.lte,
          () => `${value} <= ${spec.lte}`,
          () => `${value} !<= ${spec.lte}`
        ),
      () =>
        check(
          spec.in === undefined
            ? undefined
            : value !== null && spec.in.includes(value),
          () => `${value} in ${spec.in}`,
          () => `${value} not in ${spec.in}`
        ),
      () =>
        check(
          spec.notIn === undefined
            ? undefined
            : value === null || !spec.notIn.includes(value),
          () => `${value} not in ${spec.notIn}`,
          () => `${value} in ${spec.notIn}`
        ),
      () =>
        spec.not === undefined
          ? E.right([])
          : spec.not === null
          ? value === null
            ? E.left([() => 'null'])
            : E.right([() => `${value} not null`])
          : pipe(number(algType)(spec.not as any, value as any), IE.swap)()
    );
  };

export const int: Filter<Int> & Filter<Int | null> = number;

const stringCompare = (insensitive?: boolean) => (x: string, y: string) =>
  x.localeCompare(
    y,
    undefined,
    insensitive ? { sensitivity: 'base' } : undefined
  );

const lowerCase = memoize(
  <T extends string | null | undefined>(s: T): T => s?.toLocaleLowerCase() as T
);

export const string: Filter<string> & Filter<string | null> =
  (algType) => (spec, value) => {
    const insensitive = spec.mode === 'insensitive';

    const compare = stringCompare(insensitive);

    const { equals } = eqNullable({
      equals: (x: string, y: string) => compare(x, y) === 0
    });

    return alg(algType)(
      () =>
        check(
          spec.equals === undefined ? undefined : equals(spec.equals, value),
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        check(
          spec.startsWith === undefined
            ? undefined
            : value !== null &&
                (insensitive
                  ? lowerCase(value).startsWith(lowerCase(spec.startsWith))
                  : value.startsWith(spec.startsWith)),
          () => `${value} starts with ${spec.startsWith}`,
          () => `${value} does not start with ${spec.startsWith}`
        ),
      () =>
        check(
          spec.endsWith === undefined
            ? undefined
            : value !== null &&
                (insensitive
                  ? lowerCase(value).endsWith(lowerCase(spec.endsWith))
                  : value.endsWith(spec.endsWith)),
          () => `${value} ends with ${spec.endsWith}`,
          () => `${value} does not end with ${spec.endsWith}`
        ),
      () =>
        check(
          spec.contains === undefined
            ? undefined
            : value !== null &&
                (insensitive
                  ? lowerCase(value).includes(lowerCase(spec.contains))
                  : value.includes(spec.contains)),
          () => `${value} contains ${spec.contains}`,
          () => `${value} does not contain ${spec.contains}`
        ),
      () =>
        check(
          spec.gt === undefined
            ? undefined
            : value !== null && compare(value, spec.gt) > 0,
          () => `${value} > ${spec.gt}`,
          () => `${value} !> ${spec.gt}`
        ),
      () =>
        check(
          spec.gte === undefined
            ? undefined
            : value !== null && compare(value, spec.gte) >= 0,
          () => `${value} >= ${spec.gte}`,
          () => `${value} !>= ${spec.gte}`
        ),
      () =>
        check(
          spec.lt === undefined
            ? undefined
            : value !== null && compare(value, spec.lt) < 0,
          () => `${value} < ${spec.lt}`,
          () => `${value} !< ${spec.lt}`
        ),
      () =>
        check(
          spec.lte === undefined
            ? undefined
            : value !== null && compare(value, spec.lte) >= 0,
          () => `${value} <= ${spec.lte}`,
          () => `${value} !<= ${spec.lte}`
        ),
      () =>
        check(
          spec.in === undefined
            ? undefined
            : value !== null && spec.in.some((i) => equals(i, value)),
          () => `${value} in ${spec.in}`,
          () => `${value} not in ${spec.in}`
        ),
      () =>
        check(
          spec.notIn === undefined
            ? undefined
            : value === null || !spec.notIn.some((i) => equals(i, value)),
          () => `${value} not in ${spec.notIn}`,
          () => `${value} in ${spec.notIn}`
        ),
      () =>
        spec.not === undefined
          ? E.right([])
          : spec.not === null
          ? value === null
            ? E.left([() => 'null'])
            : E.right([() => `${value} not null`])
          : pipe(string(algType)(spec.not as any, value as any), IE.swap)()
    );
  };

export const boolean: Filter<boolean> & Filter<boolean | null> =
  (algType) => (spec, value) => {
    return alg(algType)(
      () =>
        check(
          spec.equals === undefined ? undefined : spec.equals === value,
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        spec.not === undefined
          ? E.right([])
          : spec.not === null
          ? value === null
            ? E.left([() => 'null'])
            : E.right([() => `${value} not null`])
          : pipe(boolean(algType)(spec.not as any, value as any), IE.swap)()
    );
  };

export const date: Filter<Date> & Filter<Date | null> =
  (algType) => (spec, value) => {
    const { equals } = eqNullableDate;
    return alg(algType)(
      () =>
        check(
          spec.equals === undefined ? undefined : equals(spec.equals, value),
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        check(
          spec.gt === undefined
            ? undefined
            : value !== null && value.getTime() > spec.gt.getTime(),
          () => `${value} > ${spec.gt}`,
          () => `${value} !> ${spec.gt}`
        ),
      () =>
        check(
          spec.gte === undefined
            ? undefined
            : value !== null && value.getTime() >= spec.gte.getTime(),
          () => `${value} >= ${spec.gte}`,
          () => `${value} !>= ${spec.gte}`
        ),
      () =>
        check(
          spec.lt === undefined
            ? undefined
            : value !== null && value.getTime() < spec.lt.getTime(),
          () => `${value} < ${spec.lt}`,
          () => `${value} !< ${spec.lt}`
        ),
      () =>
        check(
          spec.lte === undefined
            ? undefined
            : value !== null && value.getTime() <= spec.lte.getTime(),
          () => `${value} <= ${spec.lte}`,
          () => `${value} !<= ${spec.lte}`
        ),
      () =>
        check(
          spec.in === undefined
            ? undefined
            : value !== null && spec.in.some((i) => equals(i, value)),
          () => `${value} in ${spec.in}`,
          () => `${value} not in ${spec.in}`
        ),
      () =>
        check(
          spec.notIn === undefined
            ? undefined
            : value === null || !spec.notIn.some((i) => equals(i, value)),
          () => `${value} not in ${spec.notIn}`,
          () => `${value} in ${spec.notIn}`
        ),
      () =>
        spec.not === undefined
          ? E.right([])
          : spec.not === null
          ? value === null
            ? E.left([() => 'null'])
            : E.right([() => `${value} not null`])
          : pipe(date(algType)(spec.not as any, value as any), IE.swap)()
    );
  };

export const decimal: Filter<Decimal> & Filter<Decimal | null> =
  (algType) => (spec, value) => {
    const { equals } = eqNullableDecimal;
    return alg(algType)(
      () =>
        check(
          spec.equals === undefined ? undefined : equals(spec.equals, value),
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        check(
          spec.gt === undefined
            ? undefined
            : value !== null && value.gt(spec.gt as any),
          () => `${value} > ${spec.gt}`,
          () => `${value} !> ${spec.gt}`
        ),
      () =>
        check(
          spec.gte === undefined
            ? undefined
            : value !== null && value.gte(spec.gte as any),
          () => `${value} >= ${spec.gte}`,
          () => `${value} !>= ${spec.gte}`
        ),
      () =>
        check(
          spec.lt === undefined
            ? undefined
            : value !== null && value.lt(spec.lt as any),
          () => `${value} < ${spec.lt}`,
          () => `${value} !< ${spec.lt}`
        ),
      () =>
        check(
          spec.lte === undefined
            ? undefined
            : value !== null && value.lte(spec.lte as any),
          () => `${value} <= ${spec.lte}`,
          () => `${value} !<= ${spec.lte}`
        ),
      () =>
        check(
          spec.in === undefined
            ? undefined
            : value !== null && spec.in.some((i) => equals(i, value)),
          () => `${value} in ${spec.in}`,
          () => `${value} not in ${spec.in}`
        ),
      () =>
        check(
          spec.notIn === undefined
            ? undefined
            : value === null || !spec.notIn.some((i) => equals(i, value)),
          () => `${value} not in ${spec.notIn}`,
          () => `${value} in ${spec.notIn}`
        ),
      () =>
        spec.not === undefined
          ? E.right([])
          : spec.not === null
          ? value === null
            ? E.left([() => 'null'])
            : E.right([() => `${value} not null`])
          : pipe(decimal(algType)(spec.not as any, value as any), IE.swap)()
    );
  };

const array: <A>(item: Filter<A>) => Filter<Array<A>> =
  (fn) => (algType) => (spec, value) => {
    type A = typeof value[number];
    const equals = (x: A, y: A): boolean =>
      E.isRight(fn('failFast')({ equals: x } as any, y)());
    const eqArray = eqNullableArray({ equals });
    return alg(algType)(
      () =>
        check(
          spec.equals === undefined
            ? undefined
            : eqArray.equals(spec.equals, value),
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        check(
          spec.has === undefined
            ? undefined
            : // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
              value !== null && value.some((i) => equals(i, spec.has!)),
          () => `${value} has ${spec.has}`,
          () => `${value} does not have ${spec.has}`
        ),
      () =>
        check(
          spec.hasSome === undefined
            ? undefined
            : value !== null &&
                spec.hasSome.some((i) => value.some((j) => equals(i, j))),
          () => `${value} has at least one item in ${spec.hasSome}`,
          () => `${value} does not have at least one item in ${spec.hasSome}`
        ),
      () =>
        check(
          spec.hasEvery === undefined
            ? undefined
            : value !== null &&
                spec.hasEvery.every((i) => value.some((j) => equals(i, j))),
          () => `${value} has every item in ${spec.hasEvery}`,
          () => `${value} does not have every item in ${spec.hasEvery}`
        ),
      () =>
        check(
          spec.isEmpty === undefined ? undefined : value?.length === 0,
          () => `${value} is empty`,
          () => `${value} is not empty`
        )
    );
  };

const literalAny =
  (...values: readonly any[]) =>
  (algType: AlgType) =>
  (spec: NullableLiteralSpec<any>, value: any): FilterResult => {
    return alg(algType)(
      () =>
        check(
          spec.equals === undefined ? undefined : spec.equals === value,
          () => `${value} = ${spec.equals}`,
          () => `${value} != ${spec.equals}`
        ),
      () =>
        check(
          spec.in === undefined
            ? undefined
            : value !== null && spec.in.some((i) => i === value),
          () => `${value} in ${spec.in}`,
          () => `${value} not in ${spec.in}`
        ),
      () =>
        check(
          spec.notIn === undefined
            ? undefined
            : value === null || !spec.notIn.some((i) => i === value),
          () => `${value} not in ${spec.notIn}`,
          () => `${value} in ${spec.notIn}`
        ),
      () =>
        spec.not === undefined
          ? E.right([])
          : spec.not === null
          ? value === null
            ? E.left([() => 'null'])
            : E.right([() => `${value} not null`])
          : pipe(
              literalAny(...values)(algType)(spec.not as any, value as any),
              IE.swap
            )()
    );
  };

export const literal: <A extends readonly [Literal, ...Array<Literal>]>(
  ...values: A
) => Filter<A[number]> = literalAny as any;

export const struct: <A>(properties: {
  [K in keyof A]: Filter<A[K]>;
}) => Filter<{
  [K in keyof A]: A[K];
}> &
  Filter<
    | {
        [K in keyof A]: A[K];
      }
    | null
  > = (props) => (algType) => (s, value) => {
  if (value === null) {
    if (s === null) {
      return IE.right([() => 'null']);
    }
    return IE.left([() => 'null']);
  }

  if (s === null) {
    return IE.left([() => 'not null']);
  }

  const spec = s as Spec<Record<keyof any, unknown>>;

  const result = Object.keys(value).reduce<FilterResult>((prev, k) => {
    const key = k as keyof typeof value;

    const filter = props[key];

    if (filter === undefined || value[key] === undefined) {
      return prev;
    }

    const spec2: any = (spec as any)[key];
    if (spec2 === undefined) {
      return prev;
    }

    return alg(algType)(
      prev,
      pipe(
        filter(algType)(spec2, value[key]),
        IE.bimap(
          (e) => e.map((i) => () => `${key as string}: ${i()}`),
          (a) => a.map((i) => () => `${key as string}: ${i()}`)
        )
      )
    );
  }, IE.right([]));

  const andOrNotResults: FilterResult[] = [];

  if (spec.AND && spec.AND.length > 0) {
    andOrNotResults.push(
      ...spec.AND.map((s) => struct(props)(algType)(s as any, value))
    );
  }

  if (spec.OR && spec.OR.length > 0) {
    andOrNotResults.push(
      pipe(
        alg(algType)(
          IE.right([]),
          ...spec.OR.map((s) =>
            pipe(struct(props)(algType)(s as any, value), IE.swap)
          )
        ),
        IE.swap
      )
    );
  }

  if (spec.NOT && spec.NOT.length > 0) {
    andOrNotResults.push(
      pipe(
        alg(algType)(
          IE.right([]),
          ...spec.NOT.map((s) => struct(props)(algType)(s as any, value))
        ),
        IE.swap
      )
    );
  }

  return alg(algType)(result, ...andOrNotResults);
};

export const nullable = <A>(or: Filter<A>) => or as Filter<A | null>;

export const Schemable: SCL.SchemableLight1<URI> = {
  number,
  string,
  boolean,
  date,
  decimal,
  int,
  literal,
  nullable,
  array,
  struct
};
