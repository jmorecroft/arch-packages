import { pipe } from 'fp-ts/lib/function';
import * as D from 'io-ts/Decoder';
import * as E from 'fp-ts/Either';

export const withDefault: <T>(
  decoder: D.Decoder<unknown, T>
) => (def: T) => D.Decoder<unknown, T> = (decoder) => (def) =>
  pipe(
    {
      decode: (i: unknown) =>
        i === undefined ? E.right(def) : E.left(D.error(i, 'undefined'))
    },
    D.alt(() => decoder)
  );
