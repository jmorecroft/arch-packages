import { pipe } from 'fp-ts/lib/function';
import * as D from 'io-ts/Decoder';
import * as E from 'fp-ts/Either';
import { env } from 'process';

export const getEnvOrThrow = <T>(decoder: D.Decoder<unknown, T>): T =>
  pipe(
    decoder.decode(env),
    E.fold(
      (e) => {
        throw new Error(D.draw(e));
      },
      (env) => env
    )
  );
