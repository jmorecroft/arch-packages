import DecimalJs from 'decimal.js';
import { Decimal as PrismaDecimal } from '@prisma/client/runtime';

export interface IntBrand {
  readonly Int: unique symbol;
}

export type Int = number & IntBrand;

export type Decimal = DecimalJs | PrismaDecimal.Instance;
