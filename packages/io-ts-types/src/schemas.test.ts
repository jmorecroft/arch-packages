import { interpreter } from './schemable';
import * as SS from './schemas';
import * as SC from './schemable';
import * as De from './decoder';
import * as En from './encoder';
import * as D from 'io-ts/Decoder';
import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { Int } from './types';
import Decimal from 'decimal.js';

describe(__filename, () => {
  it('spec schema should handle valid input', () => {
    const { spec } = SS.struct({
      id: SS.int,
      name: SS.string,
      savings: SS.decimal,
      pets: SS.array(SS.string),
      gender: SS.nullable(SS.literal('male', 'female')),
      died: SS.nullable(SS.date),
      createdAt: SS.date
    });

    type Spec = SC.TypeOf<typeof spec>;
    const i: Spec = {
      id: {
        not: {
          equals: 123 as Int
        }
      },
      name: {
        startsWith: 'B'
      },
      savings: {
        gt: new Decimal(100)
      },
      pets: {
        hasSome: ['bingo', 'louie']
      },
      gender: {
        in: ['female']
      },
      died: {
        equals: null
      },
      createdAt: {
        gt: new Date(Date.UTC(2000, 1, 1))
      }
    };

    const { decode } = interpreter(De.Schemable)(spec);
    const { encode } = interpreter(En.Schemable)(spec);

    const a = decode(i);

    expect(a).toEqual(E.right(i));
    expect(encode(i)).toEqual({
      ...i,
      savings: {
        gt: '100'
      },
      createdAt: {
        gt: Date.UTC(2000, 1, 1)
      }
    });
  });

  it('string spec schema should handle invalid input', () => {
    const { spec } = SS.struct({
      id: SS.int,
      name: SS.string,
      savings: SS.decimal,
      pets: SS.array(SS.string),
      gender: SS.nullable(SS.literal('male', 'female')),
      died: SS.nullable(SS.date),
      createdAt: SS.date
    });

    const i = {
      id: {
        not: {
          equals: 123.5
        }
      },
      name: {
        startsWith: 1
      },
      savings: {
        gt: 'large number'
      },
      pets: {
        hasSome: [123]
      },
      gender: {
        in: ['nonbinary']
      },
      died: {
        equals: new Decimal(100)
      },
      createdAt: {
        gt: true
      }
    };

    const { decode } = interpreter(De.Schemable)(spec);

    const a = pipe(decode(i), E.mapLeft(D.draw));

    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode 123.5, should be Int'))
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode 1, should be string'))
    );
    expect(a).toEqual(
      E.left(
        expect.stringContaining(
          'cannot decode "large number", should be Decimal'
        )
      )
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode 123, should be string'))
    );
    expect(a).toEqual(
      E.left(
        expect.stringContaining(
          'cannot decode "nonbinary", should be "male" | "female"'
        )
      )
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode "100", should be Date'))
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode true, should be Date'))
    );
  });

  it('item schema should handle valid input', () => {
    const { item } = SS.struct({
      id: SS.int,
      name: SS.string,
      favouriteNumber: SS.literal(7, 42),
      savings: SS.decimal,
      pets: SS.array(SS.string),
      gender: SS.nullable(SS.literal('male', 'female')),
      died: SS.nullable(SS.date),
      createdAt: SS.date
    });

    type Item = SC.TypeOf<typeof item>;
    const i: Item = {
      id: 123 as Int,
      name: 'Jo',
      favouriteNumber: 42,
      savings: new Decimal(100),
      pets: ['milo', 'otis'],
      gender: 'male',
      died: null,
      createdAt: new Date(Date.UTC(2000, 1, 1))
    };

    const { decode } = interpreter(De.Schemable)(item);
    const { encode } = interpreter(En.Schemable)(item);

    const a = decode(i);

    expect(a).toEqual(E.right(i));
    expect(encode(i)).toEqual({
      ...i,
      savings: '100',
      createdAt: Date.UTC(2000, 1, 1)
    });
  });

  it('item schema should handle invalid input', () => {
    const { item } = SS.struct({
      id: SS.int,
      name: SS.string,
      favouriteNumber: SS.literal(7, 42),
      savings: SS.decimal,
      pets: SS.array(SS.string),
      gender: SS.nullable(SS.literal('male', 'female')),
      died: SS.nullable(SS.date),
      createdAt: SS.date
    });

    const i = {
      id: 123.5,
      name: 86,
      favouriteNumber: 99,
      savings: true,
      pets: 'bingo',
      gender: 'nonbinary',
      died: false,
      createdAt: null
    };

    const { decode } = interpreter(De.Schemable)(item);

    const a = pipe(decode(i), E.mapLeft(D.draw));

    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode 123.5, should be Int'))
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode 86, should be string'))
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode 99, should be 7 | 42'))
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode true, should be Decimal'))
    );
    expect(a).toEqual(
      E.left(
        expect.stringContaining(
          'cannot decode "bingo", should be Array<unknown>'
        )
      )
    );
    expect(a).toEqual(
      E.left(
        expect.stringContaining(
          'cannot decode "nonbinary", should be "male" | "female"'
        )
      )
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode false, should be Date'))
    );
    expect(a).toEqual(
      E.left(expect.stringContaining('cannot decode null, should be Date'))
    );
  });

  it('spec schema should handle nested conditions', () => {
    const { spec } = SS.struct({
      id: SS.int,
      name: SS.string,
      savings: SS.decimal,
      pets: SS.array(SS.string),
      gender: SS.nullable(SS.literal('male', 'female')),
      died: SS.nullable(SS.date),
      createdAt: SS.date
    });

    type Spec = SC.TypeOf<typeof spec>;

    // Either name is "B*****N", or has a pet named "Bingo", or is dead.
    const i: Spec = {
      OR: [
        {
          AND: [
            {
              name: {
                startsWith: 'B'
              }
            },
            {
              name: {
                endsWith: 'N'
              }
            }
          ]
        },
        {
          OR: [
            {
              pets: {
                has: 'Bingo'
              }
            },
            {
              died: {
                not: {
                  equals: null
                }
              }
            }
          ]
        }
      ]
    };

    const { decode } = interpreter(De.Schemable)(spec);
    const { encode } = interpreter(En.Schemable)(spec);

    const a = decode(i);

    expect(a).toEqual(E.right(i));
    expect(encode(i)).toEqual(i);
  });
});
