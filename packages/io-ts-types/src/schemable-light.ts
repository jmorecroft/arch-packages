import { HKT, Kind, Kind2, URIS, URIS2 } from 'fp-ts/HKT';
import { unsafeCoerce } from 'fp-ts/lib/function';
import * as S from 'io-ts/Schemable';
import * as SC from 'io-ts/Schema';
import { Decimal, Int } from './types';

export type Supported =
  | 'number'
  | 'string'
  | 'boolean'
  | 'literal'
  | 'nullable'
  | 'array'
  | 'struct';

// base type class definition
export interface SchemableLight<S> extends Pick<S.Schemable<S>, Supported> {
  readonly int: HKT<S, Int>;
  readonly decimal: HKT<S, Decimal>;
  readonly date: HKT<S, Date>;
}

// type class definition for * -> * constructors (e.g. `Eq`, `Guard`)
export interface SchemableLight1<S extends URIS>
  extends Pick<S.Schemable1<S>, Supported> {
  readonly int: Kind<S, Int>;
  readonly decimal: Kind<S, Decimal>;
  readonly date: Kind<S, Date>;
}

// type class definition for * -> * -> * constructors (e.g. `Decoder`, `Encoder`)
export interface SchemableLight2C<S extends URIS2>
  extends Pick<S.Schemable2C<S, unknown>, Supported> {
  readonly int: Kind2<S, unknown, Int>;
  readonly decimal: Kind2<S, unknown, Decimal>;
  readonly date: Kind2<S, unknown, Date>;
}

export interface Schema<A> {
  <S>(S: SchemableLight<S>): HKT<S, A>;
}

export type TypeOf<T> = T extends Schema<infer A> ? A : never;

export function make<A>(f: Schema<A>): Schema<A> {
  return S.memoize(f);
}

export const interpreter: {
  <S extends URIS2>(S: SchemableLight2C<S>): <A>(
    schema: Schema<A>
  ) => Kind2<S, unknown, A>;
  <S extends URIS>(S: SchemableLight1<S>): <A>(schema: Schema<A>) => Kind<S, A>;
} = unsafeCoerce(SC.interpreter);
