/* eslint-disable @typescript-eslint/no-explicit-any */
import { Decimal } from './types';
import {
  Encoder,
  URI,
  struct,
  partial,
  array,
  readonly,
  lazy,
  intersect,
  nullable,
  tuple,
  record,
  sum,
  id
} from 'io-ts/Encoder';

import { Schemable2C } from './schemable';

export const decimal: Encoder<string, Decimal> = {
  encode: (a) => a.toString()
};

export const date: Encoder<number, Date> = {
  encode: (a) => a.getTime()
};

export const Schemable: Schemable2C<URI> = {
  URI,
  struct: struct as any,
  type: struct as any,
  partial: partial as any,
  array,
  readonly,
  lazy: (id, f) => lazy(f),
  intersect,
  nullable,
  sum: sum as any,
  tuple: tuple as any,
  record,
  string: id(),
  number: id(),
  boolean: id(),
  literal: () => id(),
  int: id(),
  date,
  decimal
};
