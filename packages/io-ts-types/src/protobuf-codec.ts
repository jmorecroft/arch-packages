/* eslint-disable @typescript-eslint/no-explicit-any */
import * as pb from 'protobufjs';
import * as SC from './schemable';
import * as _ from 'lodash';
import Decimal from 'decimal.js';
import { Int } from './types';

import * as E from 'fp-ts/Either';
import * as O from 'fp-ts/Option';
import * as D from 'io-ts/Decoder';
import * as DE from 'io-ts/DecodeError';
import * as FS from 'io-ts/FreeSemigroup';
import { flow, pipe } from 'fp-ts/lib/function';
import { tailRec } from 'fp-ts/lib/ChainRec';
import { memoize } from 'io-ts/lib/Schemable';

export const PB_PRIMITIVE_TYPES = [
  'double',
  'float',
  'int32',
  'uint32',
  'sint32',
  'fixed32',
  'sfixed32',
  'int64',
  'uint64',
  'sint64',
  'fixed64',
  'sfixed64',
  'string',
  'bool',
  'bytes'
] as const;

export type PbPrimitiveTypeName = typeof PB_PRIMITIVE_TYPES[number];

export type RefName = `Ref${number}`;

export type IdStartSuffix = `_${number}` | '';

export type PbAnonTypeName =
  | `${'Struct' | 'Partial'}${RefName}${IdStartSuffix}`
  | `Sum${string}${RefName}${IdStartSuffix}`
  | `Intersect${RefName}${RefName}${IdStartSuffix}`
  | `${'Literal' | 'Array'}${RefName}`;

export type PbNullableTypeName = `Nullable${string}`;

export type PbTypeName =
  | PbPrimitiveTypeName
  | PbAnonTypeName
  | PbNullableTypeName;

export interface Context {
  refNames: Map<unknown, RefName>;
  idStart: number;
}

export const makeContext = (overrides?: Partial<Context>): Context => ({
  refNames: new Map(),
  idStart: 1,
  ...(overrides ? overrides : {})
});

const makeGetRefName = (refNames: Map<unknown, RefName>) => (ref: unknown) => {
  let name = refNames.get(ref);
  if (name) {
    return name;
  }
  name = `Ref${refNames.size + 1}`;
  refNames.set(ref, name);
  return name;
};

const getPbWireType = (type: PbTypeName) => {
  switch (type) {
    case 'int32':
    case 'int64':
    case 'uint32':
    case 'uint64':
    case 'sint32':
    case 'sint64':
    case 'bool':
      return 0;
    case 'fixed64':
    case 'sfixed64':
    case 'double':
      return 1;
    case 'fixed32':
    case 'sfixed32':
    case 'float':
      return 5;
    default: {
      if (type.startsWith('Literal')) {
        return 0;
      }
      return 2;
    }
  }
};

export type ProtobufCodec<T> = {
  getPbTypeName(): PbTypeName;
  getIdRange?(): [number, number];
  addPbType(parent: pb.NamespaceBase): PbTypeName;
  encode(value: T, writer?: pb.Writer): pb.Writer;
  encodeDelimited(value: T, writer?: pb.Writer): pb.Writer;
  decode(
    input: pb.Reader | Uint8Array,
    length?: number
  ): E.Either<D.DecodeError, T>;
  decodeDelimited(input: pb.Reader | Uint8Array): E.Either<D.DecodeError, T>;
};

export type ProtobufCodecMaker<T> = (context: Context) => ProtobufCodec<T>;

export type TypeOf<T> = T extends ProtobufCodecMaker<infer A> ? A : never;

export const URI = 'io-ts/ProtobufCodec';

export type URI = typeof URI;

declare module 'fp-ts/lib/HKT' {
  interface URItoKind<A> {
    readonly [URI]: ProtobufCodecMaker<A>;
  }
}

const capitalizeFirstLetter = (s: string) =>
  s.charAt(0).toUpperCase() + s.slice(1);

const tryDecode = <T>(name: string, fn: () => T, actual: () => unknown) =>
  E.tryCatch(fn, () => FS.of(DE.leaf(actual(), name)));

type GettersOnly<T> = {
  [K in keyof T as T[K] extends () => any ? K : never]: T[K];
};
const tryRead =
  <T extends keyof GettersOnly<pb.Reader>>(fn: T) =>
  (reader: pb.Reader) =>
    tryDecode(
      fn,
      () => reader[fn]() as ReturnType<pb.Reader[T]>,
      () => reader.buf.subarray(reader.pos)
    );

const tryUint32 = tryRead('uint32');
const tryInt32 = tryRead('int32');
const tryUint64 = tryRead('uint64');
const tryInt64 = tryRead('int64');
const tryDouble = tryRead('double');
const tryString = tryRead('string');
const tryBool = tryRead('bool');

const trySkipType = (reader: pb.Reader, type: number) =>
  tryDecode(
    'skipped type',
    () => reader.skipType(type),
    () => reader.buf.subarray(reader.pos)
  );

const untilEnd =
  <A>({
    initial,
    process
  }: {
    initial: A;
    process: (a: A) => E.Either<D.DecodeError, A>;
  }) =>
  ({ reader, end }: { reader: pb.Reader; end: number }) =>
    tailRec<A, E.Either<D.DecodeError, A>>(initial, (a) => {
      if (reader.pos >= end) {
        return E.right(E.right(a));
      }
      return pipe(
        process(a),
        E.fold(
          (error) => E.right(E.left(error)),
          (result) => E.left(result)
        )
      );
    });

export const number: SC.Schemable1<URI>['number'] = () => {
  const encode = (value: number, writer?: pb.Writer) =>
    (writer ?? pb.Writer.create()).double(value);

  const decode = (input: pb.Reader | Uint8Array) => {
    const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
    return tryDouble(reader);
  };

  const getPbTypeName = (): PbTypeName => 'double';

  return {
    encode,
    decode,
    getPbTypeName,
    encodeDelimited: encode,
    decodeDelimited: decode,
    addPbType: getPbTypeName
  };
};

export const string: SC.Schemable1<URI>['string'] = () => {
  const encode = (value: string, writer?: pb.Writer) =>
    (writer ?? pb.Writer.create()).string(value);

  const decode = (input: pb.Reader | Uint8Array) => {
    const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
    return tryString(reader);
  };

  const getPbTypeName = (): PbTypeName => 'string';

  return {
    encode,
    decode,
    getPbTypeName,
    encodeDelimited: encode,
    decodeDelimited: decode,
    addPbType: getPbTypeName
  };
};

export const boolean: SC.Schemable1<URI>['boolean'] = () => {
  const encode = (value: boolean, writer?: pb.Writer) =>
    (writer ?? pb.Writer.create()).bool(value);

  const decode = (input: pb.Reader | Uint8Array) => {
    const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
    return tryBool(reader);
  };

  const getPbTypeName = (): PbTypeName => 'bool';

  return {
    encode,
    decode,
    getPbTypeName,
    encodeDelimited: encode,
    decodeDelimited: decode,
    addPbType: getPbTypeName
  };
};

export const date: SC.Schemable1<URI>['date'] = () => {
  const encode = (value: Date, writer?: pb.Writer) =>
    (writer ?? pb.Writer.create()).uint64(value.getTime());

  const decode = (input: pb.Reader | Uint8Array) => {
    const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
    return pipe(
      tryUint64(reader),
      E.chain((i) => {
        const time = pb.util.LongBits.from(i).toNumber(true);
        return tryDecode(
          'date epoch',
          () => {
            const d = new Date(time);
            if (isNaN(d.getTime())) {
              throw new Error('not a valid date');
            }
            return d;
          },
          () => time
        );
      })
    );
  };

  const getPbTypeName = (): PbTypeName => 'uint64';

  return {
    encode,
    decode,
    getPbTypeName,
    encodeDelimited: encode,
    decodeDelimited: decode,
    addPbType: getPbTypeName
  };
};

export const decimal: SC.Schemable1<URI>['decimal'] = () => {
  const encode = (value: Decimal, writer?: pb.Writer) =>
    (writer ?? pb.Writer.create()).string(value.toString());

  const decode = (input: pb.Reader | Uint8Array) => {
    const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
    return pipe(
      tryString(reader),
      E.chain((s) =>
        tryDecode(
          'decimal',
          () => new Decimal(s),
          () => s
        )
      )
    );
  };

  const getPbTypeName = (): PbTypeName => 'string';

  return {
    encode,
    decode,
    getPbTypeName,
    encodeDelimited: encode,
    decodeDelimited: decode,
    addPbType: getPbTypeName
  };
};

export const int: SC.Schemable1<URI>['int'] = () => {
  const encode = (value: Int, writer?: pb.Writer) =>
    (writer ?? pb.Writer.create()).int64(value);

  const decode = (input: pb.Reader | Uint8Array) => {
    const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
    return pipe(
      tryInt64(reader),
      E.chain((i) =>
        tryDecode(
          'int',
          () => pb.util.LongBits.from(i).toNumber() as Int,
          () => i
        )
      )
    );
  };

  const getPbTypeName = (): PbTypeName => 'int64';

  return {
    encode,
    decode,
    getPbTypeName,
    encodeDelimited: encode,
    decodeDelimited: decode,
    addPbType: getPbTypeName
  };
};

export const literal: SC.Schemable1<URI>['literal'] =
  (...values) =>
  (context) => {
    const getRefName = makeGetRefName(context.refNames);

    const encode = (value: typeof values[number], writer?: pb.Writer) =>
      (writer ?? pb.Writer.create()).int32(
        values.findIndex((v) => v === value)
      );

    const decode = (input: pb.Reader | Uint8Array) => {
      const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
      return pipe(
        tryInt32(reader),
        E.chain((index) =>
          tryDecode(
            `index to one of ${values.join(', ')}`,
            () => {
              if (index < 0 || index >= values.length) {
                throw new Error('bad index');
              }
              return values[index];
            },
            () => index
          )
        )
      );
    };

    const getPbTypeName = (): PbTypeName => `Literal${getRefName(values)}`;

    const addPbType = (parent: pb.NamespaceBase): PbTypeName => {
      const name = getPbTypeName();
      if (parent.lookup(name)) {
        return name;
      }
      const enumAsObject = values.reduce((e, v, index) => {
        const key = typeof v === 'number' ? `Num_${v}` : v;
        return {
          ...e,
          [`${name}_${key?.toString() ?? 'null'}`]: index
        };
      }, {} as Record<string, number>);
      const type = new pb.Enum(name, enumAsObject);
      parent.root.add(type);
      return name;
    };

    return {
      encode,
      decode,
      getPbTypeName,
      encodeDelimited: encode,
      decodeDelimited: decode,
      addPbType
    };
  };

export const partialOrStruct: (
  partial: boolean
) => SC.Schemable1<URI>['partial'] = (partial) => (properties) => (context) => {
  const idMap = new Map<number, string>();

  const { idStart } = context;
  const typeNameSuffix: IdStartSuffix = idStart === 1 ? '' : `_${idStart}`;
  Object.keys(properties).forEach((key) => {
    idMap.set(idStart + idMap.size, key);
  });

  const codecs: Record<string, ProtobufCodec<unknown>> = Object.entries(
    properties
  ).reduce((p, [key, maker]) => {
    return {
      ...p,
      [key]: (maker as ProtobufCodecMaker<unknown>)(context)
    };
  }, {});

  const getRefName = makeGetRefName(context.refNames);

  return {
    encode(value, writer?) {
      let write = writer ?? pb.Writer.create();
      idMap.forEach((key, id) => {
        const codec = codecs[key];
        if ((value as any)[key] !== undefined) {
          const tag = (id << 3) | getPbWireType(codec.getPbTypeName());
          write = codec.encodeDelimited((value as any)[key], write.uint32(tag));
        }
      });
      return write;
    },
    encodeDelimited(value, writer?) {
      return this.encode(value, (writer ?? pb.Writer.create()).fork()).ldelim();
    },
    decode(input, length) {
      const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
      const end = length !== undefined ? reader.pos + length : reader.len;

      return pipe(
        { reader, end },
        untilEnd({
          initial: {} as any,
          process: (obj) =>
            pipe(
              tryUint32(reader),
              E.chain((tag) => {
                const type = tag & 7;
                const id = tag >>> 3;
                const key = idMap.get(id);
                if (key) {
                  const codec = codecs[key];
                  return pipe(
                    codec.decodeDelimited(reader),
                    E.mapLeft((de) =>
                      FS.of(
                        DE.key(key, partial ? DE.optional : DE.required, de)
                      )
                    ),
                    E.map((value) => ({ ...obj, [key]: value }))
                  );
                }
                return pipe(
                  E.right(obj),
                  E.chainFirst(() => trySkipType(reader, type))
                );
              })
            )
        }),
        E.chain((obj) => {
          if (!partial) {
            const error = Object.keys(properties).reduce((p, v) => {
              if (obj[v] === undefined) {
                const de = DE.key(
                  v,
                  DE.required,
                  FS.of(DE.leaf(undefined, 'defined'))
                );
                return p === undefined ? FS.of(de) : FS.concat(p, FS.of(de));
              }
              return p;
            }, undefined as D.DecodeError | undefined);
            if (error !== undefined) {
              return E.left(error);
            }
          }
          return E.right(obj);
        })
      );
    },
    decodeDelimited(input) {
      const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
      return pipe(
        tryUint32(reader),
        E.chain((length) => this.decode(reader, length))
      );
    },
    getPbTypeName() {
      return partial
        ? `Partial${getRefName(properties)}${typeNameSuffix}`
        : `Struct${getRefName(properties)}${typeNameSuffix}`;
    },
    getIdRange() {
      return [idStart, idStart + idMap.size];
    },
    addPbType(parent) {
      const name = this.getPbTypeName();
      if (parent.lookup(name)) {
        return name;
      }
      const type = new pb.Type(name);
      parent.root.add(type);

      idMap.forEach((key, id) => {
        const fieldName = _.snakeCase(key);
        const fieldType = codecs[key].addPbType(type);
        type.add(
          new pb.Field(
            fieldName,
            id,
            fieldType,
            partial ? 'optional' : 'required'
          )
        );
      });

      return name;
    }
  };
};

export const partial: SC.Schemable1<URI>['partial'] = partialOrStruct(true);

export const struct: SC.Schemable1<URI>['struct'] = partialOrStruct(
  false
) as any;

const getTupleStruct = memoize((items: ReadonlyArray<any>) =>
  struct(
    items.reduce(
      (p, v, index) => ({
        ...p,
        [`index_${index}`]: v
      }),
      {} as Record<string, ProtobufCodecMaker<unknown>>
    )
  )
);

export const tuple: SC.Schemable1<URI>['tuple'] = (...items) => {
  return (context) => {
    const codec = getTupleStruct(items)(context);

    const toObject: (tup: readonly any[]) => any = (tup) => {
      return items.reduce(
        (p, _, index) => ({
          ...p,
          [`index_${index}`]: tup[index]
        }),
        {}
      );
    };

    const fromObject = (obj: any) =>
      items.reduce((p, _, index) => {
        return [...p, obj[`index_${index}`]];
      }, [] as any);

    return {
      encode(value, writer?) {
        return codec.encode(toObject(value), writer);
      },
      encodeDelimited(value, writer) {
        return codec.encodeDelimited(toObject(value), writer);
      },
      decode(input, length) {
        return pipe(codec.decode(input, length), E.map(fromObject));
      },
      decodeDelimited(input) {
        return pipe(codec.decodeDelimited(input), E.map(fromObject));
      },
      getPbTypeName() {
        return codec.getPbTypeName();
      },
      addPbType(parent) {
        return codec.addPbType(parent);
      }
    };
  };
};

export const array: SC.Schemable1<URI>['array'] = (item) => {
  return (context) => {
    const codec = item(context);

    const getRefName = makeGetRefName(context.refNames);

    return {
      encode(value, writer?) {
        let write = writer ?? pb.Writer.create();
        const wireType = getPbWireType(codec.getPbTypeName());
        const packable = [0, 1, 5].includes(wireType);
        if (packable) {
          const tag = (1 << 3) | 2;
          write = value
            .reduce((w, v) => codec.encode(v, w), write.uint32(tag).fork())
            .ldelim();
        } else {
          const tag = (1 << 3) | wireType;
          write = value.reduce(
            (w, v) => codec.encodeDelimited(v, w.uint32(tag)),
            write
          );
        }
        return write;
      },
      encodeDelimited(value, writer?) {
        return this.encode(
          value,
          (writer ?? pb.Writer.create()).fork()
        ).ldelim();
      },
      decode(input, length) {
        const reader =
          input instanceof pb.Reader ? input : new pb.Reader(input);
        const end = length !== undefined ? reader.pos + length : reader.len;

        const wireType = getPbWireType(codec.getPbTypeName());
        const packable = [0, 1, 5].includes(wireType);
        type T = TypeOf<typeof item>;

        const readOne = (items: T[]) =>
          pipe(
            codec.decodeDelimited(reader),
            E.mapLeft((de) => FS.of(DE.index(items.length, DE.optional, de))),
            E.map((a) => [...items, a])
          );

        const readMany = (items: T[]) =>
          pipe(
            tryUint32(reader),
            E.chain((tag) => {
              const type = tag & 7;
              const id = tag >>> 3;
              if (id === 1) {
                if (type === 2 && packable) {
                  return pipe(
                    tryUint32(reader),
                    E.chain((length2) =>
                      pipe(
                        {
                          reader,
                          end: reader.pos + length2
                        },
                        untilEnd({
                          initial: items,
                          process: readOne
                        })
                      )
                    )
                  );
                }
                return readOne(items);
              }
              return pipe(
                E.of(items),
                E.chainFirst(() => trySkipType(reader, type))
              );
            })
          );

        return pipe(
          { reader, end },
          untilEnd({
            initial: [] as T[],
            process: readMany
          })
        );
      },
      decodeDelimited(input) {
        const reader =
          input instanceof pb.Reader ? input : new pb.Reader(input);
        return pipe(
          tryUint32(reader),
          E.chain((length) => this.decode(reader, length))
        );
      },
      getPbTypeName() {
        return `Array${getRefName(item)}`;
      },
      addPbType(parent) {
        const name = this.getPbTypeName();
        if (parent.lookup(name)) {
          return name;
        }
        const type = new pb.Type(name);
        parent.root.add(type);
        const valueType = item(context).addPbType(type);
        type.add(new pb.Field('values', 1, valueType, 'repeated'));
        return name;
      }
    };
  };
};

const getPairStruct = memoize(<A>(value: ProtobufCodecMaker<A>) =>
  struct({
    key: string,
    value
  })
);

export const record: SC.Schemable1<URI>['record'] = (codomain) => (context) => {
  const codec = array(getPairStruct(codomain))(context);

  const toArray = <T>(obj: Record<string, T>) =>
    Object.entries(obj).map(([key, value]) => ({
      key,
      value
    }));

  const fromArray = <T>(items: { key: string; value: T }[]) =>
    items.reduce((p, v) => ({ ...p, [v.key]: v.value }), {});

  return {
    encode(obj, writer) {
      return codec.encode(toArray(obj), writer);
    },
    encodeDelimited(obj, writer) {
      return codec.encodeDelimited(toArray(obj), writer);
    },
    decode(input, length) {
      return pipe(codec.decode(input, length), E.map(fromArray));
    },
    decodeDelimited(input) {
      return pipe(codec.decodeDelimited(input), E.map(fromArray));
    },
    addPbType(parent) {
      return codec.addPbType(parent);
    },
    getPbTypeName() {
      return codec.getPbTypeName();
    }
  };
};

export const readonly: SC.Schemable1<URI>['readonly'] = (item) => item;

export const lazy: SC.Schemable1<URI>['lazy'] = (id, f) => (context) => {
  const codec = () => {
    let cache;
    if (cache === undefined) {
      cache = f()(context);
    }
    return cache;
  };
  return {
    getPbTypeName() {
      return codec().getPbTypeName();
    },
    addPbType(parent) {
      return codec().addPbType(parent);
    },
    decode(input, length?) {
      return codec().decode(input, length);
    },
    decodeDelimited(input) {
      return codec().decodeDelimited(input);
    },
    encode(value, writer?) {
      return codec().encode(value, writer);
    },
    encodeDelimited(value, writer?) {
      return codec().encodeDelimited(value, writer);
    }
  };
};

export const sum: SC.Schemable1<URI>['sum'] =
  (type) => (members) => (context) => {
    const idKeyMap = new Map<number, string>();
    const keyIdMap = new Map<string, number>();
    const { idStart } = context;
    const typeNameSuffix: IdStartSuffix = idStart === 1 ? '' : `_${idStart}`;
    Object.keys(members).forEach((key) => {
      idKeyMap.set(idStart + idKeyMap.size, key);
      keyIdMap.set(key, idStart + keyIdMap.size);
    });

    const codecs: Record<string, ProtobufCodec<unknown>> = Object.entries(
      members
    ).reduce((p, [key, maker]) => {
      return {
        ...p,
        [key]: (maker as ProtobufCodecMaker<unknown>)(context)
      };
    }, {});

    const getRefName = makeGetRefName(context.refNames);

    return {
      decode(input, length) {
        const reader =
          input instanceof pb.Reader ? input : new pb.Reader(input);
        const end = length !== undefined ? reader.pos + length : reader.len;

        return pipe(
          { reader, end },
          untilEnd({
            initial: O.none as O.Option<any>,
            process: (val) =>
              pipe(
                tryUint32(reader),
                E.chain((tag) => {
                  const type = tag & 7;
                  const id = tag >>> 3;
                  const key = idKeyMap.get(id);
                  if (key) {
                    const codec = codecs[key];
                    return pipe(codec.decodeDelimited(reader), E.map(O.some));
                  }
                  return pipe(
                    E.of(val),
                    E.chainFirst(() => trySkipType(reader, type))
                  );
                })
              )
          }),
          E.chain(
            flow(
              O.fold(
                () => E.left(FS.of(DE.leaf(undefined, 'defined'))),
                (a) => E.right(a)
              )
            )
          )
        );
      },
      decodeDelimited(input) {
        const reader =
          input instanceof pb.Reader ? input : new pb.Reader(input);
        return pipe(
          tryUint32(reader),
          E.chain((length) => this.decode(reader, length))
        );
      },
      encode(value, writer) {
        const write = writer ?? pb.Writer.create();
        const key = (value as any)[type];
        const codec = codecs[key];
        const id = keyIdMap.get(key);
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const tag = (id! << 3) | getPbWireType(codec.getPbTypeName());
        return codec.encodeDelimited(value, write.uint32(tag));
      },
      encodeDelimited(value, writer) {
        return this.encode(
          value,
          (writer ?? pb.Writer.create()).fork()
        ).ldelim();
      },
      getPbTypeName() {
        return `Sum${capitalizeFirstLetter(type)}${getRefName(
          members
        )}${typeNameSuffix}`;
      },
      addPbType(parent) {
        const name = this.getPbTypeName();
        if (parent.lookup(name)) {
          return name;
        }
        const type = new pb.Type(name);
        parent.root.add(type);

        const oneOf = new pb.OneOf('kind');

        idKeyMap.forEach((key, id) => {
          const codec = codecs[key];
          const valueType = codec.addPbType(type);
          oneOf.add(new pb.Field(_.snakeCase(key), id, valueType));
        });

        type.add(oneOf);
        return name;
      }
    };
  };

export const intersect: SC.Schemable1<URI>['intersect'] =
  (right) => (left) => (context) => {
    const leftCodec = left(context);

    const { idStart } = context;

    const leftRange = leftCodec.getIdRange?.();

    const rightCodec = right({
      ...context,
      idStart: leftRange ? leftRange[1] : idStart
    });

    const typeNameSuffix: IdStartSuffix = idStart === 1 ? '' : `_${idStart}`;

    const getRefName = makeGetRefName(context.refNames);

    return {
      encode(value, writer) {
        let write = writer ?? new pb.Writer();
        write = leftCodec.encode(value, write);
        return rightCodec.encode(value, write);
      },
      encodeDelimited(value, writer) {
        return this.encode(value, (writer ?? new pb.Writer()).fork()).ldelim();
      },
      decode(input, length) {
        const reader =
          input instanceof pb.Reader ? input : new pb.Reader(input);
        return pipe(
          leftCodec.decode(reader.buf.subarray(reader.pos), length),
          E.bindTo('a'),
          E.bind('b', () => rightCodec.decode(reader, length)),
          E.map(({ a, b }) => ({ ...a, ...b }))
        );
      },
      decodeDelimited(input) {
        const reader =
          input instanceof pb.Reader ? input : new pb.Reader(input);
        return pipe(
          tryUint32(reader),
          E.chain((length) => this.decode(reader, length))
        );
      },
      getIdRange() {
        const rightRange = rightCodec.getIdRange?.();
        if (leftRange && rightRange) {
          return [leftRange[0], rightRange[1]];
        }
        if (leftRange) {
          return leftRange;
        }
        if (rightRange) {
          return rightRange;
        }
        return [idStart, idStart];
      },
      getPbTypeName() {
        return `Intersect${getRefName(left)}${getRefName(
          right
        )}${typeNameSuffix}`;
      },
      addPbType(parent) {
        const name = this.getPbTypeName();
        if (parent.lookup(name)) {
          return name;
        }
        const type = new pb.Type(name);
        parent.root.add(type);

        const leftTypeName = leftCodec.addPbType(type);
        const rightTypeName = rightCodec.addPbType(type);
        const leftType = type.lookup(leftTypeName);
        const rightType = type.lookup(rightTypeName);
        const fieldNames = new Set<string>();
        if (leftType instanceof pb.Type) {
          leftType.fieldsArray.forEach((f) => {
            const copy = new pb.Field(
              f.name,
              f.id,
              f.type,
              f.required ? 'required' : 'optional'
            );
            fieldNames.add(f.name);
            type.add(copy);
          });
        }
        if (rightType instanceof pb.Type) {
          rightType.fieldsArray.forEach((f) => {
            let name = f.name;
            if (fieldNames.has(name)) {
              name = `${name}_2`;
            }
            const copy = new pb.Field(
              name,
              f.id,
              f.type,
              f.required ? 'required' : 'optional'
            );
            type.add(copy);
          });
        }
        return name;
      }
    };
  };

export const nullable: SC.Schemable1<URI>['nullable'] = (or) => (context) => {
  const codec = or(context);

  return {
    decode(input, length) {
      const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
      const end = length !== undefined ? reader.pos + length : reader.len;

      type A = typeof or extends ProtobufCodecMaker<infer A> ? A : never;

      return pipe(
        { reader, end },
        untilEnd({
          initial: O.none as O.Option<A | null>,
          process: (a) =>
            pipe(
              tryUint32(reader),
              E.chain((tag) => {
                const type = tag & 7;
                const id = tag >>> 3;
                switch (id) {
                  case 1: {
                    return pipe(
                      tryBool(reader),
                      E.mapLeft((de) => FS.of(DE.member(0, de))),
                      E.map((isNull) => (isNull ? O.some(null) : a))
                    );
                  }
                  case 2: {
                    return pipe(
                      codec.decodeDelimited(reader),
                      E.mapLeft((de) => FS.of(DE.member(1, de))),
                      E.map(O.some)
                    );
                  }
                  default: {
                    return pipe(
                      E.of(a),
                      E.chainFirst(() => trySkipType(reader, type))
                    );
                  }
                }
              })
            )
        }),
        E.chain(
          flow(
            O.fold(
              () =>
                E.left(
                  FS.concat(
                    FS.of(DE.member(0, D.error(undefined, 'null'))),
                    FS.of(DE.member(1, D.error(undefined, 'defined')))
                  )
                ),
              (a) => E.right(a)
            )
          )
        )
      );
    },
    decodeDelimited(input) {
      const reader = input instanceof pb.Reader ? input : new pb.Reader(input);
      return pipe(
        tryUint32(reader),
        E.chain((length) => this.decode(reader, length))
      );
    },
    encode(value, writer) {
      const write = writer ?? pb.Writer.create();
      if (value !== null) {
        const tag = (2 << 3) | getPbWireType(codec.getPbTypeName());
        return codec.encodeDelimited(value as any, write.uint32(tag));
      }
      const tag = (1 << 3) | 0;
      return write.uint32(tag).bool(true);
    },
    encodeDelimited(value, writer) {
      return this.encode(value, (writer ?? pb.Writer.create()).fork()).ldelim();
    },
    getPbTypeName() {
      return `Nullable${capitalizeFirstLetter(codec.getPbTypeName())}`;
    },
    addPbType(parent) {
      const name = this.getPbTypeName();
      if (parent.lookup(name)) {
        return name;
      }
      const type = new pb.Type(name);
      parent.root.add(type);

      const oneOf = new pb.OneOf('kind');
      const valueType = or(context).addPbType(type);
      oneOf.add(new pb.Field('null', 1, 'bool'));
      oneOf.add(new pb.Field('value', 2, valueType));
      type.add(oneOf);
      return name;
    }
  };
};

export const Schemable: SC.Schemable1<URI> = {
  URI,
  number,
  string,
  boolean,
  date,
  nullable,
  struct,
  decimal,
  int,
  literal,
  array,
  tuple,
  type: struct,
  readonly,
  partial,
  lazy,
  record,
  sum,
  intersect
};
