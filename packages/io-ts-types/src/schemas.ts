/* eslint-disable @typescript-eslint/no-explicit-any */
import { Decimal } from './types';
import { Literal, memoize } from 'io-ts/lib/Schemable';
import * as SC from './schemable';
import * as SCL from './schemable-light';
import { Int } from './types';

const stringSpec = SC.make((S) => {
  const props = {
    equals: S.string,
    in: S.array(S.string),
    notIn: S.array(S.string),
    lt: S.string,
    lte: S.string,
    gt: S.string,
    gte: S.string,
    contains: S.string,
    startsWith: S.string,
    endsWith: S.string,
    mode: S.literal('default', 'insensitive')
  };
  return S.partial({
    ...props,
    not: S.partial({
      ...props
    })
  });
});

const nullableStringSpec = SC.make((S) => {
  const mode = S.literal('default', 'insensitive');
  const props = {
    equals: S.nullable(S.string),
    in: S.array(S.string),
    notIn: S.array(S.string),
    lt: S.string,
    lte: S.string,
    gt: S.string,
    gte: S.string,
    contains: S.string,
    startsWith: S.string,
    endsWith: S.string,
    mode
  };
  return S.partial({
    ...props,
    not: S.nullable(
      S.partial({
        ...props
      })
    )
  });
});

const boolSpec = SC.make((S) => {
  const props = {
    equals: S.boolean
  };
  return S.partial({
    ...props,
    not: S.partial(props)
  });
});

const nullableBoolSpec = SC.make((S) => {
  const props = {
    equals: S.nullable(S.boolean)
  };
  return S.partial({
    ...props,
    not: S.nullable(S.partial(props))
  });
});

const baseSpec = <A>(s: SC.Schema<A>) =>
  SC.make((S) => {
    const item = s(S);
    const props = {
      equals: item,
      in: S.array(item),
      notIn: S.array(item),
      lt: item,
      lte: item,
      gt: item,
      gte: item
    };
    return S.partial({
      ...props,
      not: S.partial(props)
    });
  });

const baseNullableSpec = <A>(s: SC.Schema<A>) =>
  SC.make((S) => {
    const item = s(S);
    const props = {
      equals: S.nullable(item),
      in: S.array(item),
      notIn: S.array(item),
      lt: item,
      lte: item,
      gt: item,
      gte: item
    };
    return S.partial({
      ...props,
      not: S.nullable(S.partial(props))
    });
  });

const literalSpec = <A>(s: SC.Schema<A>) =>
  SC.make((S) => {
    const item = s(S);
    const props = {
      equals: item,
      in: S.array(item),
      notIn: S.array(item)
    };
    return S.partial({
      ...props,
      not: S.partial({
        ...props
      })
    });
  });

const nullableLiteralSpec = <A>(s: SC.Schema<A>) =>
  SC.make((S) => {
    const item = s(S);
    const props = {
      equals: S.nullable(item),
      in: S.array(item),
      notIn: S.array(item)
    };
    return S.partial({
      ...props,
      not: S.nullable(
        S.partial({
          ...props
        })
      )
    });
  });

const arraySpec = <A>(s: SC.Schema<A>) =>
  SC.make((S) => {
    const item = s(S);
    return S.partial({
      equals: S.nullable(S.array(item)),
      has: item,
      hasEvery: S.array(item),
      hasSome: S.array(item),
      isEmpty: S.boolean
    });
  });

const structSpec = memoize(
  <A>(props: {
    [K in keyof A]: Schemas<A[K]>;
  }): SC.Schema<Spec<A>> =>
    SC.make((S) => {
      const props2 = Array.from(Object.entries(props)).reduce((prev, next) => {
        const [key, val] = next as [string, Schemas<unknown>];
        return {
          ...prev,
          [key]: val.spec(S)
        };
      }, {});
      return S.partial({
        ...props2,
        AND: S.array(S.lazy('struct', () => structSpec(props)(S))),
        OR: S.array(S.lazy('struct', () => structSpec(props)(S))),
        NOT: S.array(S.lazy('struct', () => structSpec(props)(S)))
      });
    }) as any
);

const numberSpec = baseSpec(SC.make((S) => S.number));
const intSpec = baseSpec(SC.make((S) => S.int));
const decimalSpec = baseSpec(SC.make((S) => S.decimal));
const dateSpec = baseSpec(SC.make((S) => S.date));
const nullableNumberSpec = baseNullableSpec(SC.make((S) => S.number));
const nullableIntSpec = baseNullableSpec(SC.make((S) => S.int));
const nullableDecimalSpec = baseNullableSpec(SC.make((S) => S.decimal));
const nullableDateSpec = baseNullableSpec(SC.make((S) => S.date));

class TypeHelper<T> {
  literalSpec(s: SC.Schema<T>) {
    return literalSpec(s);
  }
  nullableLiteralSpec(s: SC.Schema<T>) {
    return nullableLiteralSpec(s);
  }
  arraySpec(s: SC.Schema<T>) {
    return arraySpec(s);
  }
}

export type StringSpec = SC.TypeOf<typeof stringSpec>;
export type NumberSpec = SC.TypeOf<typeof numberSpec>;
export type IntSpec = SC.TypeOf<typeof intSpec>;
export type DecimalSpec = SC.TypeOf<typeof decimalSpec>;
export type DateSpec = SC.TypeOf<typeof dateSpec>;
export type BoolSpec = SC.TypeOf<typeof boolSpec>;
export type LiteralSpec<T> = SC.TypeOf<
  ReturnType<TypeHelper<T>['literalSpec']>
>;
export type NullableStringSpec = SC.TypeOf<typeof nullableStringSpec>;
export type NullableNumberSpec = SC.TypeOf<typeof nullableNumberSpec>;
export type NullableIntSpec = SC.TypeOf<typeof nullableIntSpec>;
export type NullableDecimalSpec = SC.TypeOf<typeof nullableDecimalSpec>;
export type NullableDateSpec = SC.TypeOf<typeof nullableDateSpec>;
export type NullableBoolSpec = SC.TypeOf<typeof nullableBoolSpec>;
export type NullableLiteralSpec<T> = SC.TypeOf<
  ReturnType<TypeHelper<T>['nullableLiteralSpec']>
>;
export type ArraySpec<T> = SC.TypeOf<ReturnType<TypeHelper<T>['arraySpec']>>;

export type StructSpec<T> = {
  [K in keyof T]?: LeafSpec<T[K]>;
} & {
  AND?: Array<StructSpec<T>>;
  OR?: Array<StructSpec<T>>;
  NOT?: Array<StructSpec<T>>;
};

export type NullableStructSpec<T> = StructSpec<T> | null;

export type LeafSpec<A> = [A] extends [string | null]
  ? [A] extends [string]
    ? [string] extends [A]
      ? StringSpec
      : LiteralSpec<A>
    : [string] extends [NonNullable<A>]
    ? NullableStringSpec
    : NullableLiteralSpec<NonNullable<A>>
  : [A] extends [Int | null]
  ? [A] extends [Int]
    ? IntSpec
    : NullableIntSpec
  : [A] extends [number | null]
  ? [A] extends [number]
    ? [number] extends [A]
      ? NumberSpec
      : LiteralSpec<A>
    : [number] extends [NonNullable<A>]
    ? NullableNumberSpec
    : NullableLiteralSpec<NonNullable<A>>
  : [A] extends [Date | null]
  ? [A] extends [Date]
    ? DateSpec
    : NullableDateSpec
  : [A] extends [Decimal | null]
  ? [A] extends [Decimal]
    ? DecimalSpec
    : NullableDecimalSpec
  : [A] extends [boolean | null]
  ? [A] extends [boolean]
    ? BoolSpec
    : NullableBoolSpec
  : [A] extends [unknown[] | null]
  ? ArraySpec<NonNullable<A>[number]>
  : never;

export type Spec<A> = [A] extends [Record<keyof any, unknown> | null]
  ? [A] extends [Record<keyof any, unknown>]
    ? StructSpec<A>
    : NullableStructSpec<NonNullable<A>>
  : LeafSpec<A>;

export type Schemas<A> = {
  spec: SC.Schema<Spec<A>>;
  item: SC.Schema<A>;
  nullable: () => Schemas<A | null>;
};

export type TypeOf<T> = T extends Schemas<infer A> ? A : never;

export type SpecTypeOf<T> = T extends Schemas<infer A> ? Spec<A> : never;

export const URI = 'io-ts/Schemas';

export type URI = typeof URI;

declare module 'fp-ts/lib/HKT' {
  interface URItoKind<A> {
    readonly [URI]: Schemas<A>;
  }
}

export const number = (() => {
  const nullable = () => ({
    spec: nullableNumberSpec,
    item: SC.make((S) => S.nullable(S.number)),
    nullable
  });
  return {
    spec: numberSpec,
    item: SC.make((S) => S.number),
    nullable
  };
})();

export const int = (() => {
  const nullable = () => ({
    spec: nullableIntSpec,
    item: SC.make((S) => S.nullable(S.int)),
    nullable
  });
  return {
    spec: intSpec,
    item: SC.make((S) => S.int),
    nullable
  };
})();

export const string = (() => {
  const nullable = () => ({
    spec: nullableStringSpec,
    item: SC.make((S) => S.nullable(S.string)),
    nullable
  });
  return {
    spec: stringSpec,
    item: SC.make((S) => S.string),
    nullable
  };
})();

export const boolean = (() => {
  const nullable = () => ({
    spec: nullableBoolSpec,
    item: SC.make((S) => S.nullable(S.boolean)),
    nullable
  });
  return {
    spec: boolSpec,
    item: SC.make((S) => S.boolean),
    nullable
  };
})();

export const date = (() => {
  const nullable = () => ({
    spec: nullableDateSpec,
    item: SC.make((S) => S.nullable(S.date)),
    nullable
  });
  return {
    spec: dateSpec,
    item: SC.make((S) => S.date),
    nullable
  };
})();

export const decimal = (() => {
  const nullable = () => ({
    spec: nullableDecimalSpec,
    item: SC.make((S) => S.nullable(S.decimal)),
    nullable
  });
  return {
    spec: decimalSpec,
    item: SC.make((S) => S.decimal),
    nullable
  };
})();

export const literal = <A extends readonly [Literal, ...Array<Literal>]>(
  ...values: A
): Schemas<A[number]> => {
  const item = SC.make((S) => S.literal(...values));
  const nullable = () => ({
    spec: nullableLiteralSpec(item),
    item: SC.make((S) => S.nullable(item(S))),
    nullable
  });
  return {
    spec: literalSpec(item),
    item,
    nullable
  } as any;
};

export const array = <A>(s: Schemas<A>) => {
  const item = SC.make((S) => S.array(s.item(S)));
  const nullable = () => ({
    spec: arraySpec(s.item),
    item,
    nullable
  });
  return nullable();
};

export const struct = <A>(props: {
  [K in keyof A]: Schemas<A[K]>;
}) => {
  const spec = structSpec(props);

  const item = SC.make((S) =>
    S.struct(
      Array.from(Object.entries(props)).reduce((prev, next) => {
        const [key, val] = next as [string, Schemas<unknown>];
        return {
          ...prev,
          [key]: val.item(S)
        };
      }, {})
    )
  ) as SC.Schema<A>;

  const nullable = () => ({
    spec: SC.make((S) => S.nullable(spec(S))) as SC.Schema<Spec<A | null>>,
    item: SC.make((S) => S.nullable(item(S))),
    nullable
  });
  return {
    spec,
    item,
    nullable
  };
};

export const nullable = <A>(or: Schemas<A>) => or.nullable();

export const Schemable: SCL.SchemableLight1<URI> = {
  number,
  string,
  boolean,
  date,
  decimal,
  int,
  literal,
  nullable,
  array,
  struct
};
