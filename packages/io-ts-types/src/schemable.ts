import { HKT, Kind, Kind2, URIS, URIS2 } from 'fp-ts/HKT';
import { unsafeCoerce } from 'fp-ts/lib/function';
import * as S from 'io-ts/Schemable';
import * as SC from 'io-ts/Schema';
import { Decimal, Int } from './types';

// base type class definition
export interface Schemable<S> extends S.Schemable<S> {
  readonly int: HKT<S, Int>;
  readonly decimal: HKT<S, Decimal>;
  readonly date: HKT<S, Date>;
  //readonly buffer: HKT<S, Uint8Array>;
}

// type class definition for * -> * constructors (e.g. `Eq`, `Guard`)
export interface Schemable1<S extends URIS> extends S.Schemable1<S> {
  readonly int: Kind<S, Int>;
  readonly decimal: Kind<S, Decimal>;
  readonly date: Kind<S, Date>;
  //readonly buffer: Kind<S, Uint8Array>;
}

// type class definition for * -> * -> * constructors (e.g. `Decoder`, `Encoder`)
export interface Schemable2C<S extends URIS2>
  extends S.Schemable2C<S, unknown> {
  readonly int: Kind2<S, unknown, Int>;
  readonly decimal: Kind2<S, unknown, Decimal>;
  readonly date: Kind2<S, unknown, Date>;
  //readonly buffer: Kind2<S, unknown, Uint8Array>;
}

export interface Schema<A> {
  <S>(S: Schemable<S>): HKT<S, A>;
}

export type TypeOf<T> = T extends Schema<infer A> ? A : never;

export function make<A>(f: Schema<A>): Schema<A> {
  return S.memoize(f);
}

export const interpreter: {
  <S extends URIS2>(S: Schemable2C<S>): <A>(
    schema: Schema<A>
  ) => Kind2<S, unknown, A>;
  <S extends URIS>(S: Schemable1<S>): <A>(schema: Schema<A>) => Kind<S, A>;
} = unsafeCoerce(SC.interpreter);
