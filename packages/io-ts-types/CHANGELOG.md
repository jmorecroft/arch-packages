# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0...@jmorecroft67/io-ts-types@1.0.1) (2023-01-05)

**Note:** Version bump only for package @jmorecroft67/io-ts-types





# [1.0.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.8...@jmorecroft67/io-ts-types@1.0.0) (2023-01-04)

**Note:** Version bump only for package @jmorecroft67/io-ts-types





# [1.0.0-staging.8](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.7...@jmorecroft67/io-ts-types@1.0.0-staging.8) (2022-12-13)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** improve proto file output, use proto2 version ([2f7c56e](https://gitlab.com/jmorecroft/arch-packages/commit/2f7c56e57ea082d640080e4b1b3910aa5c5f1c34))





# [1.0.0-staging.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.6...@jmorecroft67/io-ts-types@1.0.0-staging.7) (2022-12-03)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** fix proto gen problem due to multiple instances of protobufjs ([4cb7387](https://gitlab.com/jmorecroft/arch-packages/commit/4cb73873558102d3e064ff1406033bca4791d3f1))





# [1.0.0-staging.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.5...@jmorecroft67/io-ts-types@1.0.0-staging.6) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/io-ts-types





# [1.0.0-staging.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.4...@jmorecroft67/io-ts-types@1.0.0-staging.5) (2022-12-03)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** make enum names unique for in proto file for given context ([baafee2](https://gitlab.com/jmorecroft/arch-packages/commit/baafee2f8492f3cb29b0ef49037b558854c61cf9))





# [1.0.0-staging.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.3...@jmorecroft67/io-ts-types@1.0.0-staging.4) (2022-12-02)


### Features

* **@jmorecroft67/api-maker:** embed useful schemas in select stream definition ([3aa9e66](https://gitlab.com/jmorecroft/arch-packages/commit/3aa9e66f16e3cb344fb8ca08dba2ffbe9869bde4))





# [1.0.0-staging.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.2...@jmorecroft67/io-ts-types@1.0.0-staging.3) (2022-12-01)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** fix proto-generate errors when protobuf-codec used with api-maker ([3918412](https://gitlab.com/jmorecroft/arch-packages/commit/39184122ee051afb3d2b7947efe9024fe3fe5aa8))





# [1.0.0-staging.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.1...@jmorecroft67/io-ts-types@1.0.0-staging.2) (2022-11-29)


### Bug Fixes

* **@jmorecroft67/io-ts-types, @jmorecroft67/api-maker:** fix location of message def in proto file ([634c640](https://gitlab.com/jmorecroft/arch-packages/commit/634c640aa372069db68791a532e1bb497cbaf203))





# [1.0.0-staging.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@1.0.0-staging.0...@jmorecroft67/io-ts-types@1.0.0-staging.1) (2022-11-29)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** fix pb codec for literal numbers ([595b550](https://gitlab.com/jmorecroft/arch-packages/commit/595b5504be3f34c7235128b2de72330bd8c86498))





# [1.0.0-staging.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@0.5.3-staging.0...@jmorecroft67/io-ts-types@1.0.0-staging.0) (2022-11-24)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** added comments ([cca4a61](https://gitlab.com/jmorecroft/arch-packages/commit/cca4a6189578d1fc58e84cedb9718d1a79d0dc4f))


### BREAKING CHANGES

* **@jmorecroft67/io-ts-types:** No-op change to bump major version.





## [0.5.3-staging.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@0.5.2...@jmorecroft67/io-ts-types@0.5.3-staging.0) (2022-11-24)

**Note:** Version bump only for package @jmorecroft67/io-ts-types





## [0.5.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@0.5.1...@jmorecroft67/io-ts-types@0.5.2) (2022-11-03)

**Note:** Version bump only for package @jmorecroft67/io-ts-types





## [0.5.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@0.5.0...@jmorecroft67/io-ts-types@0.5.1) (2022-10-25)

**Note:** Version bump only for package @jmorecroft67/io-ts-types





# [0.5.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@0.4.1...@jmorecroft67/io-ts-types@0.5.0) (2022-10-24)


### Features

* **@jmorecroft67/io-ts-types:** Added protobuf codec schemable ([910bd7d](https://gitlab.com/jmorecroft/arch-packages/commit/910bd7d601c205cbcd2f971cda6f16d1b9700c78))





## 0.4.1 (2022-08-22)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** Exporting additional schema types. ([04bbb7c](https://gitlab.com/jmorecroft/arch-packages/commit/04bbb7caa02dabe4e6f160a6890323132cd75753))





# [0.4.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/io-ts-types@0.3.0...@jmorecroft67/io-ts-types@0.4.0) (2022-07-21)


### Features

* **@jmorecroft67/io-ts-types:** Added better support for numeric literals. ([280d037](https://gitlab.com/jmorecroft/arch-packages/commit/280d037ce589e142eaa541060b6c60e683064c31))





# 0.3.0 (2022-07-21)


### Bug Fixes

* Update package-lock.json. ([dbb5c07](https://gitlab.com/jmorecroft/arch-packages/commit/dbb5c0711f38b418a31386e07479bb0af6998de4))


### Features

* **@jmorecroft67/io-ts-types:** Added support for recursive AND, OR, NOT conditions in filters. ([80f6ca1](https://gitlab.com/jmorecroft/arch-packages/commit/80f6ca19e2bc9e6d2cd6792bab2239e13803eb0f))





# 0.2.0 (2022-07-11)


### Features

* **@jmorecroft67/io-ts-types:** Added utils functions - withDefault, getEnvOrThrow. ([4caeb09](https://gitlab.com/jmorecroft/arch-packages/commit/4caeb097719d91dbaf79c367311986e0fa260891))





# 0.1.0 (2022-06-28)


### Bug Fixes

* Fixed package.json publish config. ([a19237b](https://gitlab.com/jmorecroft/arch-packages/commit/a19237b42fc9a1d88d3bfec6a54c84c3b979d46b))
* Temp disable lerna task. ([9c31f36](https://gitlab.com/jmorecroft/arch-packages/commit/9c31f3687f8bfc8a56f105b4883aac90fbb34071))


### Features

* io-ts-types, api-maker libs ([02a76db](https://gitlab.com/jmorecroft/arch-packages/commit/02a76dbf3931848c538475f588ec20a3507cd539))
