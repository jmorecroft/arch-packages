import * as TE from 'fp-ts/TaskEither';
import * as jose from 'node-jose';
import {
  CreateAuthenticator,
  CreateUser,
  SaveChallenge,
  SaveCounter,
  SelectUser,
  User,
  generateAuthenticationOptions,
  generateRegistrationOptions,
  verifyAuthenticationResponse,
  verifyRegistrationResponse,
  GetJwtPayload
} from './webauthn';
import refreshTokens from './refresh-tokens';
import getJwksJson from './get-jwks-json';
import { Server } from 'nice-grpc';
import { addService } from '@jmorecroft67/api-maker';
import { AuthApi } from '@jmorecroft67/auth-api/lib/make-api';

const addAuthService =
  <D extends { email: string }, U extends User>({
    api,
    selectUser,
    createUser,
    saveChallenge,
    createAuthenticator,
    saveCounter,
    getJwtPayload,
    getKeyStore,
    getRefreshSecret,
    rpID,
    rpName,
    origin,
    jwt
  }: {
    api: AuthApi<D>;
    selectUser: SelectUser<U>;
    createUser: CreateUser<D, U>;
    saveChallenge: SaveChallenge<U>;
    createAuthenticator: CreateAuthenticator;
    saveCounter: SaveCounter;
    getJwtPayload: GetJwtPayload<U>;
    getKeyStore: TE.TaskEither<Error, jose.JWK.KeyStore>;
    getRefreshSecret: TE.TaskEither<Error, string>;
    rpID: string;
    rpName: string;
    origin: string;
    jwt: {
      access: {
        expiresIn: string;
      };
      refresh: {
        expiresIn: string;
      };
      audience: string;
      issuer: string;
    };
  }) =>
  (server: Server) => {
    addService(api)({
      server,
      impl: {
        getJwksJson: getJwksJson(getKeyStore),
        generateAuthenticationOptions: generateAuthenticationOptions({
          saveChallenge,
          selectUser
        }),
        generateRegistrationOptions: generateRegistrationOptions({
          createUser,
          saveChallenge,
          selectUser,
          rpID,
          rpName
        }),
        verifyAuthenticationResponse: verifyAuthenticationResponse({
          getKeyStore,
          getRefreshSecret,
          getJwtPayload,
          jwt,
          origin,
          rpID,
          saveChallenge,
          saveCounter,
          selectUser
        }),
        verifyRegistrationResponse: verifyRegistrationResponse({
          createAuthenticator,
          origin,
          rpID,
          saveChallenge,
          selectUser
        }),
        refreshTokens: refreshTokens({
          getKeyStore,
          getRefreshSecret,
          jwt
        })
      }
    });

    /*app.get(
      '/generate-extra-register-options',
      expressjwt({
        secret: async (req, token) => {
          const kid = token?.header.kid;
          if (!kid) {
            throw new Error('No token keyid');
          }
          const ks = await getKeyStore();
          return pipe(
            ks,
            E.fold(
              (e) => {
                throw e;
              },
              (ks) => ks.get(kid).toPEM()
            )
          );
        },
        audience: jwt.audience,
        issuer: jwt.issuer,
        algorithms: ['RS256']
      }),
      generateRegisterOptionsHandler({
        selectUser,
        createUser,
        saveChallenge,
        queryDecoder: userInfoDecoder,
        rpID,
        rpName
      })
    );*/
  };

export default addAuthService;
