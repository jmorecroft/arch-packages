import * as TE from 'fp-ts/TaskEither';
import * as O from 'fp-ts/Option';
import { flow, pipe } from 'fp-ts/lib/function';
import { KeyValStore } from './types';
import { randomBytes } from 'crypto';

const generateSecret = () => randomBytes(32).toString('hex');

export default (key: string) =>
  (store: KeyValStore): TE.TaskEither<Error, string> =>
    pipe(
      store.get(key),
      TE.chain(
        flow(
          O.map((s) => TE.of(s)),
          O.getOrElse(() =>
            pipe(
              TE.fromIO<string, Error>(generateSecret),
              TE.chainFirst((secret) => store.set(key, secret))
            )
          )
        )
      )
    );
