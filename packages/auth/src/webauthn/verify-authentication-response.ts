/* eslint-disable @typescript-eslint/no-explicit-any */
import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { SaveCounter, SelectUser, SaveChallenge, User } from './types';
import * as webauthn from '@simplewebauthn/server';
import { sequenceT } from 'fp-ts/lib/Apply';
import * as jose from 'node-jose';
import getTokens from '../get-tokens';
import { AuthApiImpl } from '@jmorecroft67/auth-api';
import { ServerError, Status } from 'nice-grpc';

type GetJwtPayload<U extends User> = (user: U) => { user: object };

const handler: <U extends User>(options: {
  selectUser: SelectUser<U>;
  saveChallenge: SaveChallenge<U>;
  saveCounter: SaveCounter;
  getJwtPayload: GetJwtPayload<U>;
  getKeyStore: TE.TaskEither<Error, jose.JWK.KeyStore>;
  getRefreshSecret: TE.TaskEither<Error, string>;
  origin: string;
  rpID: string;
  jwt: {
    audience: string;
    issuer: string;
    access: {
      expiresIn: string;
    };
    refresh: {
      expiresIn: string;
    };
  };
}) => AuthApiImpl<any>['verifyAuthenticationResponse'] =
  ({
    selectUser,
    saveChallenge,
    saveCounter,
    getKeyStore,
    getRefreshSecret,
    getJwtPayload,
    ...opts
  }) =>
  ({ email, credential }) =>
    pipe(
      selectUser({ email }),
      TE.bindTo('user'),
      TE.chainFirst(({ user }) =>
        user.currentChallenge === null
          ? TE.left(
              new ServerError(
                Status.INVALID_ARGUMENT,
                'No challenge previously issued.'
              )
            )
          : TE.right(undefined)
      ),
      TE.bind('authenticator', ({ user }) => {
        const authenticator = user.authenticators.find(
          (a) => a.credentialID.toString('base64url') === credential.rawId
        );
        if (!authenticator) {
          return TE.left(
            new ServerError(Status.INVALID_ARGUMENT, 'No authenticator found.')
          );
        }
        return TE.of(authenticator);
      }),
      TE.bind('result', ({ user, authenticator }) =>
        TE.tryCatch(
          () =>
            webauthn.verifyAuthenticationResponse({
              credential,
              // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
              expectedChallenge: user.currentChallenge!,
              expectedOrigin: opts.origin,
              expectedRPID: opts.rpID,
              authenticator,
              requireUserVerification: true
            }),
          (e) => e as Error
        )
      ),
      TE.chain(
        ({
          user,
          result
        }): ReturnType<AuthApiImpl<any>['verifyAuthenticationResponse']> =>
          result.verified
            ? pipe(
                sequenceT(TE.ApplyPar)(
                  saveChallenge({ email: user.email }, null),
                  saveCounter(
                    user.id,
                    result.authenticationInfo.credentialID,
                    result.authenticationInfo.newCounter
                  ),
                  getTokens(getJwtPayload(user), {
                    getKeyStore,
                    getRefreshSecret,
                    jwt: opts.jwt
                  })
                ),
                TE.map(([, , tokens]) => ({
                  type: 'verified',
                  tokens
                }))
              )
            : pipe(
                saveChallenge({ email: user.email }, null),
                TE.map(() => ({
                  type: 'notVerified'
                }))
              )
      )
    );

export default handler;
