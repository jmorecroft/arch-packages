import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { SelectUser, User } from './types';
import { CreateUser } from './types';
import { SaveChallenge } from './types';
import * as webauthn from '@simplewebauthn/server';
import { AuthApiImpl } from '@jmorecroft67/auth-api';
import { BadArgsError } from '@jmorecroft67/api-maker';

const handler: <D extends { email: string }, U extends User>(options: {
  selectUser: SelectUser<U>;
  createUser: CreateUser<D, U>;
  saveChallenge: SaveChallenge<U>;
  rpName: string;
  rpID: string;
}) => AuthApiImpl<D>['generateRegistrationOptions'] =
  ({ selectUser, createUser, saveChallenge, ...opts }) =>
  (userRegData) => {
    return pipe(
      pipe(
        selectUser(userRegData),
        TE.alt(() => createUser(userRegData)),
        TE.chainFirst((user) =>
          user.authenticators.length > 0
            ? TE.left(new BadArgsError('User is already registered.'))
            : TE.right(undefined)
        )
      ),
      TE.bindTo('user'),
      TE.bind('options', ({ user }) =>
        TE.of(
          webauthn.generateRegistrationOptions({
            ...opts,
            userID: user.id.toString(),
            userName: user.email,
            attestationType: 'indirect',
            // Prevent users from re-registering existing authenticators
            excludeCredentials: user.authenticators.map((authenticator) => ({
              id: authenticator.credentialID,
              type: 'public-key'
            })),
            authenticatorSelection: {
              residentKey: 'required',
              userVerification: 'preferred'
            }
          })
        )
      ),
      TE.chainFirst(({ options, user: { email } }) =>
        saveChallenge({ email }, options.challenge)
      )
    );
  };

export default handler;
