/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { CreateAuthenticator, SelectUser, User } from './types';
import { SaveChallenge } from './types';
import * as webauthn from '@simplewebauthn/server';
import { sequenceT } from 'fp-ts/lib/Apply';
import { AuthApiImpl } from '@jmorecroft67/auth-api';
import { ServerError, Status } from 'nice-grpc';

const handler: <U extends User>(options: {
  selectUser: SelectUser<U>;
  saveChallenge: SaveChallenge<U>;
  createAuthenticator: CreateAuthenticator;
  origin: string;
  rpID: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
}) => AuthApiImpl<any>['verifyRegistrationResponse'] =
  ({ selectUser, saveChallenge, createAuthenticator, ...opts }) =>
  ({ email, credential }) =>
    pipe(
      selectUser({ email }),
      TE.chainFirst((user) =>
        user.currentChallenge === null
          ? TE.left(
              new ServerError(
                Status.INVALID_ARGUMENT,
                'No challenge previously issued.'
              )
            )
          : TE.right(undefined)
      ),
      TE.bindTo('user'),
      TE.bind('result', ({ user }) =>
        TE.tryCatch(
          () =>
            webauthn.verifyRegistrationResponse({
              credential,
              expectedChallenge: user.currentChallenge!,
              expectedOrigin: opts.origin,
              expectedRPID: opts.rpID,
              requireUserVerification: true
            }),
          (e): Error =>
            new ServerError(Status.INVALID_ARGUMENT, (e as Error).message)
        )
      ),
      TE.chainFirst(({ user: { id, email }, result }) =>
        result.verified && result.registrationInfo
          ? pipe(
              sequenceT(TE.ApplyPar)(
                saveChallenge({ email }, null),
                createAuthenticator(
                  id,
                  result.registrationInfo.credentialID,
                  result.registrationInfo.credentialPublicKey,
                  result.registrationInfo.credentialDeviceType,
                  result.registrationInfo.credentialBackedUp,
                  result.registrationInfo.counter
                )
              ),
              TE.map(() => undefined)
            )
          : saveChallenge({ email }, null)
      ),
      TE.map(({ result: { verified } }) => ({ verified }))
    );

export default handler;
