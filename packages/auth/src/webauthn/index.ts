export * from './types';
import generateRegistrationOptions from './generate-registration-options';
import verifyRegistrationResponse from './verify-registration-response';
import generateAuthenticationOptions from './generate-authentication-options';
import verifyAuthenticationResponse from './verify-authentication-response';

export {
  generateRegistrationOptions,
  verifyRegistrationResponse,
  generateAuthenticationOptions,
  verifyAuthenticationResponse
};
