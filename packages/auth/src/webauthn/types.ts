import * as TE from 'fp-ts/TaskEither';

export interface Authenticator {
  credentialID: Buffer;
  credentialPublicKey: Buffer;
  counter: number;
}

export interface User {
  id: number;
  email: string;
  currentChallenge: string | null;
  authenticators: Authenticator[];
}

export type UserEmail = Pick<User, 'email'>;

export type SelectUser<U extends User> = (
  key: UserEmail
) => TE.TaskEither<Error, U>;

export type CreateUser<D, U extends User> = (
  data: D
) => TE.TaskEither<Error, U>;

export type SaveChallenge<U extends User> = (
  key: UserEmail,
  challenge: string | null
) => TE.TaskEither<Error, U>;

export type GetJwtPayload<U extends User> = (user: U) => { user: object };

export type CreateAuthenticator = (
  userId: number,
  credentialID: Buffer,
  credentialPublicKey: Buffer,
  credentialDeviceType: string,
  credentialBackedUp: boolean,
  counter: number
) => TE.TaskEither<Error, Authenticator>;

export type SaveCounter = (
  userId: number,
  credentialID: Buffer,
  counter: number
) => TE.TaskEither<Error, Authenticator>;
