import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { SelectUser, SaveChallenge, User } from './types';
import * as webauthn from '@simplewebauthn/server';
import { AuthApiImpl } from '@jmorecroft67/auth-api';
import { ServerError, Status } from 'nice-grpc';

const handler: <U extends User>(options: {
  selectUser: SelectUser<U>;
  saveChallenge: SaveChallenge<U>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
}) => AuthApiImpl<any>['generateAuthenticationOptions'] =
  ({ selectUser, saveChallenge }) =>
  ({ email }) =>
    pipe(
      selectUser({ email }),
      TE.chainFirst((user) =>
        user.authenticators.length === 0
          ? TE.left(
              new ServerError(Status.INVALID_ARGUMENT, 'User not registered.')
            )
          : TE.of(undefined)
      ),
      TE.bindTo('user'),
      TE.bind('options', ({ user }) =>
        TE.of(
          webauthn.generateAuthenticationOptions({
            allowCredentials: user.authenticators.map((authenticator) => ({
              id: authenticator.credentialID,
              type: 'public-key'
            })),
            userVerification: 'preferred'
          })
        )
      ),
      TE.chainFirst(({ options, user: { email } }) =>
        saveChallenge({ email }, options.challenge)
      )
    );

export default handler;
