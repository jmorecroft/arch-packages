import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as jose from 'node-jose';
import { sign } from 'jsonwebtoken';

export default (
    payload: object,
    options: {
      audience: string;
      issuer: string;
      expiresIn: string;
    }
  ) =>
  (ks: jose.JWK.KeyStore): TE.TaskEither<Error, string> => {
    const keys = ks.all({ use: 'sig' });

    if (keys.length === 0) {
      return TE.left(new Error('No signing key found.'));
    }

    return () =>
      new Promise((resolve) =>
        sign(
          payload,
          keys[0].toPEM(true),
          {
            algorithm: 'RS256',
            keyid: keys[0].kid,
            ...options
          },
          (err, encoded) => {
            if (err) {
              resolve(E.left(err));
              return;
            }
            resolve(E.right(encoded as string));
          }
        )
      );
  };
