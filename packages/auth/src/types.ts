import * as TE from 'fp-ts/TaskEither';
import * as O from 'fp-ts/Option';

export interface KeyValStore {
  get(key: string): TE.TaskEither<Error, O.Option<string>>;
  set(key: string, value: string): TE.TaskEither<Error, void>;
}
