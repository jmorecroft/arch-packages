import * as TE from 'fp-ts/TaskEither';
import * as O from 'fp-ts/Option';
import { pipe } from 'fp-ts/lib/function';
import { promisify } from 'util';
import * as fs from 'fs';
import { KeyValStore } from './types';

const fileKeyValStore: KeyValStore = {
  get: (key) =>
    pipe(
      TE.tryCatch(
        () => promisify(fs.readFile)(key),
        (e) => e as NodeJS.ErrnoException
      ),
      TE.map((buf) => O.some(buf.toString())),
      TE.orElse((e) => (e.code === 'ENOENT' ? TE.right(O.none) : TE.left(e)))
    ),
  set: (key, value) =>
    TE.tryCatch(
      () => promisify(fs.writeFile)(key, value),
      (e) => e as NodeJS.ErrnoException
    )
};

export default fileKeyValStore;
