import * as TE from 'fp-ts/TaskEither';
import * as O from 'fp-ts/Option';
import { pipe } from 'fp-ts/lib/function';
import * as jose from 'node-jose';
import { KeyValStore } from './types';

const generateKeyStore: TE.TaskEither<Error, jose.JWK.KeyStore> = TE.tryCatch(
  async () => {
    const keyStore = jose.JWK.createKeyStore();
    // add sigining key
    await keyStore.generate('RSA', 2048, {
      alg: 'RS256',
      use: 'sig'
    });

    return keyStore;
  },
  (e) => e as Error
);

export default (key: string) =>
  (store: KeyValStore): TE.TaskEither<Error, jose.JWK.KeyStore> =>
    pipe(
      store.get(key),
      TE.chain((val) =>
        pipe(
          val,
          O.map((s) =>
            TE.tryCatch(
              () => jose.JWK.asKeyStore(s),
              (e) => e as Error
            )
          ),
          O.getOrElse(() =>
            pipe(
              generateKeyStore,
              TE.chainFirst((ks) =>
                store.set(key, JSON.stringify(ks.toJSON(true)))
              )
            )
          )
        )
      )
    );
