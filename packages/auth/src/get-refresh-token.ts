import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { sign } from 'jsonwebtoken';

export default (
    payload: object,
    options: {
      audience: string;
      issuer: string;
      expiresIn: string;
    }
  ) =>
  (secret: string): TE.TaskEither<Error, string> =>
  () =>
    new Promise((resolve) =>
      sign(payload, secret, options, (err, encoded) => {
        if (err) {
          resolve(E.left(err));
          return;
        }
        resolve(E.right(encoded as string));
      })
    );
