import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { JwtPayload, verify } from 'jsonwebtoken';

export default (
    token: string,
    {
      audience,
      issuer
    }: {
      audience: string;
      issuer: string;
    }
  ) =>
  (secret: string): TE.TaskEither<Error, JwtPayload> => {
    return () =>
      new Promise((resolve) =>
        verify(
          token,
          secret,
          {
            audience,
            issuer
          },
          (err, decoded) => {
            if (err) {
              resolve(E.left(err));
              return;
            }
            resolve(E.right(decoded as JwtPayload));
          }
        )
      );
  };
