import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import * as jose from 'node-jose';
import getAccessToken from './get-access-token';
import getRefreshToken from './get-refresh-token';
import { sequenceS } from 'fp-ts/lib/Apply';

export default (
  payload: { user: object },
  {
    getKeyStore,
    getRefreshSecret,
    jwt: { audience, issuer, refresh, access }
  }: {
    getKeyStore: TE.TaskEither<Error, jose.JWK.KeyStore>;
    getRefreshSecret: TE.TaskEither<Error, string>;
    jwt: {
      audience: string;
      issuer: string;
      refresh: {
        expiresIn: string;
      };
      access: {
        expiresIn: string;
      };
    };
  }
): TE.TaskEither<Error, { access: string; refresh: string }> =>
  sequenceS(TE.ApplyPar)({
    access: pipe(
      getKeyStore,
      TE.chain(getAccessToken(payload, { audience, issuer, ...access }))
    ),
    refresh: pipe(
      getRefreshSecret,
      TE.chain(getRefreshToken(payload, { audience, issuer, ...refresh }))
    )
  });
