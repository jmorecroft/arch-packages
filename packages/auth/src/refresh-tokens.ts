import * as TE from 'fp-ts/TaskEither';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as jose from 'node-jose';
import { pipe } from 'fp-ts/lib/function';
import verifyRefreshToken from './verify-refresh-token';
import getTokens from './get-tokens';
import { AuthApiImpl } from '@jmorecroft67/auth-api';
import { ServerError, Status } from 'nice-grpc';

const handler: (options: {
  getKeyStore: TE.TaskEither<Error, jose.JWK.KeyStore>;
  getRefreshSecret: TE.TaskEither<Error, string>;
  jwt: {
    audience: string;
    issuer: string;
    access: {
      expiresIn: string;
    };
    refresh: {
      expiresIn: string;
    };
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
}) => AuthApiImpl<any>['refreshTokens'] =
  ({ getKeyStore, getRefreshSecret, ...opts }) =>
  ({ refresh }) =>
    pipe(
      getRefreshSecret,
      TE.chain((secret) =>
        pipe(
          secret,
          pipe(
            verifyRefreshToken(refresh, opts.jwt),
            RTE.mapLeft(
              (e) => new ServerError(Status.PERMISSION_DENIED, e.message)
            ),
            RTE.chainTaskEitherK(({ user }) =>
              getTokens(
                { user },
                {
                  getKeyStore,
                  getRefreshSecret: TE.of(secret),
                  jwt: opts.jwt
                }
              )
            )
          )
        )
      ),
      TE.map((tokens) => ({ tokens }))
    );

export default handler;
