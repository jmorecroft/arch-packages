import * as jose from 'node-jose';
import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { AuthApiImpl } from '@jmorecroft67/auth-api';

const handler: (
  getKeyStore: TE.TaskEither<Error, jose.JWK.KeyStore>
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
) => AuthApiImpl<any>['getJwksJson'] = (getKeyStore) => () =>
  pipe(
    getKeyStore,
    TE.map((ks) => ({ jwksJson: JSON.stringify(ks.toJSON()) }))
  );

export default handler;
