import { flow, pipe } from 'fp-ts/lib/function';
import * as RTE from 'fp-ts/ReaderTaskEither';
import * as O from 'fp-ts/Option';

const memoize = <R, A>(
  get: (key: string) => RTE.ReaderTaskEither<R, Error, A>
): ((key: string) => RTE.ReaderTaskEither<R, Error, A>) => {
  const cache = new Map<string, A>();
  return (key: string) =>
    pipe(
      RTE.fromIO<O.Option<A>, R, Error>(() => {
        const val = cache.get(key);
        if (val) {
          return O.some(val);
        }
        return O.none;
      }),
      RTE.chain(
        flow(
          O.fold(
            () =>
              pipe(
                get(key),
                RTE.chainFirst((val) => {
                  cache.set(key, val);
                  return RTE.of(undefined);
                })
              ),
            (val) => RTE.of(val)
          )
        )
      )
    );
};

export default memoize;
