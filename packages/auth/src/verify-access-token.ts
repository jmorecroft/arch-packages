import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as jose from 'node-jose';
import { JwtPayload, verify } from 'jsonwebtoken';

export default (
    token: string,
    {
      audience,
      issuer
    }: {
      audience: string;
      issuer: string;
    }
  ) =>
  (ks: jose.JWK.KeyStore): TE.TaskEither<Error, JwtPayload> => {
    const keys = ks.all({ use: 'sig' });

    if (keys.length === 0) {
      return TE.left(new Error('No signing key found.'));
    }

    return () =>
      new Promise((resolve) =>
        verify(
          token,
          keys[0].toPEM(),
          {
            audience,
            issuer
          },
          (err, decoded) => {
            if (err) {
              resolve(E.left(err));
              return;
            }
            resolve(E.right(decoded as JwtPayload));
          }
        )
      );
  };
