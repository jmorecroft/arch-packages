# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0...@jmorecroft67/auth@1.0.1) (2023-01-05)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.10...@jmorecroft67/auth@1.0.0) (2023-01-04)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.10](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.9...@jmorecroft67/auth@1.0.0-staging.10) (2022-12-15)


### Features

* **@jmorecroft67/auth:** add support for passkeys per https://simplewebauthn.dev/docs/advanced/passkeys ([bf5f023](https://gitlab.com/jmorecroft/arch-packages/commit/bf5f023a35c81894dda1d600367d040aaa6ab167))





# [1.0.0-staging.9](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.8...@jmorecroft67/auth@1.0.0-staging.9) (2022-12-13)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.8](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.7...@jmorecroft67/auth@1.0.0-staging.8) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.6...@jmorecroft67/auth@1.0.0-staging.7) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.5...@jmorecroft67/auth@1.0.0-staging.6) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.4...@jmorecroft67/auth@1.0.0-staging.5) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.3...@jmorecroft67/auth@1.0.0-staging.4) (2022-12-02)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.2...@jmorecroft67/auth@1.0.0-staging.3) (2022-12-01)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.1...@jmorecroft67/auth@1.0.0-staging.2) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/auth





# [1.0.0-staging.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@1.0.0-staging.0...@jmorecroft67/auth@1.0.0-staging.1) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/auth





# 1.0.0-staging.0 (2022-11-28)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** Exporting additional schema types. ([04bbb7c](https://gitlab.com/jmorecroft/arch-packages/commit/04bbb7caa02dabe4e6f160a6890323132cd75753))


### Features

* **@jmorecroft67/auth:** auth major refactor ([fe920fb](https://gitlab.com/jmorecroft/arch-packages/commit/fe920fb5546f305844376a17309689e2482e2b0a))


### BREAKING CHANGES

* **@jmorecroft67/auth:** @jmorecroft67/auth refactored from an express app to an unauthenticated GRPC service.





# [0.2.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@0.1.1...@jmorecroft67/auth@0.2.0) (2022-07-22)


### Features

* **@jmorecroft67/api-maker:** More simplification, extra convenience maker method. ([db1f160](https://gitlab.com/jmorecroft/arch-packages/commit/db1f160521dc62f160908b15499438a8c83496dc))





## [0.1.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth@0.1.0...@jmorecroft67/auth@0.1.1) (2022-07-19)


### Bug Fixes

* **@jmorecroft67/auth:** Fix types, memoize, tweak factory function inputs. ([dac290d](https://gitlab.com/jmorecroft/arch-packages/commit/dac290d73ae5d7231c46f7dd4d4699e861cd13a9))





# 0.1.0 (2022-07-18)


### Bug Fixes

* **@jmorecroft67/auth:** Fix error handler. ([38c0bd8](https://gitlab.com/jmorecroft/arch-packages/commit/38c0bd8f080e2608a7debe7b277af0008cbc078b))


### Features

* **@jmorecroft67/auth:** New lib for auth-related components. ([d8a56ca](https://gitlab.com/jmorecroft/arch-packages/commit/d8a56ca89966111d9df75001c99ed6db3fd9a641))
