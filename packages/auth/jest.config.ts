// jest.config.ts
import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  preset: 'ts-jest',
  roots: ['<rootDir>/src', '<rootDir>/test'],
  testEnvironment: 'node',
  verbose: true,
  moduleDirectories: ['node_modules'],
  moduleNameMapper: {
    'test-utils': '<rootDir>/test/test-utils'
  },
  setupFiles: ['dotenv/config'],
  setupFilesAfterEnv: ['jest-extended/all']
};

export default config;
