/* eslint-disable @typescript-eslint/no-explicit-any */
import addAuthService from '../src/add-auth-service';
import * as TE from 'fp-ts/TaskEither';
import { schemable as S } from '@jmorecroft67/io-ts-types';
import { createLogger } from 'bunyan';

export const testUserInfoSchema = S.make((S) =>
  S.struct({
    email: S.string
  })
);

type Options = Parameters<typeof addAuthService>[0];
export const addTestAuthService = (
  options: Pick<Options, 'api'> & Partial<Options>
) => {
  const defaults = {
    selectUser: jest.fn(),
    createUser: jest.fn(),
    saveChallenge: jest.fn(),
    createAuthenticator: jest.fn(),
    saveCounter: jest.fn(),
    getRefreshSecret: TE.left(new Error('No secret')),
    getKeyStore: TE.left(new Error('No keystore')),
    getJwtPayload: (user: any) => ({
      user: { id: user.id, email: user.email }
    }),
    rpID: 'rpID',
    rpName: 'rpName',
    origin: 'http://localhost',
    jwt: {
      access: {
        expiresIn: '5s'
      },
      refresh: {
        expiresIn: '1d'
      },
      audience: 'testaud',
      issuer: 'testiss'
    },
    logger: createLogger({
      name: 'test',
      level: 'debug'
    })
  };
  return addAuthService({
    ...defaults,
    ...options
  });
};
