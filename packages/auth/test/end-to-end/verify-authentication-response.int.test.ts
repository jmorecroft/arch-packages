/* eslint-disable @typescript-eslint/no-explicit-any */
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as webauthn from '@simplewebauthn/server';
import { addTestAuthService, testUserInfoSchema } from 'test-utils';
import * as jose from 'node-jose';
import { verify } from 'jsonwebtoken';
import {
  createChannel,
  createClient,
  createServer,
  Server,
  Status,
  ChannelCredentials
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import { ApiClient, makeClient } from '@jmorecroft67/api-maker';
import makeApi from '@jmorecroft67/auth-api';

jest.mock('@simplewebauthn/server');

const webauthnMocked = webauthn as jest.Mocked<typeof webauthn>;

describe(__filename, () => {
  const selectUser = jest.fn();
  const saveChallenge = jest.fn();
  const saveCounter = jest.fn();

  const refreshSecret = 'secret';

  const keyStore = jose.JWK.createKeyStore();

  let signingKey: jose.JWK.Key;

  const api = makeApi(testUserInfoSchema);

  let server: Server;
  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    server = createServer();
    addTestAuthService({
      api,
      getRefreshSecret: TE.of(refreshSecret),
      getKeyStore: TE.of(keyStore),
      selectUser,
      saveChallenge,
      saveCounter
    })(server);
    const channel = await listen();
    client = makeClient(api)((def) => createClient(def, channel));
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  beforeAll(async () => {
    signingKey = await keyStore.generate('RSA', 2048, {
      alg: 'RS256',
      use: 'sig'
    });
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  const verification: Awaited<
    ReturnType<typeof webauthn.verifyAuthenticationResponse>
  > = {
    verified: true,
    authenticationInfo: {
      userVerified: true,
      credentialBackedUp: true,
      credentialDeviceType: 'singleDevice',
      credentialID: Buffer.from('1234'),
      newCounter: 99
    }
  };

  const credential: Parameters<
    typeof client.verifyAuthenticationResponse
  >[0]['credential'] = {
    id: '123',
    rawId: Buffer.from('456789').toString('base64url'),
    type: 'type',
    clientExtensionResults: {},
    response: {
      authenticatorData: 'authData',
      clientDataJSON: 'clientData',
      signature: 'sig'
    }
  };

  it('should verify authenticate', async () => {
    const authenticator = {
      credentialID: Buffer.from(credential.rawId, 'base64url')
    };
    const user = {
      id: 1,
      email: 'a@b.com',
      currentChallenge: '123',
      authenticators: [authenticator]
    };

    selectUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));
    saveCounter.mockImplementation(() => TE.of(undefined));

    webauthnMocked.verifyAuthenticationResponse.mockResolvedValue(verification);

    const { id, email } = user;
    const resp = await client.verifyAuthenticationResponse({
      email,
      credential
    })();

    expect(resp).toEqual(
      E.right({
        type: 'verified',
        tokens: { access: expect.any(String), refresh: expect.any(String) }
      })
    );
    expect(
      verify((resp as any).right.tokens.access, signingKey.toPEM(), {
        audience: 'testaud',
        issuer: 'testiss'
      })
    ).toEqual(
      expect.objectContaining({
        user: { id, email }
      })
    );
    expect(
      verify((resp as any).right.tokens.refresh, refreshSecret, {
        audience: 'testaud',
        issuer: 'testiss'
      })
    ).toEqual(
      expect.objectContaining({
        user: { id, email }
      })
    );
    expect(selectUser).toHaveBeenCalledWith({ email });
    expect(saveChallenge).toHaveBeenCalledWith({ email }, null);
    expect(saveCounter).toHaveBeenCalledWith(
      1,
      verification.authenticationInfo.credentialID,
      verification.authenticationInfo.newCounter
    );
    expect(webauthnMocked.verifyAuthenticationResponse).toHaveBeenCalledWith({
      credential,
      expectedChallenge: '123',
      expectedOrigin: 'http://localhost',
      expectedRPID: 'rpID',
      authenticator,
      requireUserVerification: true
    });
  });

  it('should not verify authenticate if no authenticator found', async () => {
    const authenticator = { credentialID: Buffer.from('123') };
    const user = {
      id: 1,
      email: 'a@b.com',
      currentChallenge: '123',
      authenticators: [authenticator]
    };

    selectUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));
    saveCounter.mockImplementation(() => TE.of(undefined));

    const { email } = user;
    const resp = await client.verifyAuthenticationResponse({
      email,
      credential
    })();

    expect(selectUser).toHaveBeenCalledWith({ email });
    expect(resp).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INVALID_ARGUMENT,
          details: 'No authenticator found.'
        })
      )
    );
  });

  it('should clear challenge on verify authenticate fail', async () => {
    const authenticator = {
      credentialID: Buffer.from(credential.rawId, 'base64url')
    };
    const user = {
      id: 1,
      email: 'a@b.com',
      currentChallenge: '123',
      authenticators: [authenticator]
    };

    selectUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));
    saveCounter.mockImplementation(() => TE.of(undefined));

    webauthnMocked.verifyAuthenticationResponse.mockResolvedValue({
      ...verification,
      verified: false
    });

    const { email } = user;
    const resp = await client.verifyAuthenticationResponse({
      email,
      credential
    })();

    expect(resp).toEqual(
      E.right({
        type: 'notVerified'
      })
    );
    expect(selectUser).toHaveBeenCalledWith({ email });
    expect(saveChallenge).toHaveBeenCalledWith({ email }, null);
    expect(webauthnMocked.verifyAuthenticationResponse).toHaveBeenCalledWith({
      credential,
      expectedChallenge: '123',
      expectedOrigin: 'http://localhost',
      expectedRPID: 'rpID',
      authenticator,
      requireUserVerification: true
    });
  });
});
