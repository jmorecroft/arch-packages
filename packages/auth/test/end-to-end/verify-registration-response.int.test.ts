/* eslint-disable @typescript-eslint/no-explicit-any */
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as webauthn from '@simplewebauthn/server';
import { addTestAuthService, testUserInfoSchema } from 'test-utils';
import {
  createChannel,
  createClient,
  createServer,
  Server,
  ChannelCredentials,
  Status
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import { ApiClient, makeClient } from '@jmorecroft67/api-maker';
import makeApi from '@jmorecroft67/auth-api';

jest.mock('@simplewebauthn/server');

const webauthnMocked = webauthn as jest.Mocked<typeof webauthn>;

describe(__filename, () => {
  const selectUser = jest.fn();
  const saveChallenge = jest.fn();
  const createAuthenticator = jest.fn();

  const api = makeApi(testUserInfoSchema);

  let server: Server;
  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    server = createServer();
    addTestAuthService({
      api,
      selectUser,
      saveChallenge,
      createAuthenticator
    })(server);
    const channel = await listen();
    client = makeClient(api)((def) => createClient(def, channel));
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  beforeEach(() => {
    jest.resetAllMocks();
  });
  const verification: Awaited<
    ReturnType<typeof webauthn.verifyRegistrationResponse>
  > = {
    verified: true,
    registrationInfo: {
      aaguid: 'aaguid',
      attestationObject: Buffer.from('123'),
      credentialBackedUp: true,
      credentialDeviceType: 'multiDevice',
      credentialType: 'public-key',
      fmt: 'apple',
      userVerified: true,
      credentialID: Buffer.from('456'),
      credentialPublicKey: Buffer.from('789'),
      counter: 99
    }
  };

  const credential: Parameters<
    typeof client.verifyRegistrationResponse
  >[0]['credential'] = {
    id: '123',
    rawId: '456',
    type: 'type',
    clientExtensionResults: {},
    response: {
      attestationObject: 'attestation',
      clientDataJSON: 'clientData'
    }
  };

  it('should verify register', async () => {
    const user = {
      id: 1,
      email: 'a@b.com',
      currentChallenge: '123',
      authenticators: []
    };

    selectUser.mockImplementation(() => TE.of(user));
    createAuthenticator.mockImplementation(() => TE.of(undefined));
    saveChallenge.mockImplementation(() => TE.of(undefined));

    webauthnMocked.verifyRegistrationResponse.mockResolvedValue(verification);

    const { email } = user;
    const resp = await client.verifyRegistrationResponse({
      email,
      credential
    })();

    expect(resp).toEqual(E.right({ verified: true }));
    expect(selectUser).toHaveBeenCalledWith({ email });
    expect(saveChallenge).toHaveBeenCalledWith({ email }, null);
    expect(createAuthenticator).toHaveBeenCalledWith(
      1,
      verification.registrationInfo?.credentialID,
      verification.registrationInfo?.credentialPublicKey,
      verification.registrationInfo?.credentialDeviceType,
      verification.registrationInfo?.credentialBackedUp,
      verification.registrationInfo?.counter
    );
    expect(webauthnMocked.verifyRegistrationResponse).toHaveBeenCalledWith({
      credential,
      expectedChallenge: '123',
      expectedOrigin: 'http://localhost',
      expectedRPID: 'rpID',
      requireUserVerification: true
    });
  });

  it('should not verify register when no challenge', async () => {
    const user = {
      id: 1,
      email: 'a@b.com',
      currentChallenge: null,
      authenticators: []
    };

    selectUser.mockImplementation(() => TE.of(user));

    const { email } = user;

    const resp = await client.verifyRegistrationResponse({
      email,
      credential
    })();

    expect(selectUser).toHaveBeenCalledWith({ email });
    expect(resp).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INVALID_ARGUMENT,
          details: 'No challenge previously issued.'
        })
      )
    );
  });

  it('should clear challenge on verify register fail', async () => {
    const user = {
      id: 1,
      email: 'a@b.com',
      currentChallenge: '123',
      authenticators: []
    };

    selectUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));

    webauthnMocked.verifyRegistrationResponse.mockResolvedValue({
      verified: false
    });

    const { email } = user;
    const resp = await client.verifyRegistrationResponse({
      email,
      credential
    })();

    expect(resp).toEqual(E.right({ verified: false }));
    expect(selectUser).toHaveBeenCalledWith({ email });
    expect(saveChallenge).toHaveBeenCalledWith({ email }, null);
    expect(webauthnMocked.verifyRegistrationResponse).toHaveBeenCalledWith({
      credential,
      expectedChallenge: '123',
      expectedOrigin: 'http://localhost',
      expectedRPID: 'rpID',
      requireUserVerification: true
    });
  });
});
