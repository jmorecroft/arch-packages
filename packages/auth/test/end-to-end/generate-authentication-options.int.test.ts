/* eslint-disable @typescript-eslint/no-explicit-any */
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as webauthn from '@simplewebauthn/server';
import { testUserInfoSchema, addTestAuthService } from 'test-utils';
import makeApi from '@jmorecroft67/auth-api';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server,
  Status
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import { ApiClient, makeClient } from '@jmorecroft67/api-maker';

jest.mock('@simplewebauthn/server');

const webauthnMocked = webauthn as jest.Mocked<typeof webauthn>;

describe(__filename, () => {
  const selectUser = jest.fn();
  const saveChallenge = jest.fn();

  const api = makeApi(testUserInfoSchema);

  let server: Server;
  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    server = createServer();
    addTestAuthService({
      api,
      selectUser,
      saveChallenge
    })(server);
    const channel = await listen();
    client = makeClient(api)((def) => createClient(def, channel));
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  beforeEach(() => {
    jest.resetAllMocks();
  });

  const options: ReturnType<typeof webauthn.generateAuthenticationOptions> = {
    challenge: '1234567'
  };

  it('should generate options', async () => {
    const authenticators = [{ credentialID: '123' }];

    const user = {
      id: 1,
      email: 'a@b.com',
      authenticators
    };

    selectUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));

    webauthnMocked.generateAuthenticationOptions.mockReturnValue(options);

    const args = { email: user.email };
    const resp = await client.generateAuthenticationOptions(args)();

    expect(resp).toEqual(E.right({ options }));
    expect(selectUser).toHaveBeenCalledWith(args);
    expect(saveChallenge).toHaveBeenCalledWith(args, '1234567');
    expect(webauthnMocked.generateAuthenticationOptions).toHaveBeenCalledWith({
      allowCredentials: [
        {
          id: '123',
          type: 'public-key'
        }
      ],
      userVerification: 'preferred'
    });
  });

  it('should not generate options if not registered', async () => {
    const user = {
      id: 1,
      email: 'a@b.com',
      authenticators: []
    };

    selectUser.mockImplementation(() => TE.of(user));

    const args = { email: user.email };
    const resp = await client.generateAuthenticationOptions(args)();

    expect(resp).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INVALID_ARGUMENT,
          details: 'User not registered.'
        })
      )
    );
    expect(selectUser).toHaveBeenCalledWith(args);
  });
});
