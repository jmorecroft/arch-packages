/* eslint-disable @typescript-eslint/no-explicit-any */
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as jose from 'node-jose';
import { sign, verify } from 'jsonwebtoken';
import { testUserInfoSchema, addTestAuthService } from 'test-utils';
import {
  createChannel,
  createClient,
  createServer,
  Server,
  Status,
  ChannelCredentials
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import { ApiClient, makeClient } from '@jmorecroft67/api-maker';
import makeApi from '@jmorecroft67/auth-api';

describe(__filename, () => {
  const refreshSecret = 'secret';

  const keyStore = jose.JWK.createKeyStore();

  let signingKey: jose.JWK.Key;

  const api = makeApi(testUserInfoSchema);
  let server: Server;
  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    server = createServer();
    addTestAuthService({
      api,
      getRefreshSecret: TE.of(refreshSecret),
      getKeyStore: TE.of(keyStore)
    })(server);
    const channel = await listen();
    client = makeClient(api)((def) => createClient(def, channel));
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  beforeAll(async () => {
    signingKey = await keyStore.generate('RSA', 2048, {
      alg: 'RS256',
      use: 'sig'
    });
  });

  it('should refresh tokens', async () => {
    const user = { id: 1, email: 'a@b.com' };

    const refresh = sign({ user }, refreshSecret, {
      audience: 'testaud',
      issuer: 'testiss',
      expiresIn: '1d'
    });

    const resp = await client.refreshTokens({ refresh })();

    expect(resp).toEqual(
      E.right({
        tokens: {
          access: expect.any(String),
          refresh: expect.any(String)
        }
      })
    );
    expect(
      verify((resp as any).right.tokens.access, signingKey.toPEM(), {
        audience: 'testaud',
        issuer: 'testiss',
        complete: true
      }).payload
    ).toEqual(expect.objectContaining({ user }));
    expect(
      verify((resp as any).right.tokens.refresh, refreshSecret, {
        audience: 'testaud',
        issuer: 'testiss',
        complete: true
      }).payload
    ).toEqual(expect.objectContaining({ user }));
  });

  it('should fail, wrong secret', async () => {
    const refresh = sign({}, 'wrongSecret', {
      audience: 'testaud',
      issuer: 'testiss',
      expiresIn: '1d'
    });

    const resp = await client.refreshTokens({ refresh })();

    expect(resp).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.PERMISSION_DENIED,
          details: 'invalid signature'
        })
      )
    );
  });
});
