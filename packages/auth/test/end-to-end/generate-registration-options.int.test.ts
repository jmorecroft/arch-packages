/* eslint-disable @typescript-eslint/no-explicit-any */
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import * as webauthn from '@simplewebauthn/server';
import { testUserInfoSchema, addTestAuthService } from 'test-utils';
import * as jose from 'node-jose';
import makeApi from '@jmorecroft67/auth-api';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import { ApiClient, makeClient } from '@jmorecroft67/api-maker';

jest.mock('@simplewebauthn/server');

const webauthnMocked = webauthn as jest.Mocked<typeof webauthn>;

describe(__filename, () => {
  const selectUser = jest.fn();
  const createUser = jest.fn();
  const saveChallenge = jest.fn();
  const keyStore = jose.JWK.createKeyStore();

  //let signingKey: jose.JWK.Key;

  const api = makeApi(testUserInfoSchema);

  let server: Server;
  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    server = createServer();
    addTestAuthService({
      api,
      getKeyStore: TE.of(keyStore),
      selectUser,
      createUser,
      saveChallenge
    })(server);
    const channel = await listen();
    client = makeClient(api)((def) => createClient(def, channel));
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  /*beforeAll(async () => {
    signingKey = await keyStore.generate('RSA', 2048, {
      alg: 'RS256',
      use: 'sig'
    });
  });*/
  beforeEach(() => {
    jest.resetAllMocks();
  });

  const options: ReturnType<typeof webauthn.generateRegistrationOptions> = {
    challenge: '123',
    excludeCredentials: [
      {
        id: 'id',
        type: 'public-key',
        transports: ['nfc']
      }
    ],
    pubKeyCredParams: [
      {
        alg: 123,
        type: 'public-key'
      }
    ],
    rp: {
      name: 'rp',
      id: 'id'
    },
    user: {
      displayName: 'displayName',
      id: 'id',
      name: 'name'
    }
  };

  it('should generate options, new user', async () => {
    const user = { id: 1, email: 'a@b.com', authenticators: [] };

    selectUser.mockImplementation(() => TE.left(new Error('User not found.')));
    createUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));

    webauthnMocked.generateRegistrationOptions.mockReturnValue(options);

    const args = { email: user.email };
    const resp = await client.generateRegistrationOptions(args)();

    expect(resp).toEqual(E.right({ options }));
    expect(selectUser).toHaveBeenCalledWith(args);
    expect(createUser).toHaveBeenCalledWith(args);
    expect(saveChallenge).toHaveBeenCalledWith(args, '123');
    expect(webauthnMocked.generateRegistrationOptions).toHaveBeenCalledWith({
      attestationType: 'indirect',
      authenticatorSelection: {
        residentKey: 'required',
        userVerification: 'preferred'
      },
      excludeCredentials: [],
      rpID: 'rpID',
      rpName: 'rpName',
      userID: '1',
      userName: 'a@b.com'
    });
  });

  it('should generate options, existing user with no authenticators', async () => {
    const user = { id: 1, email: 'a@b.com', authenticators: [] };

    selectUser.mockImplementation(() => TE.of(user));
    saveChallenge.mockImplementation(() => TE.of(undefined));

    webauthnMocked.generateRegistrationOptions.mockReturnValue(options);

    const args = { email: user.email };
    const resp = await client.generateRegistrationOptions(args)();

    expect(resp).toEqual(E.right({ options }));
    expect(webauthnMocked.generateRegistrationOptions).toHaveBeenCalledWith({
      attestationType: 'indirect',
      authenticatorSelection: {
        residentKey: 'required',
        userVerification: 'preferred'
      },
      excludeCredentials: [],
      rpID: 'rpID',
      rpName: 'rpName',
      userID: '1',
      userName: 'a@b.com'
    });
    expect(selectUser).toHaveBeenCalledWith(args);
    expect(saveChallenge).toHaveBeenCalledWith(args, '123');
  });

  it('should not generate options, existing user with authenticators', async () => {
    const user = {
      id: 1,
      email: 'a@b.com',
      authenticators: [{ credentialID: '123' }]
    };

    selectUser.mockImplementation(() => TE.of(user));

    const args = { email: user.email };
    await client.generateRegistrationOptions(args)();

    expect(selectUser).toHaveBeenCalledWith(args);
  });

  /*it('should generate options, existing authenticated user with authenticators', async () => {
    const user = {
      id: 1,
      email: 'a@b.com'
    };

    selectUser.mockImplementation(() =>
      TE.of({ ...user, authenticators: [{ credentialID: '123' }] })
    );
    saveChallenge.mockImplementation(() => TE.of(undefined));

    const options = { challenge: '123' };
    webauthnMocked.generateRegistrationOptions.mockReturnValue(options as any);

    const token = sign({ user }, signingKey.toPEM(true), {
      audience: 'testaud',
      issuer: 'testiss',
      expiresIn: '5s',
      algorithm: 'RS256',
      keyid: signingKey.kid
    });
    const resp = await request(app)
      .get('/generate-extra-register-options')
      .set('Authorization', `Bearer ${token}`)
      .expect(200);

    expect(resp.body).toEqual({ options });
    expect(webauthnMocked.generateRegistrationOptions).toHaveBeenCalledWith({
      attestationType: 'indirect',
      excludeCredentials: [
        {
          id: '123',
          type: 'public-key'
        }
      ],
      rpID: 'rpID',
      rpName: 'rpName',
      userID: '1',
      userName: 'a@b.com'
    });
    expect(saveChallenge).toHaveBeenCalledWith({ email: user.email }, '123');
  });*/
});
