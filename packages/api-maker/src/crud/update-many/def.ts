import {
  schemable as SC,
  schemableLight as SCL,
  schemas as S
} from '@jmorecroft67/io-ts-types';
import { pipe } from 'fp-ts/lib/function';
import { unaryDef } from '../../core';

export default <
  T extends Record<string, unknown>,
  D,
  C extends S.Spec<T> = S.Spec<T>
>({
  entity,
  data,
  constraint
}: {
  entity: SCL.Schema<T>;
  data: SC.Schema<D>;
  constraint?: SC.Schema<C>;
}) => {
  const { spec } = SCL.interpreter(S.Schemable)(entity);

  const whereSchema = constraint
    ? SC.make((S) => pipe(spec(S), S.intersect(constraint(S))))
    : spec;

  const req = SC.make((S) =>
    S.struct({
      data: data(S),
      where: whereSchema(S)
    })
  );

  const res = SC.make((S) =>
    S.struct({
      count: S.number
    })
  );

  return unaryDef(req, res);
};
