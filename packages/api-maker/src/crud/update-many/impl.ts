import * as TE from 'fp-ts/TaskEither';
import { ServerError, Status } from 'nice-grpc-common';
import { schemas as S } from '@jmorecroft67/io-ts-types';

export type Updated = { count: number };

export interface Db<T, D, C> {
  updateMany(options: { data: D; where: S.Spec<T> & C }): Promise<Updated>;
}

export default <T, D, C>(db: Db<T, D, C>) =>
  (options: { data: D; where: S.Spec<T> & C }) =>
    TE.tryCatch(
      () => db.updateMany(options),
      (e) => new ServerError(Status.INTERNAL, (e as Error).message)
    );
