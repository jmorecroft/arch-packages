import { schemable as SC } from '@jmorecroft67/io-ts-types';
import { unaryDef } from '../../core';

export default <D>(data: SC.Schema<D>) => {
  const req = SC.make((S) =>
    S.struct({
      data: S.array(data(S))
    })
  );
  const res = SC.make((S) =>
    S.struct({
      count: S.number
    })
  );
  return unaryDef(req, res);
};
