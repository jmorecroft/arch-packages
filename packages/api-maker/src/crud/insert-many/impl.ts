import * as TE from 'fp-ts/TaskEither';
import { ServerError, Status } from 'nice-grpc-common';

export type Inserted = { count: number };

export interface Db<D> {
  createMany(options: { data: D[] }): Promise<Inserted>;
}

export default <D>(db: Db<D>) =>
  (options: { data: D[] }) =>
    TE.tryCatch(
      () => db.createMany(options),
      (e) => {
        const err = e as Error & { code?: string };
        // Map any known Prisma error codes.
        if (err.code === 'P2002') {
          return new ServerError(Status.ALREADY_EXISTS, err.message);
        }
        return new ServerError(Status.INTERNAL, err.message);
      }
    );
