import { def as insertDef, impl as insertImpl } from './insert';
import { def as updateDef, impl as updateImpl } from './update';
import { def as deleteDef, impl as deleteImpl } from './delete';
import { def as insertManyDef, impl as insertManyImpl } from './insert-many';
import { def as updateManyDef, impl as updateManyImpl } from './update-many';
export {
  insertDef,
  updateDef,
  deleteDef,
  insertManyDef,
  updateManyDef,
  insertImpl,
  updateImpl,
  deleteImpl,
  insertManyImpl,
  updateManyImpl
};
