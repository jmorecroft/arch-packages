import {
  schemable as SC,
  schemableLight as SCL
} from '@jmorecroft67/io-ts-types';
import { unaryDef } from '../../core';

export default <T extends Record<string, unknown>, D, K>({
  entity,
  data,
  key
}: {
  entity: SCL.Schema<T>;
  data: SC.Schema<D>;
  key: SC.Schema<K>;
}) => {
  const req = SC.make((S) =>
    S.struct({
      data: data(S),
      where: key(S)
    })
  );
  return unaryDef(req, entity);
};
