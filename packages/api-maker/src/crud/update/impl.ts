import * as TE from 'fp-ts/TaskEither';
import { ServerError, Status } from 'nice-grpc-common';

export interface Db<T, D, K> {
  update(options: { data: D; where: K }): Promise<T>;
}

export default <T, D, K>(db: Db<T, D, K>) =>
  (options: { data: D; where: K }) =>
    TE.tryCatch(
      () => db.update(options),
      (e) => {
        const err = e as Error & { code?: string };
        // Map any known Prisma error codes.
        if (err.code === 'P2025') {
          return new ServerError(Status.NOT_FOUND, err.message);
        }
        if (err.code === 'P2002') {
          return new ServerError(Status.ALREADY_EXISTS, err.message);
        }
        return new ServerError(Status.INTERNAL, err.message);
      }
    );
