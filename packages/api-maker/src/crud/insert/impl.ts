import * as TE from 'fp-ts/TaskEither';
import { ServerError, Status } from 'nice-grpc-common';

export interface Db<T, D> {
  create(options: { data: D }): Promise<T>;
}

export default <T, D>(db: Db<T, D>) =>
  (options: { data: D }) =>
    TE.tryCatch(
      () => db.create(options),
      (e) => {
        const err = e as Error & { code?: string };
        // Map any known Prisma error codes.
        if (err.code === 'P2002') {
          return new ServerError(Status.ALREADY_EXISTS, err.message);
        }
        return new ServerError(Status.INTERNAL, err.message);
      }
    );
