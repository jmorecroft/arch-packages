import {
  schemable as SC,
  schemableLight as SCL
} from '@jmorecroft67/io-ts-types';
import { unaryDef } from '../../core';

export default <T extends Record<string, unknown>, D>({
  entity,
  data
}: {
  entity: SCL.Schema<T>;
  data: SC.Schema<D>;
}) => {
  const req = SC.make((S) =>
    S.struct({
      data: data(S)
    })
  );
  return unaryDef(req, entity);
};
