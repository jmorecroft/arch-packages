import * as TE from 'fp-ts/TaskEither';
import { ServerError, Status } from 'nice-grpc-common';

export interface Db<T, K> {
  delete(options: { where: K }): Promise<T>;
}

export default <T, K>(db: Db<T, K>) =>
  (options: { where: K }) =>
    TE.tryCatch(
      () => db.delete(options),
      (e) => {
        const err = e as Error & { code?: string };
        // Map any known Prisma error codes.
        if (err.code === 'P2025') {
          return new ServerError(Status.NOT_FOUND, err.message);
        }
        return new ServerError(Status.INTERNAL, err.message);
      }
    );
