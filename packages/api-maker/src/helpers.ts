import { Eq } from 'fp-ts/lib/Eq';
import { schemable as SC } from '@jmorecroft67/io-ts-types';
import * as TE from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/lib/function';
import { sequenceS } from 'fp-ts/lib/Apply';

export type ChangeType<T> = SC.TypeOf<ReturnType<typeof makeChangeSchema<T>>>;

export type ChangesType<T> = SC.TypeOf<ReturnType<typeof makeChangesSchema<T>>>;

export type SnapshotType<T> = SC.TypeOf<
  ReturnType<typeof makeSnapshotSchema<T>>
>;

export type SnapshotOrChangesType<T> = SnapshotType<T> | ChangesType<T>;

export const equals: Eq<{ id: number }> = { equals: (x, y) => x.id === y.id };

export const updatedAt = (obj: { updatedAt: Date }) => obj.updatedAt;

export const stdStreamServerOptions = { equals, updatedAt };

export const makeChangeSchema = <T>(entity: SC.Schema<T>) =>
  SC.make((S) =>
    S.sum('type')({
      Insert: S.struct({
        type: S.literal('Insert'),
        newRecord: entity(S)
      }),
      Update: S.struct({
        type: S.literal('Update'),
        oldRecord: entity(S),
        newRecord: entity(S)
      }),
      Delete: S.struct({
        type: S.literal('Delete'),
        oldRecord: entity(S)
      })
    })
  );

const makeSnapshotSchema = <T>(entity: SC.Schema<T>) =>
  SC.make((S) =>
    S.struct({
      type: S.literal('snapshot'),
      values: S.readonly(S.array(entity(S)))
    })
  );

const makeChangesSchema = <T>(entity: SC.Schema<T>) =>
  SC.make((S) =>
    S.struct({
      type: S.literal('changes'),
      changes: S.readonly(S.array(makeChangeSchema(entity)(S)))
    })
  );

export const makeSnapshotOrChangesSchema = <T>(entity: SC.Schema<T>) =>
  SC.make((S) =>
    S.sum('type')({
      snapshot: makeSnapshotSchema(entity)(S),
      changes: makeChangesSchema(entity)(S)
    })
  );

export const makeMergeChanges =
  <T>({ equals }: Eq<T>) =>
  (snapshot: readonly T[]) =>
  (changes: readonly ChangeType<T>[]): readonly T[] =>
    changes.reduce((prev, next) => {
      switch (next.type) {
        case 'Insert':
        case 'Update': {
          const value = next.newRecord;
          return [...prev.filter((i) => !equals(i, value)), value];
        }
        case 'Delete': {
          const value = next.oldRecord;
          return prev.filter((i) => !equals(i, value));
        }
      }
    }, snapshot);

export const makeMapChanges =
  <T>(filter: (a: T) => boolean) =>
  (changes: readonly ChangeType<T>[]): ChangeType<T>[] =>
    changes.flatMap((i) => {
      switch (i.type) {
        case 'Insert': {
          const value = i.newRecord;
          if (filter(value)) {
            return i;
          }
          return [];
        }
        case 'Update': {
          const newOk = filter(i.newRecord);
          const oldOk = filter(i.oldRecord);

          if (newOk && oldOk) {
            return i;
          }
          if (!oldOk && newOk) {
            return {
              type: 'Insert' as const,
              newRecord: i.newRecord
            };
          }
          if (oldOk && !newOk) {
            return {
              type: 'Delete' as const,
              oldRecord: i.oldRecord
            };
          }
          return [];
        }
        case 'Delete': {
          if (filter(i.oldRecord)) {
            return i;
          }
          return [];
        }
      }
    });

export const mapEntity: <E, A, B>(
  f: (a: A) => TE.TaskEither<E, B>
) => (
  snapshotOrChanges: SnapshotOrChangesType<A>
) => TE.TaskEither<E, SnapshotOrChangesType<B>> = (f) => (val) => {
  if (val.type === 'snapshot') {
    return pipe(
      val.values,
      TE.traverseArray(f),
      TE.map((values) => ({
        type: val.type,
        values
      }))
    );
  }

  type E = ReturnType<typeof f> extends TE.TaskEither<infer U, unknown>
    ? U
    : never;

  type B = ReturnType<typeof f> extends TE.TaskEither<unknown, infer U>
    ? U
    : never;

  return pipe(
    val.changes,
    TE.traverseArray((val): TE.TaskEither<E, ChangeType<B>> => {
      if (val.type === 'Delete') {
        return pipe(
          f(val.oldRecord),
          TE.map((oldRecord) => ({
            type: val.type,
            oldRecord
          }))
        );
      } else if (val.type === 'Update') {
        return pipe(
          sequenceS(TE.ApplyPar)({
            oldRecord: f(val.oldRecord),
            newRecord: f(val.newRecord)
          }),
          TE.map((val2) => ({
            type: val.type,
            ...val2
          }))
        );
      }
      // Insert
      return pipe(
        f(val.newRecord),
        TE.map((newRecord) => ({
          type: val.type,
          newRecord
        }))
      );
    }),
    TE.map((changes) => ({
      type: 'changes',
      changes
    }))
  );
};
