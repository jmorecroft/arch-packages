import { CustomError } from 'ts-custom-error';
import { Status } from 'nice-grpc-common';

export class InternalError extends CustomError {
  readonly code: Status = Status.INTERNAL;
  constructor(message?: string) {
    super(message);
  }
}

export class BadArgsError extends CustomError {
  readonly code: Status = Status.INVALID_ARGUMENT;
  constructor(message?: string) {
    super(message);
  }
}

export class AbortError extends CustomError {
  readonly code: Status = Status.ABORTED;
  constructor(message?: string) {
    super(message);
  }
}

export class TimeoutError extends CustomError {
  readonly code: Status = Status.DEADLINE_EXCEEDED;
  constructor(message?: string) {
    super(message);
  }
}

export class UnknownError extends CustomError {
  readonly code: Status = Status.UNKNOWN;
  constructor(message?: string) {
    super(message);
  }
}
