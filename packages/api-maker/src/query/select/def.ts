import {
  schemable as SC,
  schemableLight as SCL,
  schemas as S
} from '@jmorecroft67/io-ts-types';
import { pipe } from 'fp-ts/lib/function';
import { unaryDef } from '../../core';

export type AscOrDesc<T> = {
  [K in keyof T]?: 'asc' | 'desc';
};

export type OrderByItem<T, K extends keyof T> = AscOrDesc<Pick<T, K>>;

export type OrderBy<T, O extends keyof T> = Array<OrderByItem<T, O>>;

export default <
  T extends Record<string, unknown>,
  O extends keyof T,
  C extends S.Spec<T> = S.Spec<T>
>({
  entity,
  orderBy,
  constraint
}: {
  entity: SCL.Schema<T>;
  orderBy: O[];
  constraint?: SC.Schema<C>;
}) => {
  const { spec } = SCL.interpreter(S.Schemable)(entity);

  const orderBySchema = SC.make((S) =>
    S.array(
      S.partial(
        orderBy.reduce(
          (prev, next) => ({ ...prev, [next]: S.literal('asc', 'desc') }),
          {}
        )
      )
    )
  ) as SC.Schema<OrderBy<T, O>>;

  const whereSchema = constraint
    ? SC.make((S) => pipe(spec(S), S.intersect(constraint(S))))
    : spec;

  const req = SC.make((S) =>
    pipe(
      S.struct({
        where: whereSchema(S)
      }),
      S.intersect(
        S.partial({
          take: S.number,
          skip: S.number,
          orderBy: orderBySchema(S)
        })
      )
    )
  );

  const res = SC.make((S) => S.array(entity(S)));

  return unaryDef(req, res);
};
