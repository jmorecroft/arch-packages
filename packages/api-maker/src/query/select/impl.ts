import * as TE from 'fp-ts/TaskEither';
import { ServerError, Status } from 'nice-grpc-common';
import { OrderBy } from './def';
import { schemas as S } from '@jmorecroft67/io-ts-types';

export interface Db<T, C, O extends keyof T> {
  findMany(options: {
    where: S.Spec<T> & C;
    take?: number;
    skip?: number;
    orderBy?: OrderBy<T, O>;
  }): Promise<T[]>;
}

export default <T, C, O extends keyof T>(db: Db<T, C, O>) =>
  (options: Parameters<Db<T, C, O>['findMany']>[0]) =>
    TE.tryCatch(
      () => db.findMany(options),
      (e) => new ServerError(Status.INTERNAL, (e as Error).message)
    );
