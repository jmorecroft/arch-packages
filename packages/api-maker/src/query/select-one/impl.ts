import { pipe } from 'fp-ts/lib/function';
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { ServerError, Status } from 'nice-grpc-common';

export interface Db<T, K> {
  findUnique(options: {
    where: K;
    rejectOnNotFound: boolean;
  }): Promise<T | null>;
}

export default <T, K>(db: Db<T, K>) =>
  (options: { where: K }) =>
    pipe(
      TE.tryCatch(
        () => db.findUnique({ ...options, rejectOnNotFound: false }),
        (e) => new ServerError(Status.INTERNAL, (e as Error).message)
      ),
      TE.chainEitherK((result) => {
        if (result === null) {
          return E.left(new ServerError(Status.NOT_FOUND, 'Not found.'));
        }
        return E.right(result);
      })
    );
