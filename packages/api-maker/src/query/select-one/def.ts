import {
  schemable as SC,
  schemableLight as SCL
} from '@jmorecroft67/io-ts-types';
import { unaryDef } from '../../core';

export default <T extends Record<string, unknown>, K>({
  entity,
  key
}: {
  entity: SCL.Schema<T>;
  key: SC.Schema<K>;
}) => {
  const req = SC.make((S) =>
    S.struct({
      where: key(S)
    })
  );
  return unaryDef(req, entity);
};
