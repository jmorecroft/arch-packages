import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { CallContext, ServerError, Status } from 'nice-grpc-common';
import { OrderBy } from './def';
import * as D from 'io-ts/Decoder';
import * as _ from 'lodash';
import {
  decoder as DE,
  schemas as S,
  schemable as SC,
  schemableLight as SCL,
  filter as F
} from '@jmorecroft67/io-ts-types';
import { Eq } from 'fp-ts/lib/Eq';
import { pipe } from 'fp-ts/lib/function';
import {
  ChangeType,
  makeChangeSchema,
  makeMapChanges,
  makeMergeChanges
} from '../../helpers';

export interface Db<T, C, K extends keyof T, O extends keyof T> {
  findMany(options: {
    where: { AND: ((S.Spec<T> & C) | Pick<T, K>)[] };
    take?: number;
    skip?: number;
    orderBy?: OrderBy<T, O>;
  }): Promise<T[]>;
}

export interface Cache {
  idAfterOrEqual(date: Date): string;
  get(id: string): TE.TaskEither<Error, { id: string; data: string }[]>;
  stream(
    id: string,
    signal: CallContext['signal']
  ): AsyncIterable<E.Either<Error, string[]>>;
}

export type MakeCache<T, K extends keyof T> = (key: Pick<T, K>) => Cache;

const makeFindMany =
  <T, C, K extends keyof T, O extends keyof T>(db: Db<T, C, K, O>) =>
  (options: Parameters<Db<T, C, K, O>['findMany']>[0]) =>
    TE.tryCatch(
      () => db.findMany(options),
      (e) => new ServerError(Status.INTERNAL, (e as Error).message)
    );

const makeDecodeChanges = <T>(schema: SC.Schema<ChangeType<T>>) => {
  const changeDecoder = SC.interpreter(DE.Schemable)(schema);

  const changeDecoderFromString = pipe(
    D.string,
    D.parse((a) =>
      E.tryCatch(
        () => JSON.parse(a),
        (e) => D.error(a, `JSON, but got ${(e as Error).message}`)
      )
    ),
    D.compose(changeDecoder)
  );

  return (changes: string[]): E.Either<ServerError, readonly ChangeType<T>[]> =>
    pipe(
      changes,
      E.traverseArray((change) => changeDecoderFromString.decode(change)),
      E.mapLeft((de) => new ServerError(Status.INTERNAL, D.draw(de)))
    );
};

export default <T, K extends keyof T, O extends keyof T>({
  entity,
  db,
  makeCache,
  updatedAt,
  equals
}: {
  entity: SCL.Schema<T>;
  orderBy: O[];
  key: SC.Schema<Pick<T, K>>;
  db: Db<T, S.Spec<T>, K, O>;
  makeCache: MakeCache<T, K>;
  updatedAt: (a: T) => Date;
  equals: Eq<T>;
}) => {
  const whereFilter = SCL.interpreter(F.Schemable)(entity)('failFast');

  const makeFilter = (where: S.Spec<T>) => (value: T) =>
    E.isRight(whereFilter(where, value)());

  const changeSchema = makeChangeSchema(entity);
  const decodeChanges = makeDecodeChanges(changeSchema);
  const mergeChanges = makeMergeChanges(equals);
  const findMany = makeFindMany(db);

  return async function* (
    request: {
      key: Pick<T, K>;
      where: S.Spec<T>;
      take?: number;
      skip?: number;
      orderBy?: OrderBy<T, O>;
    },
    context: CallContext
  ) {
    const now = new Date();
    const mapChanges = makeMapChanges(makeFilter(request.where));

    const cache = makeCache(request.key);

    const { key, where, ...rest } = request;

    const snapshot = await pipe(
      findMany({ where: { AND: [key, where] }, ...rest }),
      TE.chain((snapshot) => {
        const last = _.maxBy(snapshot, (a) => updatedAt(a).getTime());
        if (!last) {
          return TE.right({ snapshot: [] as readonly T[] } as {
            snapshot: readonly T[];
            start?: string;
          });
        }

        let start = cache.idAfterOrEqual(updatedAt(last));

        return pipe(
          cache.get(start),
          TE.mapLeft((e) => new ServerError(Status.INTERNAL, e.message)),
          TE.chain((cached) => {
            if (cached.length === 0) {
              return TE.right({ snapshot, start });
            }
            start = cached[cached.length - 1].id;

            return TE.fromEither(
              pipe(
                decodeChanges(cached.map((a) => a.data)),
                E.map(mapChanges),
                E.map(mergeChanges(snapshot)),
                E.map((merged) => ({ snapshot: merged, start }))
              )
            );
          })
        );
      })
    )();

    if (E.isLeft(snapshot)) {
      yield snapshot;
      return;
    }

    const { snapshot: values, start = cache.idAfterOrEqual(now) } =
      snapshot.right;

    yield E.right({ type: 'snapshot' as const, values });

    for await (const resp of cache.stream(start, context.signal)) {
      const changes = pipe(
        resp,
        E.mapLeft((e) => new ServerError(Status.INTERNAL, e.message)),
        E.chain(decodeChanges),
        E.map(mapChanges),
        E.map((changes) => ({ type: 'changes' as const, changes }))
      );

      if (E.isRight(changes)) {
        if (changes.right.changes.length > 0) {
          yield changes;
        }
      } else {
        yield changes;
        break;
      }
    }
  };
};
