import {
  schemable as SC,
  schemableLight as SCL,
  schemas as S
} from '@jmorecroft67/io-ts-types';
import { pipe } from 'fp-ts/lib/function';
import { streamDef } from '../../core';
import { makeSnapshotOrChangesSchema } from '../../helpers';

export type AscOrDesc<T> = {
  [K in keyof T]?: 'asc' | 'desc';
};

export type OrderByItem<T, K extends keyof T> = AscOrDesc<Pick<T, K>>;

export type OrderBy<T, O extends keyof T> = Array<OrderByItem<T, O>>;

export default <
  T extends Record<string, unknown>,
  K extends keyof T,
  O extends keyof T,
  C extends S.Spec<T> = S.Spec<T>
>({
  entity,
  orderBy,
  key,
  constraint
}: {
  entity: SCL.Schema<T>;
  orderBy: O[];
  key: SC.Schema<Pick<T, K>>;
  constraint?: SC.Schema<C>;
}) => {
  const { spec } = SCL.interpreter(S.Schemable)(entity);

  const orderBySchema = SC.make((S) =>
    S.array(
      S.partial(
        orderBy.reduce(
          (prev, next) => ({ ...prev, [next]: S.literal('asc', 'desc') }),
          {}
        )
      )
    )
  ) as SC.Schema<OrderBy<T, O>>;

  const reqSchema1 = SC.make((S) =>
    S.struct({
      key: key(S),
      where: constraint ? pipe(spec(S), S.intersect(constraint(S))) : spec(S)
    })
  ) as SC.Schema<{
    key: Pick<T, K>;
    where: S.Spec<T> & C;
  }>;

  const req = SC.make((S) =>
    pipe(
      reqSchema1(S),
      S.intersect(
        S.partial({
          take: S.number,
          skip: S.number,
          orderBy: orderBySchema(S)
        })
      )
    )
  );

  const res = makeSnapshotOrChangesSchema(entity);

  return {
    ...streamDef(req, res),
    // We return the following also as a convenience for building the impl.
    entity,
    key,
    orderBy
  };
};
