import { ApiDef } from './make-service-def';
import * as pb from 'protobufjs';
import { schemable as S } from '@jmorecroft67/io-ts-types';
import { protobufCodec as SPC } from '@jmorecroft67/io-ts-types';

export default <A extends ApiDef>({ methods, name }: A) =>
  (root?: pb.Root): pb.Root => {
    const parent = root === undefined ? new pb.Root() : root;

    const service = new pb.Service(name);

    parent.add(service);

    const context = SPC.makeContext();

    Object.entries(methods).forEach(([key, { req, res, type }]) => {
      const reqCodec = S.interpreter(SPC.Schemable)(req)(context);
      const resCodec = S.interpreter(SPC.Schemable)(res)(context);

      const requestType = reqCodec.addPbType(service);
      const responseType = resCodec.addPbType(service);

      const method = new pb.Method(
        key,
        'rpc',
        requestType,
        responseType,
        false,
        type === 'serverStreaming'
      );

      service.add(method);
    });

    return parent;
  };
