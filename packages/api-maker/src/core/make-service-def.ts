/* eslint-disable @typescript-eslint/no-explicit-any */
import { schemable as S } from '@jmorecroft67/io-ts-types';
import { protobufCodec as SPC } from '@jmorecroft67/io-ts-types';
import { flow, pipe } from 'fp-ts/lib/function';
import * as E from 'fp-ts/Either';
import * as D from 'io-ts/Decoder';
import { BadArgsError } from '../errors';

export type MethodDefBase<Req, Res> = {
  req: S.Schema<Req>;
  res: S.Schema<Res>;
};

export type UnaryDef<Req, Res> = MethodDefBase<Req, Res> & {
  type: 'unary';
};

export type StreamDef<Req, Res> = MethodDefBase<Req, Res> & {
  type: 'serverStreaming';
};

export type MethodDef<Req, Res> = UnaryDef<Req, Res> | StreamDef<Req, Res>;

export type ApiDef = {
  name: string;
  methods: { [method: string]: MethodDef<unknown, unknown> };
};

export type ProtobufJsWriter = {
  finish(): Uint8Array;
};

export type TsProtoMessageType<Message> = {
  encode(message: Message): ProtobufJsWriter;
  decode(input: Uint8Array): Message;
};

export type TsProtoMethodDef<Request, Response> = {
  name: string;
  requestType: TsProtoMessageType<Request>;
  requestStream: boolean;
  responseType: TsProtoMessageType<Response>;
  responseStream: boolean;
  // eslint-disable-next-line @typescript-eslint/ban-types
  options: {};
};

export type TsProtoDef<A extends ApiDef> = {
  [K in keyof A['methods']]: TsProtoMethodDef<
    ReqOf<A['methods'][K]>,
    ResOf<A['methods'][K]>
  > & {
    requestStream: false;
    responseStream: A['methods'][K] extends { type: 'serverStreaming' }
      ? true
      : false;
  };
};

export type ServiceDef<A extends ApiDef> = {
  name: string;
  fullName: string;
  methods: TsProtoDef<A>;
};

export type ReqOf<A> = A extends MethodDefBase<infer T, unknown> ? T : never;
export type ResOf<A> = A extends MethodDefBase<unknown, infer T> ? T : never;

export const unaryDef = <Req, Res>(
  req: S.Schema<Req>,
  res: S.Schema<Res>
): UnaryDef<Req, Res> => ({
  req,
  res,
  type: 'unary'
});

export const streamDef = <Req, Res>(
  req: S.Schema<Req>,
  res: S.Schema<Res>
): StreamDef<Req, Res> => ({
  req,
  res,
  type: 'serverStreaming'
});

export default <A extends ApiDef>({ methods, name }: A): ServiceDef<A> => {
  const context = SPC.makeContext();

  return {
    fullName: name,
    name,
    methods: Object.entries(methods).reduce(
      (obj, [key, { req, res, type }]) => {
        const reqCodec = S.interpreter(SPC.Schemable)(req)(context);
        const resCodec = S.interpreter(SPC.Schemable)(res)(context);

        const makeType = (codec: SPC.ProtobufCodec<any>) => ({
          encode(value: any) {
            return codec.encode(value);
          },
          decode(input: Uint8Array) {
            return pipe(
              codec.decode(input),
              flow(
                E.fold(
                  (de) => {
                    throw new BadArgsError(D.draw(de));
                  },
                  (a) => a
                )
              )
            );
          }
        });

        return {
          ...obj,
          [key]: {
            name: key,
            requestStream: false,
            requestType: makeType(reqCodec),
            responseStream: type === 'serverStreaming',
            responseType: makeType(resCodec),
            options: {}
          }
        };
      },
      {} as any
    )
  };
};
