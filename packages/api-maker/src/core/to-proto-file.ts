import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import * as pb from 'protobufjs';
import { exec } from 'child_process';

export default (root: pb.Root): TE.TaskEither<Error, string> => {
  return () =>
    new Promise((resolve) => {
      const pbts = exec('$(npm bin)/pbjs -t proto2 -', (error, stdout) => {
        if (error) {
          resolve(E.left(error));
          return;
        }
        resolve(E.right(stdout));
      });
      if (pbts.stdin) {
        pbts.stdin.write(JSON.stringify(root.toJSON().nested));
        pbts.stdin.end();
      }
    });
};
