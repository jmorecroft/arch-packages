/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { CallContext, ServerError, Status } from 'nice-grpc-common';
import makeServiceDef, {
  ApiDef,
  MethodDef,
  ReqOf,
  ResOf,
  ServiceDef
} from './make-service-def';
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { flow } from 'fp-ts/lib/function';

export type ApiMethodImpl<
  A extends MethodDef<any, any>,
  ContextExt
> = A extends {
  type: 'unary';
}
  ? ApiUnaryMethodImpl<A, ContextExt>
  : A extends { type: 'serverStreaming' }
  ? ApiStreamMethodImpl<A, ContextExt>
  : never;

export type ApiUnaryMethodImpl<A extends MethodDef<any, any>, ContextExt> = (
  request: ReqOf<A>,
  context: CallContext & ContextExt
) => TE.TaskEither<Error & { code?: Status }, ResOf<A>>;

export type ApiStreamMethodImpl<A extends MethodDef<any, any>, ContextExt> = (
  request: ReqOf<A>,
  context: CallContext & ContextExt
) => AsyncIterable<E.Either<Error & { code?: Status }, ResOf<A>>>;

export type ApiImpl<A extends ApiDef, ContextExt> = {
  [K in keyof A['methods']]: ApiMethodImpl<A['methods'][K], ContextExt>;
};

export type RawUnaryMethodImpl<A extends MethodDef<any, any>, ContextExt> = (
  request: ReqOf<A>,
  context: CallContext & ContextExt
) => Promise<ResOf<A>>;

export type RawStreamMethodImpl<A extends MethodDef<any, any>, ContextExt> = (
  request: ReqOf<A>,
  context: CallContext & ContextExt
) => AsyncIterable<ResOf<A>>;

export type RawMethodImpl<
  A extends MethodDef<any, any>,
  ContextExt
> = A extends {
  type: 'unary';
}
  ? RawUnaryMethodImpl<A, ContextExt>
  : A extends { type: 'serverStreaming' }
  ? RawStreamMethodImpl<A, ContextExt>
  : never;

export type RawImpl<A extends ApiDef, ContextExt> = {
  [K in keyof A['methods']]: RawMethodImpl<A['methods'][K], ContextExt>;
};

export type Server<A extends ApiDef, ContextExt> = {
  add: (def: ServiceDef<A>, impl: RawImpl<A, ContextExt>) => void;
};

const wrapUnaryFn =
  (method: ApiUnaryMethodImpl<any, any>) => (request: any, context: any) =>
    method(request, context)().then(
      flow(
        E.fold(
          (error) => {
            if (error.code !== undefined && !(error instanceof ServerError)) {
              throw new ServerError(error.code, error.message);
            }
            throw error;
          },
          (a) => a
        )
      )
    );

const wrapStreamingFn = (method: ApiStreamMethodImpl<any, any>) =>
  async function* (request: any, context: any) {
    for await (const res of method(request, context)) {
      if (E.isLeft(res)) {
        const error = res.left;
        if (error.code !== undefined && !(error instanceof ServerError)) {
          throw new ServerError(error.code, error.message);
        }
        throw error;
      }
      yield res.right;
    }
  };

export default <A extends ApiDef>(api: A) =>
  <ContextExt = {}>({
    impl,
    server
  }: {
    impl: ApiImpl<A, ContextExt>;
    server: Server<A, ContextExt>;
  }) => {
    const def = makeServiceDef(api);

    const rawImpl: RawImpl<typeof api, any> = Object.entries(impl).reduce(
      (p, [key, method]) => {
        const wrapped = def.methods[key].responseStream
          ? wrapStreamingFn(method)
          : wrapUnaryFn(method);

        return {
          ...p,
          [key]: wrapped
        };
      },
      {} as any
    );

    return server.add(def, rawImpl);
  };
