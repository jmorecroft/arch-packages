/* eslint-disable @typescript-eslint/no-explicit-any */
import { CallOptions, ClientError, Status } from 'nice-grpc-common';
import makeServiceDef, {
  ApiDef,
  MethodDef,
  ReqOf,
  ResOf,
  ServiceDef
} from './make-service-def';
import * as E from 'fp-ts/Either';
import * as TE from 'fp-ts/TaskEither';
import { isAbortError } from 'abort-controller-x';
import { UnknownError, AbortError, TimeoutError } from '../errors';

export type ApiMethodClient<A extends MethodDef<any, any>> = A extends {
  type: 'unary';
}
  ? ApiUnaryMethodClient<A>
  : A extends { type: 'serverStreaming' }
  ? ApiStreamMethodClient<A>
  : never;

export type RawMethodClient<A extends MethodDef<any, any>> = A extends {
  type: 'unary';
}
  ? RawUnaryMethodClient<A>
  : A extends { type: 'serverStreaming' }
  ? RawStreamMethodClient<A>
  : never;

export type RawUnaryMethodClient<A extends MethodDef<any, any>> = (
  request: ReqOf<A>,
  options?: CallOptions
) => Promise<ResOf<A>>;

export type RawStreamMethodClient<A extends MethodDef<any, any>> = (
  request: ReqOf<A>,
  options?: CallOptions
) => AsyncIterable<ResOf<A>>;

export type ApiUnaryMethodClient<A extends MethodDef<any, any>> = (
  request: ReqOf<A>,
  options?: CallOptions
) => TE.TaskEither<Error & { code: Status }, ResOf<A>>;

export type ApiStreamMethodClient<A extends MethodDef<any, any>> = (
  request: ReqOf<A>,
  options?: CallOptions
) => AsyncIterable<E.Either<Error & { code: Status }, ResOf<A>>>;

export type ApiClient<A extends ApiDef> = {
  [K in keyof A['methods']]: ApiMethodClient<A['methods'][K]>;
};

export type RawClient<A extends ApiDef> = {
  [K in keyof A['methods']]: RawMethodClient<A['methods'][K]>;
};

const wrapUnaryFn =
  (method: RawUnaryMethodClient<any>) =>
  (request: any, options: CallOptions) => {
    return TE.tryCatch(
      () => method(request, options),
      (e) => {
        if (e instanceof ClientError) {
          return e;
        }
        if (isAbortError(e)) {
          return new AbortError((e as Error).message);
        }
        return new UnknownError((e as Error).message);
      }
    );
  };

const wrapStreamingFn = (method: RawStreamMethodClient<any>) =>
  async function* (request: any, options: CallOptions) {
    try {
      for await (const response of method(request, options)) {
        yield E.right(response);
      }
    } catch (e) {
      if (e instanceof ClientError) {
        if (
          e.code === Status.UNKNOWN &&
          e.details === 'Response closed without grpc-status (Headers only)'
        ) {
          // This happens when the Envoy proxy disconnects due to the idle timeout
          // being exceeded.
          yield E.left(
            new TimeoutError(
              `${e.path}: ${e.details}: assuming Envoy/proxy idle timeout.`
            )
          );
          return;
        }

        yield E.left(e);
        return;
      }
      if (isAbortError(e)) {
        yield E.left(new AbortError((e as Error).message));
        return;
      }
      yield E.left(new UnknownError((e as Error).message));
    }
  };

export type RawClientMaker<A extends ApiDef> = (
  def: ServiceDef<A>
) => RawClient<A>;

export default <A extends ApiDef>(api: A) =>
  (makeRawClient: RawClientMaker<A>): ApiClient<A> => {
    const def = makeServiceDef(api);

    const rawClient = makeRawClient(def);

    const client: ApiClient<typeof api> = Object.entries(rawClient).reduce(
      (p, [key, method]) => {
        const wrapped = def.methods[key].responseStream
          ? wrapStreamingFn(method)
          : wrapUnaryFn(method);

        return {
          ...p,
          [key]: wrapped
        };
      },
      {} as any
    );

    return client;
  };
