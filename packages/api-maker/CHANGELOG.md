# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0...@jmorecroft67/api-maker@1.0.1) (2023-01-05)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [1.0.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.10...@jmorecroft67/api-maker@1.0.0) (2023-01-04)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [1.0.0-staging.10](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.9...@jmorecroft67/api-maker@1.0.0-staging.10) (2022-12-13)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** improve proto file output, use proto2 version ([2f7c56e](https://gitlab.com/jmorecroft/arch-packages/commit/2f7c56e57ea082d640080e4b1b3910aa5c5f1c34))





# [1.0.0-staging.9](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.8...@jmorecroft67/api-maker@1.0.0-staging.9) (2022-12-03)


### Bug Fixes

* **@jmorecroft67/api-maker:** fix grpc serialization so not using delimited payloads ([39bdb4e](https://gitlab.com/jmorecroft/arch-packages/commit/39bdb4ea8345f339c540b83c0bae86f8bd852671))





# [1.0.0-staging.8](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.7...@jmorecroft67/api-maker@1.0.0-staging.8) (2022-12-03)


### Bug Fixes

* **@jmorecroft67/io-ts-types:** fix proto gen problem due to multiple instances of protobufjs ([4cb7387](https://gitlab.com/jmorecroft/arch-packages/commit/4cb73873558102d3e064ff1406033bca4791d3f1))





# [1.0.0-staging.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.6...@jmorecroft67/api-maker@1.0.0-staging.7) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [1.0.0-staging.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.5...@jmorecroft67/api-maker@1.0.0-staging.6) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [1.0.0-staging.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.4...@jmorecroft67/api-maker@1.0.0-staging.5) (2022-12-02)


### Features

* **@jmorecroft67/api-maker:** embed useful schemas in select stream definition ([3aa9e66](https://gitlab.com/jmorecroft/arch-packages/commit/3aa9e66f16e3cb344fb8ca08dba2ffbe9869bde4))





# [1.0.0-staging.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.3...@jmorecroft67/api-maker@1.0.0-staging.4) (2022-12-01)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [1.0.0-staging.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.2...@jmorecroft67/api-maker@1.0.0-staging.3) (2022-11-29)


### Bug Fixes

* **@jmorecroft67/io-ts-types, @jmorecroft67/api-maker:** fix location of message def in proto file ([634c640](https://gitlab.com/jmorecroft/arch-packages/commit/634c640aa372069db68791a532e1bb497cbaf203))





# [1.0.0-staging.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.1...@jmorecroft67/api-maker@1.0.0-staging.2) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [1.0.0-staging.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@1.0.0-staging.0...@jmorecroft67/api-maker@1.0.0-staging.1) (2022-11-28)


### Features

* **@jmorecroft67/auth:** auth major refactor ([fe920fb](https://gitlab.com/jmorecroft/arch-packages/commit/fe920fb5546f305844376a17309689e2482e2b0a))


### BREAKING CHANGES

* **@jmorecroft67/auth:** @jmorecroft67/auth refactored from an express app to an unauthenticated GRPC service.





# [1.0.0-staging.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.14.5-staging.0...@jmorecroft67/api-maker@1.0.0-staging.0) (2022-11-24)


### Bug Fixes

* **@jmorecroft67/api-maker:** removed no longer used proto file, ts-proto code gen scripts ([e54189a](https://gitlab.com/jmorecroft/arch-packages/commit/e54189a0429ea2984c55354ce8b5b032359bc9a7))
* **@jmorecroft67/api-maker:** removed unused test file ([8c22019](https://gitlab.com/jmorecroft/arch-packages/commit/8c22019adc57c70155573dd5ac287158aa6dd1a3))


### BREAKING CHANGES

* **@jmorecroft67/api-maker:** Forced bump due to major refactor and changes to simplify usage and remove proto file/ ts-proto dependency





## [0.14.5-staging.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.14.4...@jmorecroft67/api-maker@0.14.5-staging.0) (2022-11-24)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.14.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.14.3...@jmorecroft67/api-maker@0.14.4) (2022-11-03)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.14.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.14.2...@jmorecroft67/api-maker@0.14.3) (2022-10-25)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.14.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.14.1...@jmorecroft67/api-maker@0.14.2) (2022-10-24)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.14.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.14.0...@jmorecroft67/api-maker@0.14.1) (2022-08-22)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [0.14.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.13.3...@jmorecroft67/api-maker@0.14.0) (2022-08-13)


### Features

* **@jmorecroft67/api-maker:** Updated nice-grpc lib and tweaked AbortController usage as required. ([178e526](https://gitlab.com/jmorecroft/arch-packages/commit/178e526a0c2752913388c4918aecda25317d3aec))





## [0.13.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.13.2...@jmorecroft67/api-maker@0.13.3) (2022-08-03)


### Bug Fixes

* **@jmorecroft67/api-maker:** Exposed useful RPC types. ([9cc32b1](https://gitlab.com/jmorecroft/arch-packages/commit/9cc32b13bb529ce8f5a117fed8edc23285574ef1))





## [0.13.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.13.1...@jmorecroft67/api-maker@0.13.2) (2022-08-03)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.13.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.13.0...@jmorecroft67/api-maker@0.13.1) (2022-07-27)


### Bug Fixes

* **@jmorecroft67/api-maker:** Add missing import. ([b7b721f](https://gitlab.com/jmorecroft/arch-packages/commit/b7b721fbe57133105bc6a5c28fe08a28a5f9475c))





# [0.13.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.12.1...@jmorecroft67/api-maker@0.13.0) (2022-07-26)


### Features

* **@jmorecroft67/api-maker:** Added mapEntity helper function. ([156241c](https://gitlab.com/jmorecroft/arch-packages/commit/156241c4972662df83b22cba6be8c1ee770c3465))





## [0.12.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.12.0...@jmorecroft67/api-maker@0.12.1) (2022-07-26)


### Bug Fixes

* **@jmorecroft67/api-maker:** Fix readonly types. ([79c4fd7](https://gitlab.com/jmorecroft/arch-packages/commit/79c4fd700cdbfce5d29bd098ef4685ad48677677))





# [0.12.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.11.0...@jmorecroft67/api-maker@0.12.0) (2022-07-26)


### Features

* **@jmorecroft67/api-maker:** Expose merge function. ([2b164f6](https://gitlab.com/jmorecroft/arch-packages/commit/2b164f62833834efe429eb49e585e2d28df51b82))





# [0.11.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.10.0...@jmorecroft67/api-maker@0.11.0) (2022-07-26)


### Features

* **@jmorecroft67/api-maker:** More type fiddling. ([977d7f8](https://gitlab.com/jmorecroft/arch-packages/commit/977d7f8c92faee2e2d7c9c8b116db7cf3b7d66ee))





# [0.10.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.9.0...@jmorecroft67/api-maker@0.10.0) (2022-07-25)


### Features

* **@jmorecroft67/api-maker:** Added helper types. ([80e2e69](https://gitlab.com/jmorecroft/arch-packages/commit/80e2e69f3e2c841fea51d58d11ff5339105c2e49))





# [0.9.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.8.0...@jmorecroft67/api-maker@0.9.0) (2022-07-25)


### Features

* **@jmorecroft67/api-maker:** Moved convenience codec functions into helpers. ([87d3b71](https://gitlab.com/jmorecroft/arch-packages/commit/87d3b717bd8677d3415564cb39b167aa044e2af1))





# [0.8.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.7.0...@jmorecroft67/api-maker@0.8.0) (2022-07-22)


### Features

* **@jmorecroft67/api-maker:** More simplification, extra convenience maker method. ([db1f160](https://gitlab.com/jmorecroft/arch-packages/commit/db1f160521dc62f160908b15499438a8c83496dc))





# [0.7.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.6.0...@jmorecroft67/api-maker@0.7.0) (2022-07-22)


### Features

* **@jmorecroft67/api-maker:** Simplified rpc makers wrt wrapper types. ([2084f25](https://gitlab.com/jmorecroft/arch-packages/commit/2084f25182eeb458ea05a8bd24e2627d6bf16515))





# [0.6.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.5.1...@jmorecroft67/api-maker@0.6.0) (2022-07-22)


### Features

* **@jmorecroft67/api-maker:** Added support for additional server context. ([f04b456](https://gitlab.com/jmorecroft/arch-packages/commit/f04b456f18c228e22df5995e1723eda941c0f9f1))





## [0.5.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.5.0...@jmorecroft67/api-maker@0.5.1) (2022-07-21)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [0.5.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.4.2...@jmorecroft67/api-maker@0.5.0) (2022-07-21)


### Features

* **@jmorecroft67/io-ts-types:** Added support for recursive AND, OR, NOT conditions in filters. ([80f6ca1](https://gitlab.com/jmorecroft/arch-packages/commit/80f6ca19e2bc9e6d2cd6792bab2239e13803eb0f))





## [0.4.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.4.1...@jmorecroft67/api-maker@0.4.2) (2022-07-12)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.4.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.4.0...@jmorecroft67/api-maker@0.4.1) (2022-07-12)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [0.4.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.3.4...@jmorecroft67/api-maker@0.4.0) (2022-07-12)


### Features

* Initial ALPHA support for OpenTelemetry in some packages. ([628006d](https://gitlab.com/jmorecroft/arch-packages/commit/628006daf4ce15e0337f53b9247ec3d248fb63dd))





## [0.3.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.3.3...@jmorecroft67/api-maker@0.3.4) (2022-07-11)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.3.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.3.2...@jmorecroft67/api-maker@0.3.3) (2022-07-11)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.3.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.3.1...@jmorecroft67/api-maker@0.3.2) (2022-07-11)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.3.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.3.0...@jmorecroft67/api-maker@0.3.1) (2022-07-11)

**Note:** Version bump only for package @jmorecroft67/api-maker





# [0.3.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.2.0...@jmorecroft67/api-maker@0.3.0) (2022-07-10)


### Features

* **@jmorecroft67/api-maker:** Added support for take, skip, orderBy to makeStream. ([4641efd](https://gitlab.com/jmorecroft/arch-packages/commit/4641efdbcfd3b8ae601af032899ae89b52dcd900))





# [0.2.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.9...@jmorecroft67/api-maker@0.2.0) (2022-07-10)


### Features

* **@jmorecroft67/api-maker:** Better handling of Envoy/proxy idle timeout behaviour in streaming client. ([2b1e3fc](https://gitlab.com/jmorecroft/arch-packages/commit/2b1e3fc471f74c1bbee0232d3a9463dc8a97e04c))





## [0.1.9](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.8...@jmorecroft67/api-maker@0.1.9) (2022-07-05)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.1.8](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.7...@jmorecroft67/api-maker@0.1.8) (2022-07-02)


### Bug Fixes

* **@jmorecroft67/api-maker:** Remove unnecessary dependencies. ([207f644](https://gitlab.com/jmorecroft/arch-packages/commit/207f6444ef1595ad65ff7c2e91ffd3a9c42c1079))





## [0.1.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.6...@jmorecroft67/api-maker@0.1.7) (2022-07-02)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.1.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.5...@jmorecroft67/api-maker@0.1.6) (2022-07-02)


### Bug Fixes

* **@jmorecroft67/api-maker:** Remove dependency on grpc-js. ([75bcbef](https://gitlab.com/jmorecroft/arch-packages/commit/75bcbef79f130563f4d1aee8ca1337bf181da94a))
* Fix broken tests. ([2d4061a](https://gitlab.com/jmorecroft/arch-packages/commit/2d4061a7035bb3573e2a4634f9a6c26598390e4c))





## [0.1.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.4...@jmorecroft67/api-maker@0.1.5) (2022-07-01)


### Bug Fixes

* **@jmorecroft67/api-maker:** Improved types. ([85f0fb4](https://gitlab.com/jmorecroft/arch-packages/commit/85f0fb4e92b2d9d09298da9c40f36ab2ad57ca60))





## [0.1.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.3...@jmorecroft67/api-maker@0.1.4) (2022-07-01)


### Bug Fixes

* **@jmorecroft67/api-maker:** Minor fixes. ([34b5616](https://gitlab.com/jmorecroft/arch-packages/commit/34b56169795f82d4942d107ad561436259a23564))





## [0.1.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.2...@jmorecroft67/api-maker@0.1.3) (2022-07-01)

**Note:** Version bump only for package @jmorecroft67/api-maker





## [0.1.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.1...@jmorecroft67/api-maker@0.1.2) (2022-06-30)


### Bug Fixes

* Add cache types to exports. ([e8d5438](https://gitlab.com/jmorecroft/arch-packages/commit/e8d54385020b4fe8a4de51b16da3227206dbe4eb))





## [0.1.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/api-maker@0.1.0...@jmorecroft67/api-maker@0.1.1) (2022-06-30)

**Note:** Version bump only for package @jmorecroft67/api-maker





# 0.1.0 (2022-06-28)


### Bug Fixes

* Fixed package.json publish config. ([a19237b](https://gitlab.com/jmorecroft/arch-packages/commit/a19237b42fc9a1d88d3bfec6a54c84c3b979d46b))
* Temp disable lerna task. ([9c31f36](https://gitlab.com/jmorecroft/arch-packages/commit/9c31f3687f8bfc8a56f105b4883aac90fbb34071))


### Features

* io-ts-types, api-maker libs ([02a76db](https://gitlab.com/jmorecroft/arch-packages/commit/02a76dbf3931848c538475f588ec20a3507cd539))
