/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  schemableLight as SCL,
  schemable as SC
} from '@jmorecroft67/io-ts-types';
import * as E from 'fp-ts/Either';
import { PrismaClient } from '@prisma/client';
import { pipe } from 'fp-ts/function';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server,
  Status
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import {
  insertDef,
  insertImpl,
  addService,
  makeClient,
  ApiClient
} from '../../src';

describe(__filename, () => {
  let server: Server;

  beforeEach(() => {
    server = createServer();
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  const entity = SCL.make((S) =>
    S.struct({
      id: S.number,
      name: S.nullable(S.string),
      updatedAt: S.date,
      createdAt: S.date
    })
  );

  const data = SC.make((S) =>
    pipe(
      S.struct({
        name: S.nullable(S.string)
      }),
      S.intersect(
        S.partial({
          id: S.number
        })
      )
    )
  );

  const db = new PrismaClient().test;

  afterEach(() => db.deleteMany({}));

  const api = {
    name: 'TestInsertApi',
    methods: {
      insert: insertDef({ entity, data })
    }
  };

  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    addService(api)({
      impl: {
        insert: insertImpl(db)
      },
      server
    });

    const channel = await listen();

    client = makeClient(api)((def) => createClient(def, channel));
  });

  it('should send between client and server, no errors', async () => {
    const mrT = {
      name: 'Mr T'
    };

    const result = await client.insert({
      data: mrT
    })();

    expect(result).toEqual(E.right(expect.objectContaining(mrT)));
  });

  it('should fail on already exists', async () => {
    const mrT = await db.create({
      data: {
        name: 'Mr T'
      }
    });

    const result = await client.insert({
      data: {
        id: mrT.id,
        name: 'Sly Stallone'
      }
    })();

    expect(result).toEqual(
      E.left(expect.objectContaining({ code: Status.ALREADY_EXISTS }))
    );
  });

  it('should fail on bad (forced) input', async () => {
    const result = await client.insert({
      data: {
        id: 123
      } as any
    })();

    expect(result).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INTERNAL,
          message: `/TestInsertApi/insert INTERNAL: required property "data"
└─ required property "name"
   └─ cannot decode undefined, should be defined`
        })
      )
    );
  });
});
