import 'jest-extended';
import {
  addService,
  makeClient,
  addProtoService,
  streamDef,
  toProtoFile,
  unaryDef,
  AbortError
} from '../../src';
import { schemable as SC } from '@jmorecroft67/io-ts-types';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server,
  ServerError,
  Status
} from 'nice-grpc';
import * as TE from 'fp-ts/TaskEither';
import * as E from 'fp-ts/Either';
import { ServerCredentials } from '@grpc/grpc-js';
import { pipe } from 'fp-ts/lib/function';

describe(__filename, () => {
  let server: Server;

  beforeEach(() => {
    server = createServer();
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  it('should handle unary call', async () => {
    const api = {
      name: 'TestApi',
      methods: {
        echo: unaryDef(
          SC.make((S) =>
            S.struct({
              ping: S.date
            })
          ),
          SC.make((S) =>
            S.struct({
              pong: S.date
            })
          )
        )
      }
    };

    addService(api)({
      impl: {
        echo: (request) => {
          return TE.of({
            pong: request.ping
          });
        }
      },
      server
    });

    const channel = await listen();

    const client = makeClient(api)((def) => createClient(def, channel));

    const now = new Date();

    const result = await client.echo({ ping: now })();

    expect(result).toEqual(E.right({ pong: now }));
  });

  it('should handle unary call error', async () => {
    const api = {
      name: 'TestApi',
      methods: {
        echo: unaryDef(
          SC.make((S) =>
            S.struct({
              ping: S.date
            })
          ),
          SC.make((S) =>
            S.struct({
              pong: S.date
            })
          )
        )
      }
    };

    addService(api)({
      impl: {
        echo: () => {
          const fail = TE.left(new ServerError(Status.DATA_LOSS, 'no data'));
          return fail;
        }
      },
      server
    });

    const channel = await listen();

    const client = makeClient(api)((def) => createClient(def, channel));

    const now = new Date();

    const result = await client.echo({ ping: now })();

    expect(result).toEqual(
      E.left(expect.objectContaining({ code: Status.DATA_LOSS }))
    );
  });

  it('should handle streaming api', async () => {
    const api = {
      name: 'TestApi',
      methods: {
        echo: streamDef(
          SC.make((S) =>
            S.struct({
              ping: S.date
            })
          ),
          SC.make((S) =>
            S.struct({
              pong: S.date
            })
          )
        )
      }
    };

    addService(api)({
      impl: {
        echo: async function* (request) {
          yield E.right({
            pong: request.ping
          });
          yield E.right({
            pong: request.ping
          });
        }
      },
      server
    });

    const channel = await listen();

    const client = makeClient(api)((def) => createClient(def, channel));

    const now = new Date();

    const result: unknown[] = [];

    for await (const res of client.echo({ ping: now })) {
      result.push(res);
    }

    expect(result).toEqual([E.right({ pong: now }), E.right({ pong: now })]);
  });

  it('should handle streaming api error', async () => {
    const api = {
      name: 'TestApi',
      methods: {
        echo: streamDef(
          SC.make((S) =>
            S.struct({
              ping: S.date
            })
          ),
          SC.make((S) =>
            S.struct({
              pong: S.date
            })
          )
        )
      }
    };

    addService(api)({
      impl: {
        echo: async function* (request) {
          yield E.right({
            pong: request.ping
          });
          yield E.left(new ServerError(Status.DATA_LOSS, 'no data'));
        }
      },
      server
    });

    const channel = await listen();

    const client = makeClient(api)((def) => createClient(def, channel));

    const now = new Date();

    const result: unknown[] = [];

    for await (const res of client.echo({ ping: now })) {
      result.push(res);
    }

    expect(result).toEqual([
      E.right({ pong: now }),
      E.left(expect.objectContaining({ code: Status.DATA_LOSS }))
    ]);
  });

  it('should handle streaming client bailing early', async () => {
    const api = {
      name: 'TestApi',
      methods: {
        echo: streamDef(
          SC.make((S) =>
            S.struct({
              ping: S.date
            })
          ),
          SC.make((S) =>
            S.struct({
              pong: S.date
            })
          )
        )
      }
    };

    const done = new Promise((resolve, reject) => {
      addService(api)({
        impl: {
          echo: async function* (request) {
            try {
              for (;;) {
                yield E.right({
                  pong: request.ping
                });
              }
            } catch (e) {
              reject(e);
            } finally {
              resolve(undefined);
            }
          }
        },
        server
      });
    });

    const channel = await listen();

    const client = makeClient(api)((def) => createClient(def, channel));

    const now = new Date();

    const result: unknown[] = [];

    for await (const res of client.echo({ ping: now })) {
      result.push(res);
      if (result.length === 2) {
        break;
      }
    }

    const error = await done;

    expect(result).toEqual([E.right({ pong: now }), E.right({ pong: now })]);
    expect(error).toBe(undefined);
  });

  it('should handle streaming client aborting', async () => {
    const api = {
      name: 'TestApi',
      methods: {
        echo: streamDef(
          SC.make((S) =>
            S.struct({
              ping: S.date
            })
          ),
          SC.make((S) =>
            S.struct({
              pong: S.date
            })
          )
        )
      }
    };

    const done = new Promise((resolve, reject) => {
      addService(api)({
        impl: {
          echo: async function* (request) {
            try {
              for (;;) {
                yield E.right({
                  pong: request.ping
                });
              }
            } catch (e) {
              reject(e);
            } finally {
              resolve(undefined);
            }
          }
        },
        server
      });
    });

    const channel = await listen();

    const client = makeClient(api)((def) => createClient(def, channel));

    const now = new Date();

    const result: unknown[] = [];

    const controller = new AbortController();

    for await (const res of client.echo(
      { ping: now },
      { signal: controller.signal }
    )) {
      result.push(res);
      if (result.length === 1) {
        controller.abort();
      }
    }

    const error = await done;

    expect(result[0]).toEqual(E.right({ pong: now }));
    expect(result[result.length - 1]).toEqual(
      E.left(new AbortError('The operation has been aborted'))
    );
    expect(error).toBe(undefined);
  });

  it('should handle proto define', async () => {
    const api = {
      name: 'GreeterService',
      methods: {
        sayHi: unaryDef(
          SC.make((S) =>
            S.struct({
              hi: S.string
            })
          ),
          SC.make((S) =>
            S.partial({
              bye: S.string
            })
          )
        ),
        chat: streamDef(
          SC.make((S) =>
            S.struct({
              question: S.string
            })
          ),
          SC.make((S) =>
            S.struct({
              answer: S.nullable(S.string)
            })
          )
        )
      }
    };

    const protoFile = await pipe(addProtoService(api)(), toProtoFile)();

    expect(protoFile).toEqual(
      E.right(`syntax = "proto2";
service GreeterService {
    rpc sayHi (StructRef1) returns (PartialRef2);
    rpc chat (StructRef3) returns (stream StructRef4);
}

message StructRef1 {

    required string hi = 1;
}

message PartialRef2 {

    optional string bye = 1;
}

message StructRef3 {

    required string question = 1;
}

message StructRef4 {

    required NullableString answer = 1;
}

message NullableString {

    oneof kind {

        bool null = 1;
        string value = 2;
    }
}`)
    );
  });
});
