/* eslint-disable @typescript-eslint/no-explicit-any */
import { schemableLight as SCL } from '@jmorecroft67/io-ts-types';
import * as E from 'fp-ts/Either';
import { PrismaClient } from '@prisma/client';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server,
  Status
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import {
  selectDef,
  selectImpl,
  addService,
  makeClient,
  ApiClient
} from '../../src';

describe(__filename, () => {
  let server: Server;

  beforeEach(() => {
    server = createServer();
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  const entity = SCL.make((S) =>
    S.struct({
      id: S.number,
      name: S.nullable(S.string),
      createdAt: S.date,
      updatedAt: S.date
    })
  );

  const constraint = SCL.make((S) =>
    S.struct({
      name: S.struct({
        contains: S.string
      })
    })
  );

  const db = new PrismaClient().test;

  afterEach(() => db.deleteMany({}));

  const api = {
    name: 'TestSelectApi',
    methods: {
      select: selectDef({ entity, constraint, orderBy: ['id'] })
    }
  };

  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    addService(api)({
      impl: {
        select: selectImpl(db)
      },
      server
    });

    const channel = await listen();

    client = makeClient(api)((def) => createClient(def, channel));
  });

  it('should send between client and server, no errors', async () => {
    const bA = await db.create({ data: { name: 'B. A. Baracus' } });
    const face = await db.create({ data: { name: 'Face' } });
    await db.create({ data: { name: 'Murdoch' } });

    const result = await client.select({
      take: 2,
      skip: 1,
      orderBy: [{ id: 'desc' }],
      where: {
        name: {
          contains: 'c'
        }
      }
    })();

    expect(result).toEqual(E.right([face, bA]));
  });

  it('should send between client and server, empty results', async () => {
    const result = await client.select({
      where: {
        name: {
          contains: 'F'
        }
      }
    })();

    expect(result).toEqual(E.right([]));
  });

  it('should fail on bad (forced) input', async () => {
    const result = await client.select({
      orderBy: [{ id: 'desc' }],
      where: {
        name: {
          equals: 'Mr T'
        }
      } as any
    })();

    expect(result).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INTERNAL,
          message: `/TestSelectApi/select INTERNAL: required property "where"
└─ required property "name"
   └─ required property "contains"
      └─ cannot decode undefined, should be defined`
        })
      )
    );
  });
});
