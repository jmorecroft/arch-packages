/* eslint-disable @typescript-eslint/no-explicit-any */
import { schemableLight as SCL } from '@jmorecroft67/io-ts-types';
import * as E from 'fp-ts/Either';
import { PrismaClient } from '@prisma/client';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server,
  Status
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import {
  deleteDef,
  deleteImpl,
  addService,
  makeClient,
  ApiClient
} from '../../src';

describe(__filename, () => {
  let server: Server;

  beforeEach(() => {
    server = createServer();
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  const entity = SCL.make((S) =>
    S.struct({
      id: S.number,
      name: S.nullable(S.string),
      createdAt: S.date,
      updatedAt: S.date
    })
  );

  const key = SCL.make((S) =>
    S.struct({
      id: S.number
    })
  );

  const db = new PrismaClient().test;

  afterEach(() => db.deleteMany({}));

  const api = {
    name: 'TestDeleteApi',
    methods: {
      delete: deleteDef({ entity, key })
    }
  };

  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    addService(api)({
      impl: {
        delete: deleteImpl(db)
      },
      server
    });

    const channel = await listen();

    client = makeClient(api)((def) => createClient(def, channel));
  });

  it('should send between client and server, no errors', async () => {
    const biff = await db.create({
      data: {
        name: 'Biff'
      }
    });

    const result = await client.delete({
      where: {
        id: biff.id
      }
    })();

    expect(result).toEqual(E.right(biff));
    expect(await db.findMany()).toEqual([]);
  });

  it('should send between client and server, not found', async () => {
    const result = await client.delete({
      where: {
        id: 1
      }
    })();

    expect(result).toEqual(
      E.left(expect.objectContaining({ code: Status.NOT_FOUND }))
    );
  });

  it('should fail on bad (forced) input', async () => {
    const result = await client.delete({
      where: {}
    } as any)(); // force, since type safety won't allow this

    expect(result).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INTERNAL,
          message: `/TestDeleteApi/delete INTERNAL: required property "where"
└─ required property "id"
   └─ cannot decode undefined, should be defined`
        })
      )
    );
  });
});
