/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  schemable as SC,
  schemableLight as SCL
} from '@jmorecroft67/io-ts-types';
import * as E from 'fp-ts/Either';
import { PrismaClient } from '@prisma/client';
import 'jest-extended';
import {
  ChannelCredentials,
  createChannel,
  createClient,
  createServer,
  Server,
  Status
} from 'nice-grpc';
import { ServerCredentials } from '@grpc/grpc-js';
import {
  updateManyDef,
  updateManyImpl,
  addService,
  makeClient,
  ApiClient
} from '../../src';

describe(__filename, () => {
  let server: Server;

  beforeEach(() => {
    server = createServer();
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  const entity = SCL.make((S) =>
    S.struct({
      id: S.number,
      name: S.nullable(S.string),
      createdAt: S.date,
      updatedAt: S.date
    })
  );

  const data = SC.make((S) =>
    S.partial({
      name: S.nullable(S.string)
    })
  );

  const constraint = SC.make((S) =>
    S.struct({
      updatedAt: S.struct({
        gte: S.date
      })
    })
  );

  const db = new PrismaClient().test;

  afterEach(() => db.deleteMany({}));

  const api = {
    name: 'TestUpdateManyApi',
    methods: {
      updateMany: updateManyDef({ data, entity, constraint })
    }
  };

  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    addService(api)({
      impl: {
        updateMany: updateManyImpl(db)
      },
      server
    });

    const channel = await listen();

    client = makeClient(api)((def) => createClient(def, channel));
  });

  it('should send between client and server, no errors', async () => {
    const mrT = await db.create({ data: { name: 'Mr T' } });
    const face = await db.create({ data: { name: 'Face' } });

    const result = await client.updateMany({
      data: {
        name: null
      },
      where: {
        updatedAt: {
          gte: new Date(2000, 0, 1)
        }
      }
    })();

    expect(result).toEqual(E.right({ count: 2 }));
    expect(await db.findMany()).toIncludeAllMembers([
      expect.objectContaining({
        id: mrT.id,
        name: null
      }),
      expect.objectContaining({
        id: face.id,
        name: null
      })
    ]);
  });

  it('should send between client and server, not found', async () => {
    const result = await client.updateMany({
      data: {
        name: null
      },
      where: {
        updatedAt: {
          gte: new Date(3000, 0, 1)
        }
      }
    })();

    expect(result).toEqual(E.right({ count: 0 }));
  });

  it('should fail on bad (forced) input', async () => {
    const result = await client.updateMany({
      data: {
        name: null
      },
      where: {
        updatedAt: {},
        id: {
          gt: 1
        }
      } as any
    })(); // force, since type safety won't allow this

    expect(result).toEqual(
      E.left(
        expect.objectContaining({
          code: Status.INTERNAL,
          message: `/TestUpdateManyApi/updateMany INTERNAL: required property "where"
└─ required property "updatedAt"
   └─ required property "gte"
      └─ cannot decode undefined, should be defined`
        })
      )
    );
  });
});
