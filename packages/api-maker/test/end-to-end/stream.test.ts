/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  schemable as SC,
  schemableLight as SCL
} from '@jmorecroft67/io-ts-types';
import * as E from 'fp-ts/Either';
import { createClient } from 'redis';
import { PrismaClient } from '@prisma/client';
import { env } from 'process';
import Logger from 'bunyan';
import { promisify } from 'util';
import {
  ChannelCredentials,
  createChannel,
  createServer,
  createClient as createGrpcClient,
  Server,
  Status
} from 'nice-grpc';
import {
  makeRedisCache,
  makeXRead,
  push,
  clear
} from '@jmorecroft67/redis-cache';
import { ServerCredentials } from '@grpc/grpc-js';
import {
  MakeCache,
  selectStreamDef,
  selectStreamImpl,
  addService,
  makeClient,
  ApiClient,
  ChangeType,
  stdStreamServerOptions
} from '../../src';

describe(__filename, () => {
  let server: Server;

  beforeEach(() => {
    server = createServer();
  });

  afterEach(() => {
    server.forceShutdown();
  });

  const listen = async () => {
    const host = '0.0.0.0';
    const port = await server.listen(
      `${host}:0`,
      ServerCredentials.createInsecure()
    );
    return createChannel(
      `${host}:${port}`,
      ChannelCredentials.createInsecure()
    );
  };

  const entity = SCL.make((S) =>
    S.struct({
      id: S.number,
      name: S.nullable(S.string),
      createdAt: S.date,
      updatedAt: S.date
    })
  );

  const key = SC.make((S) => S.struct({}));

  const constraint = SC.make((S) =>
    S.struct({
      name: S.struct({
        endsWith: S.string
      })
    })
  );

  type EntityType = SCL.TypeOf<typeof entity>;

  const asCacheData = (obj: any) =>
    JSON.stringify(obj, (key, value) => {
      if (key === 'updatedAt') {
        return value.toString();
      }
      return value;
    });

  const asInsert = (newRecord: EntityType): ChangeType<EntityType> => ({
    type: 'Insert',
    newRecord
  });

  const asUpdate = (
    oldRecord: EntityType,
    newRecord: EntityType
  ): ChangeType<EntityType> => ({
    type: 'Update',
    oldRecord,
    newRecord
  });

  const asDelete = (oldRecord: EntityType): ChangeType<EntityType> => ({
    type: 'Delete',
    oldRecord
  });

  const db = new PrismaClient().test;

  const logger = new Logger({
    name: 'test',
    level: 'debug'
  });

  const redis = createClient({
    url: `redis://${env.REDIS_HOST}:${env.REDIS_PORT}`
  });

  const xread = makeXRead();

  const makeCache: MakeCache<EntityType, never> = (components) =>
    makeRedisCache({
      key: { prefix: 'Test', components }
    })({
      redis,
      logger,
      xread
    });

  makeCache({ id: '1' });

  afterEach(() => db.deleteMany({}));
  afterEach(() => clear('Test')({ redis }));
  afterAll(() => redis.disconnect());

  const api = {
    name: 'TestSelectStreamApi',
    methods: {
      selectStream: selectStreamDef({
        entity,
        key,
        constraint,
        orderBy: ['id']
      })
    }
  };

  let client: ApiClient<typeof api>;

  beforeEach(async () => {
    addService(api)({
      impl: {
        selectStream: selectStreamImpl({
          ...api.methods.selectStream,
          ...stdStreamServerOptions,
          db,
          makeCache
        })
      },
      server
    });

    const channel = await listen();

    client = makeClient(api)((def) => createGrpcClient(def, channel));
  });

  it('should stream from server, snapshot and changes', async () => {
    const received: any[] = [];
    let griff;
    let griff2;

    const lorraine = await db.create({ data: { name: 'Lorraine' } });
    await push(
      'Test',
      lorraine.updatedAt,
      asCacheData(asInsert(lorraine))
    )({ redis })();

    const jiff = await db.create({ data: { name: 'Jiff' } });
    const biff = await db.create({ data: { name: 'Biff' } });
    await db.create({ data: { name: 'Sniff' } });

    await Promise.all([
      (async () => {
        for await (const result of client.selectStream({
          key: {},
          where: { name: { endsWith: 'iff' } },
          orderBy: [{ id: 'desc' }],
          take: 2,
          skip: 1
        })) {
          received.push(result);
          if (
            E.isRight(result) &&
            result.right.type === 'changes' &&
            result.right.changes[result.right.changes.length - 1].type ===
              'Delete'
          ) {
            break;
          }
        }
      })(),
      (async () => {
        await promisify(setTimeout)(500);

        const marty = await db.create({ data: { name: 'Marty' } });
        await push(
          'Test',
          marty.updatedAt,
          asCacheData(asInsert(marty))
        )({ redis })();

        const doc = await db.create({ data: { name: 'Doc' } });
        await push(
          'Test',
          doc.updatedAt,
          asCacheData(asInsert(doc))
        )({ redis })();

        griff = await db.create({ data: { name: 'Griff' } });
        await push(
          'Test',
          griff.updatedAt,
          asCacheData(asInsert(griff))
        )({ redis })();

        griff2 = await db.update({
          data: { name: 'Tiff' },
          where: { id: griff.id }
        });
        await push(
          'Test',
          griff2.updatedAt,
          asCacheData(asUpdate(griff, griff2))
        )({ redis })();

        await db.delete({
          where: {
            id: griff.id
          }
        });
        await push(
          'Test',
          griff2.updatedAt,
          asCacheData(asDelete(griff2))
        )({ redis })();
      })()
    ]);

    expect(received).toEqual([
      E.right({
        type: 'snapshot',
        values: [biff, jiff]
      }),
      E.right({
        type: 'changes',
        changes: [asInsert(griff as any)]
      }),
      E.right({
        type: 'changes',
        changes: [asUpdate(griff as any, griff2 as any)]
      }),
      E.right({
        type: 'changes',
        changes: [asDelete(griff2 as any)]
      })
    ]);
  });

  it('should handle multiple simultaneous streams', async () => {
    const received1: any[] = [];
    const received2: any[] = [];

    let marty;
    let griff;

    const lorraine = await db.create({ data: { name: 'Lorraine' } });
    await push(
      'Test',
      lorraine.updatedAt,
      asCacheData(asInsert(lorraine))
    )({ redis })();

    const biff = await db.create({ data: { name: 'Biff' } });
    await push(
      'Test',
      biff.updatedAt,
      asCacheData(asInsert(biff))
    )({ redis })();

    const controller = new AbortController();
    const { signal } = controller;

    await Promise.all([
      (async () => {
        for await (const result of client.selectStream(
          {
            key: {},
            where: { name: { endsWith: 'iff' } }
          },
          {
            signal
          }
        )) {
          received1.push(result);
        }
      })(),
      (async () => {
        for await (const result of client.selectStream(
          {
            key: {},
            where: { name: { endsWith: '' } }
          },
          {
            signal
          }
        )) {
          received2.push(result);
        }
      })(),
      (async () => {
        await promisify(setTimeout)(500);

        marty = await db.create({ data: { name: 'Marty' } });
        await push(
          'Test',
          marty.updatedAt,
          asCacheData(asInsert(marty))
        )({ redis })();

        griff = await db.create({ data: { name: 'Griff' } });
        await push(
          'Test',
          griff.updatedAt,
          asCacheData(asInsert(griff))
        )({ redis })();

        await promisify(setTimeout)(200);

        controller.abort();
      })()
    ]);

    expect(received1).toEqual([
      E.right({
        type: 'snapshot',
        values: [biff]
      }),
      E.right({
        type: 'changes',
        changes: [asInsert(griff as any)]
      }),
      E.left(expect.objectContaining({ code: Status.ABORTED }))
    ]);

    expect(received2).toEqual([
      E.right({
        type: 'snapshot',
        values: [lorraine, biff]
      }),
      E.right({
        type: 'changes',
        changes: [asInsert(marty as any)]
      }),
      E.right({
        type: 'changes',
        changes: [asInsert(griff as any)]
      }),
      E.left(expect.objectContaining({ code: Status.ABORTED }))
    ]);
  });

  it('should stream from server until any error', async () => {
    const received: any[] = [];

    await Promise.all([
      (async () => {
        for await (const result of client.selectStream({
          key: {},
          where: { name: { endsWith: 'iff' } }
        })) {
          received.push(result);
        }
      })(),
      (async () => {
        await promisify(setTimeout)(500);

        await push('Test', new Date(), 'bad data')({ redis })();
      })()
    ]);

    await promisify(setTimeout)(500);

    expect(received).toEqual([
      E.right({
        type: 'snapshot',
        values: []
      }),
      E.left(expect.objectContaining({ code: Status.INTERNAL }))
    ]);
  });
});
