/* eslint-disable @typescript-eslint/ban-types */
import { unaryDef, ApiImpl, ApiClient } from '@jmorecroft67/api-maker';
import { schemable as S } from '@jmorecroft67/io-ts-types';
import {
  authenticationCredentialJSONSchema,
  publicKeyCredentialOptionsJSONSchema,
  publicKeyCredentialRequestOptionsJSONSchema,
  registrationCredentialJSONSchema
} from './webauthn';

function makeApi<D extends { email: string }>(userRegData: S.Schema<D>) {
  return {
    name: 'AuthApi',
    methods: {
      getJwksJson: unaryDef(
        S.make((S) => S.struct({})),
        S.make((S) => S.struct({ jwksJson: S.string }))
      ),
      generateRegistrationOptions: unaryDef(
        userRegData,
        S.make((S) =>
          S.struct({
            options: publicKeyCredentialOptionsJSONSchema(S)
          })
        )
      ),
      verifyRegistrationResponse: unaryDef(
        S.make((S) =>
          S.struct({
            email: S.string,
            credential: registrationCredentialJSONSchema(S)
          })
        ),
        S.make((S) =>
          S.struct({
            verified: S.boolean
          })
        )
      ),
      generateAuthenticationOptions: unaryDef(
        S.make((S) =>
          S.struct({
            email: S.string
          })
        ),
        S.make((S) =>
          S.struct({
            options: publicKeyCredentialRequestOptionsJSONSchema(S)
          })
        )
      ),
      verifyAuthenticationResponse: unaryDef(
        S.make((S) =>
          S.struct({
            email: S.string,
            credential: authenticationCredentialJSONSchema(S)
          })
        ),
        S.make((S) =>
          S.sum('type')({
            verified: S.struct({
              type: S.literal('verified'),
              tokens: S.struct({
                access: S.string,
                refresh: S.string
              })
            }),
            notVerified: S.struct({
              type: S.literal('notVerified')
            })
          })
        )
      ),
      refreshTokens: unaryDef(
        S.make((S) =>
          S.struct({
            refresh: S.string
          })
        ),
        S.make((S) =>
          S.struct({
            tokens: S.struct({
              access: S.string,
              refresh: S.string
            })
          })
        )
      )
    }
  };
}

export type UserRegData = { email: string };
export type AuthApi<D extends UserRegData> = ReturnType<typeof makeApi<D>>;
export type AuthApiImpl<D extends UserRegData> = ApiImpl<AuthApi<D>, {}>;
export type AuthApiClient<D extends UserRegData> = ApiClient<AuthApi<D>>;

export default makeApi;
