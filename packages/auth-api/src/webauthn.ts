import { schemable as S } from '@jmorecroft67/io-ts-types';
import { pipe } from 'fp-ts/lib/function';

const publicKeyCredentialEntitySchema = S.make((S) =>
  pipe(
    S.struct({
      name: S.string
    }),
    S.intersect(
      S.partial({
        id: S.string
      })
    )
  )
);

const publicKeyCredentialUserEntityJSONSchema = S.make((S) =>
  pipe(
    S.struct({
      id: S.string,
      displayName: S.string
    }),
    S.intersect(publicKeyCredentialEntitySchema(S))
  )
);

const publicKeyCredentialDescriptorJSONSchema = S.make((S) =>
  pipe(
    S.struct({
      id: S.string,
      type: S.literal('public-key')
    }),
    S.intersect(
      S.partial({
        transports: S.array(
          S.literal('ble', 'internal', 'nfc', 'usb', 'cable', 'hybrid')
        )
      })
    )
  )
);

export const publicKeyCredentialRequestOptionsJSONSchema = S.make((S) =>
  pipe(
    S.struct({
      challenge: S.string
    }),
    S.intersect(
      S.partial({
        rpId: S.string,
        allowCredentials: S.array(publicKeyCredentialDescriptorJSONSchema(S)),
        extensions: S.partial({
          appid: S.string,
          appidExclude: S.string,
          credProps: S.boolean,
          uvm: S.boolean
        }),
        timeout: S.number,
        userVerification: S.literal('discouraged', 'preferred', 'required')
      })
    )
  )
);

export const authenticationCredentialJSONSchema = S.make((S) =>
  pipe(
    S.struct({
      id: S.string,
      type: S.string,
      rawId: S.string,
      response: pipe(
        S.struct({
          authenticatorData: S.string,
          clientDataJSON: S.string,
          signature: S.string
        }),
        S.intersect(
          S.partial({
            userHandle: S.string
          })
        )
      ),
      clientExtensionResults: S.partial({
        appid: S.boolean,
        credProps: S.partial({
          rk: S.boolean
        }),
        uvm: S.array(S.array(S.number))
      })
    }),
    S.intersect(
      S.partial({
        authenticatorAttachment: S.literal('cross-platform', 'platform')
      })
    )
  )
);

export const registrationCredentialJSONSchema = S.make((S) =>
  pipe(
    S.struct({
      id: S.string,
      type: S.string,
      rawId: S.string,
      response: S.struct({
        clientDataJSON: S.string,
        attestationObject: S.string
      }),
      clientExtensionResults: S.partial({
        appid: S.boolean,
        credProps: S.partial({
          rk: S.boolean
        }),
        uvm: S.array(S.array(S.number))
      })
    }),
    S.intersect(
      S.partial({
        transports: S.array(
          S.literal('ble', 'internal', 'nfc', 'usb', 'cable', 'hybrid')
        ),
        authenticatorAttachment: S.literal('cross-platform', 'platform')
      })
    )
  )
);

export const publicKeyCredentialOptionsJSONSchema = S.make((S) =>
  pipe(
    S.struct({
      user: publicKeyCredentialUserEntityJSONSchema(S),
      challenge: S.string,
      pubKeyCredParams: S.array(
        S.struct({
          alg: S.number,
          type: S.literal('public-key')
        })
      ),
      rp: publicKeyCredentialEntitySchema(S),
      excludeCredentials: S.array(publicKeyCredentialDescriptorJSONSchema(S))
    }),
    S.intersect(
      S.partial({
        attestation: S.literal('none', 'indirect', 'direct', 'enterprise'),
        authenticationSelection: S.partial({
          authenticatorAttachment: S.literal('cross-platform', 'platform'),
          requireResidentKey: S.boolean,
          residentKey: S.literal('discouraged', 'preferred', 'required'),
          userVerification: S.literal('discouraged', 'preferred', 'required')
        }),
        extensions: S.partial({
          appid: S.string,
          appidExclude: S.string,
          credProps: S.boolean,
          uvm: S.boolean
        }),
        timeout: S.number
      })
    )
  )
);
