import makeApi, { AuthApiImpl, AuthApiClient } from './make-api';

export { AuthApiImpl, AuthApiClient };
export default makeApi;
