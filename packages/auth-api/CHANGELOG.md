# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0...@jmorecroft67/auth-api@1.0.1) (2023-01-05)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.9...@jmorecroft67/auth-api@1.0.0) (2023-01-04)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.9](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.8...@jmorecroft67/auth-api@1.0.0-staging.9) (2022-12-13)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.8](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.7...@jmorecroft67/auth-api@1.0.0-staging.8) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.7](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.6...@jmorecroft67/auth-api@1.0.0-staging.7) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.6](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.5...@jmorecroft67/auth-api@1.0.0-staging.6) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.5](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.4...@jmorecroft67/auth-api@1.0.0-staging.5) (2022-12-03)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.4](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.3...@jmorecroft67/auth-api@1.0.0-staging.4) (2022-12-02)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.3](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.2...@jmorecroft67/auth-api@1.0.0-staging.3) (2022-12-01)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.2](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.1...@jmorecroft67/auth-api@1.0.0-staging.2) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/auth-api





# [1.0.0-staging.1](https://gitlab.com/jmorecroft/arch-packages/compare/@jmorecroft67/auth-api@1.0.0-staging.0...@jmorecroft67/auth-api@1.0.0-staging.1) (2022-11-29)

**Note:** Version bump only for package @jmorecroft67/auth-api





# 1.0.0-staging.0 (2022-11-28)


### Bug Fixes

* **@jmorecroft67/auth-api:** add missing dependency ([0bdb224](https://gitlab.com/jmorecroft/arch-packages/commit/0bdb22475005db414856527239c53e6c74e5a696))


### Features

* **@jmorecroft67/auth:** auth major refactor ([fe920fb](https://gitlab.com/jmorecroft/arch-packages/commit/fe920fb5546f305844376a17309689e2482e2b0a))


### BREAKING CHANGES

* **@jmorecroft67/auth:** @jmorecroft67/auth refactored from an express app to an unauthenticated GRPC service.
