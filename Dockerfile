FROM body1967/builder:node-16

WORKDIR /app

COPY ./package*.json ./
COPY ./packages/io-ts-types/package*.json ./packages/io-ts-types/
COPY ./packages/api-maker/package*.json ./packages/api-maker/
COPY ./packages/redis-cache/package*.json ./packages/redis-cache/
COPY ./packages/gcp-pubsub-to-redis/package*.json ./packages/gcp-pubsub-to-redis/

RUN npm install -ws

COPY . .

RUN npm run build -ws

CMD [ "npm", "run", "test", "-ws" ]